module count_cage_occ_module
    use precision_module
    use parameters_module
    use system_module
    use molecule_definitions_module
    use simulation_utilities_module
    implicit none

    integer, dimension(:,:), allocatable :: large_cage_definition,small_cage_definition
    real(kind=mc_prec), dimension(:,:), allocatable :: small_pos, large_pos 
    integer :: nsmall, nlarge
    integer :: n_per_small, n_per_large

contains

    subroutine cages_initialize()
        integer :: i, j
        integer, parameter :: cagefile = 988998

        open(cagefile,file='cage_defs',status='old')
        read(cagefile,*) nsmall, n_per_small
        allocate(small_cage_definition(n_per_small,nsmall))
        allocate(small_pos(3,nsmall))
        do i = 1, nsmall
            read(cagefile,*)small_cage_definition(1:n_per_small,i)
        end do
        read(cagefile,*) nlarge, n_per_large
        allocate(large_cage_definition(n_per_large,nlarge))
        allocate(large_pos(3,nlarge))
        do i = 1, nlarge
            read(cagefile,*)large_cage_definition(1:n_per_large,i)
        end do
        close(cagefile)

    end subroutine 

    subroutine count_occupancies(sys,wfile)
        type(system) :: sys
        integer :: wfile !File to write output
        integer, dimension(:), allocatable :: s_nocc, l_nocc

        integer :: i,j,ind

        call find_cage_com(sys)
        allocate(s_nocc(sys%ntypes))
        allocate(l_nocc(sys%ntypes))
        s_nocc = 0
        l_nocc = 0
        do i =2,sys%ntypes
            do j = 1, sys%species(i)%nmol
                ind = occ_large(  sys%species(i)%atom(1)%pos%X(j),&
                                  sys%species(i)%atom(1)%pos%Y(j),&
                                  sys%species(i)%atom(1)%pos%Z(j), sys )
                if(ind.gt.0)then
                    l_nocc(i) = l_nocc(i) + 1
                else
                    ind = occ_small(  sys%species(i)%atom(1)%pos%X(j),&
                                      sys%species(i)%atom(1)%pos%Y(j),&
                                      sys%species(i)%atom(1)%pos%Z(j), sys )
                    if(ind.gt.0)then
                        s_nocc(i) = s_nocc(i) + 1
                    else
                        write(*,*) 'warning: no cage found'
                    end if
                end if
            end do
        end do
        write(wfile,*) s_nocc(2:sys%ntypes), l_nocc(2:sys%ntypes)
    
    end subroutine

    function occ_small(x,y,z,sys) result(index)
        type(system) :: sys
        integer :: index
        real(kind=mc_prec) :: radsquared = 10._mc_prec
        real(kind=mc_prec) :: dr, x, y, z
        integer :: i

        index = -10
        do i = 1,nsmall
            dr = distsq2(x,y,z,&
                         small_pos(1,i),small_pos(2,i),small_pos(3,i), &
                         sys%bc)
            if(dr.lt.radsquared)index = i
        end do
        return

    end function

    function occ_large(x,y,z,sys) result(index)
        type(system) :: sys
        integer :: index
        real(kind=mc_prec) :: radsquared = 10._mc_prec
        real(kind=mc_prec) :: dr, x, y, z
        integer :: i

        index = -10
        do i = 1,nlarge
            dr = distsq2(x,y,z,&
                         large_pos(1,i),large_pos(2,i),large_pos(3,i), &
                         sys%bc)
            if(dr.lt.radsquared)index = i
        end do
        return

    end function

    subroutine find_cage_com(sys)
    
        type(system) :: sys
        integer :: i,j,sd
        real(kind=mc_prec) :: x,ax,y,ay,z,az

        do i = 1, nsmall
            sd = small_cage_definition(1,i)
            ax = sys%species(1)%atom(1)%pos%X(sd)
            ay = sys%species(1)%atom(1)%pos%Y(sd)
            az = sys%species(1)%atom(1)%pos%Z(sd)
            do j = 2, n_per_small
                sd = small_cage_definition(j,i)
                x = sys%species(1)%atom(1)%pos%X(sd)
                y = sys%species(1)%atom(1)%pos%Y(sd)
                z = sys%species(1)%atom(1)%pos%Z(sd)
                if(x-ax.gt..5d0*sys%bc(1))x = x - sys%bc(1)
                if(y-ay.gt..5d0*sys%bc(1))y = y - sys%bc(2)
                if(z-az.gt..5d0*sys%bc(1))z = z - sys%bc(3)

                if(x-ax.lt.-.5d0*sys%bc(1))x = x + sys%bc(1)
                if(y-ay.lt.-.5d0*sys%bc(1))y = y + sys%bc(2)
                if(z-az.lt.-.5d0*sys%bc(1))z = z + sys%bc(3)

                small_pos(1,i) = small_pos(1,i) + x
                small_pos(2,i) = small_pos(2,i) + y
                small_pos(3,i) = small_pos(3,i) + z 
            end do
            small_pos(1,i) = (small_pos(1,i) + ax)/n_per_small
            small_pos(2,i) = (small_pos(2,i) + ay)/n_per_small
            small_pos(3,i) = (small_pos(3,i) + az)/n_per_small 
        end do

        do i = 1, nlarge
            sd = large_cage_definition(1,i)
            ax = sys%species(1)%atom(1)%pos%X(sd)
            ay = sys%species(1)%atom(1)%pos%Y(sd)
            az = sys%species(1)%atom(1)%pos%Z(sd)
            do j = 2, n_per_large
                sd = large_cage_definition(j,i)
                x = sys%species(1)%atom(1)%pos%X(sd)
                y = sys%species(1)%atom(1)%pos%Y(sd)
                z = sys%species(1)%atom(1)%pos%Z(sd)
                if(x-ax.gt..5d0*sys%bc(1))x = x - sys%bc(1)
                if(y-ay.gt..5d0*sys%bc(1))y = y - sys%bc(2)
                if(z-az.gt..5d0*sys%bc(1))z = z - sys%bc(3)

                if(x-ax.lt.-.5d0*sys%bc(1))x = x + sys%bc(1)
                if(y-ay.lt.-.5d0*sys%bc(1))y = y + sys%bc(2)
                if(z-az.lt.-.5d0*sys%bc(1))z = z + sys%bc(3)

                large_pos(1,i) = large_pos(1,i) + x
                large_pos(2,i) = large_pos(2,i) + y
                large_pos(3,i) = large_pos(3,i) + z 
            end do
            large_pos(1,i) = (large_pos(1,i) + ax)/n_per_large
            large_pos(2,i) = (large_pos(2,i) + ay)/n_per_large
            large_pos(3,i) = (large_pos(3,i) + az)/n_per_large 
        end do

    end subroutine

end module

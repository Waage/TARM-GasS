module barrier_module
    !Defines barriers which molecules are not allowed to pass through
    use precision_module
    use molecule_definitions_module
    implicit none

    type barrierpointer

        class(barrier), pointer :: p

    end type

    type, abstract :: barrier
        integer, dimension(:), allocatable :: constrain
        logical :: shall_sample = .false.

    contains

        procedure(move_is_allowed), deferred :: allowed
        procedure(sample_barrier), deferred :: sample
        procedure(barrier_update), deferred :: update
        procedure(barrier_print), deferred :: printinfo
        procedure :: change_constraint_position
        procedure :: magicallytransport => barrier_transport
    end type

    abstract interface
        logical function move_is_allowed(bar,index,typind,frompos,topos,species,box,delta_bias)
            import molecule_species
            import barrier
            import mc_prec

            class(barrier) :: bar
            real(kind=mc_prec), dimension(3) :: frompos,topos,box
            real(kind=mc_prec) :: delta_bias
            integer :: index, typind
            logical :: allowed
            type(molecule_species), dimension(:) :: species

        end function

        subroutine sample_barrier(bar,fileno,species,box)
            import molecule_species
            import barrier
            import mc_prec

            class(barrier) :: bar
            integer :: fileno
            real(kind=mc_prec), dimension(3) :: box
            type(molecule_species), dimension(:) :: species


        end subroutine

        
        subroutine barrier_update(bar,index, typind,frompos,topos,box)
            import barrier
            import mc_prec

            class(barrier) :: bar
            integer :: index, typind
            real(kind=mc_prec), dimension(3) :: frompos,topos,box

        end subroutine

        subroutine barrier_print(bar)
            import barrier
            import mc_prec
            class(barrier) :: bar

        end subroutine

    end interface

    type, extends(barrier) :: straight_wall
        !A straight bar defined as follows
        !Let c1 and c2 be two distinct sets of species. Let wvec be a vector running from the center of mass of species in c1 to the center of mass of species in c2.
        !Let distance be some distance along this bar. Let mvec be a vector between a molecule m's center of mass and c1. The molecule m is defined to be on one side of the bar if the dot product
        !of mvec and wvec is less than distance, and on the other side if it is greater.
        !A move is only allowed if all species listed in constrain remain on the same side of the bar after the move is completed.
        !All species defining one center are assumed to be of equal mass

        integer, dimension(:), allocatable :: c1,c2
        real(kind=mc_prec) :: distance
        real(kind=mc_prec), dimension(3) :: cm1, cm2, wvec


    contains

        procedure :: passed => wall_passed
        procedure :: allowed => wall_is_move_allowed
        procedure :: shifted_past => wall_shifted_past
        procedure :: sample => wall_sample
        procedure :: update => wall_update
        procedure :: printinfo => wall_print
        procedure :: magicallytransport => wall_transport
        procedure :: change_constraint_position => change_constraint_wall
    

    end type

    interface straight_wall

        procedure :: new_straight_wall

    end interface

    type, extends(straight_wall) :: straight_wall_absolute
        !A straight wall, but the barrier is given in absolute rather than relative coordinates
        !For simplicity, 

    contains

        procedure :: passed => absolute_wall_passed
    !   procedure :: allowed => absolute_wall_is_move_allowed
        procedure :: shifted_past => absolute_wall_shifted_past
    !   procedure :: sample => wall_sample
    !   procedure :: update => wall_update
    !   procedure :: printinfo => wall_print
        procedure :: magicallytransport => absolute_wall_transport


    end type

    interface straight_wall_absolute

        procedure :: new_straight_wall_absolute

    end interface


contains

    subroutine change_constraint_position(bar,new_val)
        class(barrier) :: bar
        real(kind=mc_prec) ::new_val
    end subroutine change_constraint_position

    subroutine change_constraint_wall(bar,new_val)

        class(straight_wall) :: bar
        real(kind=mc_prec) ::new_val

        bar%distance = new_val
    end subroutine change_constraint_wall


    subroutine barrier_transport(bar,mol,box)

        class(barrier) :: bar
        type(single_molecule) :: mol
        real(kind=mc_prec), dimension(3) :: box

        !Does nothing. Not deferred because not strictly necessary

    end subroutine


    ! real(kind=mc_prec) function bias_value(bar)
    !     class(barrier) :: bar
    !     bias_value= 0._mc_prec
    ! end function bias_value


    function new_straight_wall(c1,c2,constrain,distance,species,box) result(wal)

        integer, dimension(:) :: c1,c2, constrain
        type(molecule_species), dimension(:) :: species
        real(kind=mc_prec) :: distance
        real(kind=mc_prec), dimension(3) :: box,cp
        type(straight_wall) :: wal

        integer :: nc1,nc2,nconstrain
        integer :: i

        nc1 = size(c1)
        nc2 = size(c2)
        nconstrain = size(constrain)

        allocate(wal%c1(nc1))
        allocate(wal%c2(nc2))
        allocate(wal%constrain(nconstrain))

        wal%c1 = c1
        wal%c2 = c2
        wal%constrain = constrain

        wal%distance = distance

        wal%cm1 = (/species(1)%atom(1)%pos%X(c1(1)),&
                    species(1)%atom(1)%pos%Y(c1(1)),&
                    species(1)%atom(1)%pos%Z(c1(1))/)
        wal%cm2 = (/species(1)%atom(1)%pos%X(c2(1)),&
                    species(1)%atom(1)%pos%Y(c2(1)),&
                    species(1)%atom(1)%pos%Z(c2(1))/)

        do i = 2, nc1
            cp = (/species(1)%atom(1)%pos%X(c1(i)),&
                   species(1)%atom(1)%pos%Y(c1(i)),&
                   species(1)%atom(1)%pos%Z(c1(i))/)
            cp = closest_vector(to=cp,from=wal%cm1,box=box)
            wal%cm1 = wal%cm1 + cp/i
        end do
        do i = 2, nc2
            cp = (/species(1)%atom(1)%pos%X(c2(i)),&
                   species(1)%atom(1)%pos%Y(c2(i)),&
                   species(1)%atom(1)%pos%Z(c2(i))/)
            cp = closest_vector(to=cp,from=wal%cm2,box=box)
            wal%cm2 = wal%cm2 + cp/i
        end do

        wal%wvec = closest_vector(to=wal%cm2,from=wal%cm1,box=box)

        return

    end function

    logical function wall_passed(bar,frompos,topos,box)

        class(straight_wall) :: bar
        real(kind=mc_prec), dimension(3) :: frompos,topos,box
        real(kind=mc_prec), dimension(3) :: molvecf, molvect
        real(kind=mc_prec) :: norm

        molvecf = closest_vector(to=frompos,from=bar%cm1,box=box)

        molvect = closest_vector(to=topos,from=bar%cm1,box=box)
        norm = dotprod(bar%wvec,bar%wvec)
        if(( dotprod(molvecf,bar%wvec)/norm-bar%distance )*( dotprod(molvect,bar%wvec)/norm-bar%distance ) .lt. 0.d0 ) then
            !The molecule is on the other side of the bar than before it moved, the bar has been passed
            wall_passed = .true.
        else
            wall_passed = .false.
        end if

        return

    end function

    logical function wall_shifted_past(bar,which_molecule,frompos,topos,species,box)

        !Returns true if moving the molecule belonging to which_center would cause a molecule to end up on the other side of the bar defined by centers 1 and 2
        !Assumes that molecules that are being constrained are always present as component no 2 (unsafe, fix)
        class(straight_wall) :: bar
        real(kind=mc_prec), dimension(3) :: frompos,topos,box
        real(kind=mc_prec), dimension(3) :: nwallvec, ncm1, ncm2, nmolvec, omolvec
        type(molecule_species), dimension(:) :: species
        integer :: which_molecule

        integer :: i

        ncm1 = bar%cm1
        ncm2 = bar%cm2


        do i = 1, size(bar%c1)
            if(which_molecule.eq.bar%c1(i))then
                ncm1 = closest_vector(to=topos,from=frompos,box=box)/size(bar%c1) + bar%cm1
            end if
        end do
        do i = 1, size(bar%c2)
            if(which_molecule.eq.bar%c2(i))then
                ncm2 = closest_vector(to=topos,from=frompos,box=box)/size(bar%c2) + bar%cm2
            end if
        end do

        nwallvec = closest_vector(to=ncm2,from=ncm1,box=box)


        wall_shifted_past = .false.

        do i =1, size(bar%constrain)

            omolvec =   (/species(3)%atom(1)%pos%X(bar%constrain(i)),&
                          species(3)%atom(1)%pos%Y(bar%constrain(i)),&
                          species(3)%atom(1)%pos%Z(bar%constrain(i))/)

            omolvec = closest_vector(to=omolvec,from=bar%cm1,box=box)
            nmolvec =   (/species(3)%atom(1)%pos%X(bar%constrain(i)),&
                          species(3)%atom(1)%pos%Y(bar%constrain(i)),&
                          species(3)%atom(1)%pos%Z(bar%constrain(i))/)
            nmolvec = closest_vector(to=nmolvec,from=ncm1,box=box)
            if(( dotprod(omolvec,bar%wvec)/dotprod(bar%wvec,bar%wvec)-bar%distance ) &
                & *( dotprod(nmolvec,nwallvec)/dotprod(nwallvec,nwallvec)-bar%distance ) .lt. 0.d0 ) then
                !The molecule is on the other side of the bar than before it moved, the bar has been passed
                wall_shifted_past = .true.
                return
            end if
        end do

        return

    end function


    function wall_is_move_allowed(bar,index,typind,frompos,topos,species,box, delta_bias) result(allowed)

        !Returns true if the move does not cause any species to end up on other sides of walls

        class(straight_wall) :: bar
        real(kind=mc_prec), dimension(3) :: frompos,topos,box
        real(kind=mc_prec) :: delta_bias
        integer :: index, typind
        logical :: allowed
        type(molecule_species), dimension(:) :: species

        integer :: i

        delta_bias = 0._mc_prec
        allowed = .true.
        if(typind .eq. 1) then
            do i = 1, size(bar%c1)
                if(bar%c1(i).eq.index)  allowed = .not.(bar%shifted_past(index,frompos,topos,species,box))
            end do
            do i = 1, size(bar%c2)
                if(bar%c2(i).eq.index)  allowed = (allowed.and..not.(bar%shifted_past(index,frompos,topos,species,box)))
            end do
        else
            do i = 1, size(bar%constrain)
                if(bar%constrain(i).eq.index)   allowed = (allowed.and..not.(bar%passed(frompos,topos,box)))
            end do
        end if


    end function

    subroutine wall_sample(bar,fileno, species,box)
        !UNSAFE should know which component the constrained molecule is.
        class(straight_wall) :: bar
        integer :: fileno
        real(kind=mc_prec), dimension(3) :: box
        type(molecule_species), dimension(:) :: species

        real(kind=mc_prec), dimension(3) :: molvec

        molvec = (/species(3)%atom(1)%pos%X(bar%constrain(1)),&
                   species(3)%atom(1)%pos%Y(bar%constrain(1)),&
                   species(3)%atom(1)%pos%Z(bar%constrain(1))/)
        molvec = closest_vector(to=molvec,from=bar%cm1,box=box)
        write(fileno,*) dotprod(molvec,bar%wvec)/sqrt(dotprod(bar%wvec,bar%wvec)),sqrt(dotprod(bar%wvec,bar%wvec)) 

    end subroutine

    subroutine wall_update(bar,index,typind,frompos,topos,box)


        class(straight_wall) :: bar
        real(kind=mc_prec), dimension(3) :: frompos,topos, box
        real(kind=mc_prec), dimension(3) :: nwallvec, ncm1, ncm2
        integer :: index, typind
        logical :: affected

        integer :: i

        affected = .false.
        ncm1 = bar%cm1
        ncm2 = bar%cm2
        do i = 1, size(bar%c1)
            if(typind.eq.1.and.index.eq.bar%c1(i))then
                affected = .true.
                ncm1 = closest_vector(to=topos,from=frompos,box=box)/size(bar%c1) + ncm1
            end if
        end do
        do i = 1, size(bar%c2)
            if(typind.eq.1.and.index.eq.bar%c2(i))then
                affected = .true.
                ncm2 = closest_vector(to=topos,from=frompos,box=box)/size(bar%c2) + ncm2
            end if
        end do

        if(affected)then

            nwallvec = closest_vector(to=ncm2,from=ncm1,box=box)

            bar%cm1 = ncm1
            bar%cm2 = ncm2
            bar%wvec = nwallvec
        end if

        return

    end subroutine

    subroutine wall_print(bar)

        class(straight_wall) :: bar

        write(*,*) bar%c1
        write(*,*) bar%c2
        write(*,*) bar%constrain
        write(*,*) bar%distance
        write(*,*) bar%wvec
        write(*,*) bar%cm1
        write(*,*) bar%cm2

    end subroutine

    subroutine wall_transport(bar,mol,box)
        !Transport the molecule to a position on the interior of the umbrella-sampling zone
        class(straight_wall) :: bar
        type(single_molecule) :: mol
        real(kind=mc_prec), dimension(3) :: dummy, box
        integer :: i

        dummy = bar%cm1 + bar%wvec*(bar%distance*.9d0)
        dummy = closest_vector(to=dummy,from=(/mol%X(1),mol%Y(1),mol%Z(1)/),box=box) 

        call translate(mol,dummy)


    end subroutine

    function new_straight_wall_absolute(c1,c2,constrain,distance,species,box) result(wal)

        integer, dimension(:) :: c1,c2, constrain
        type(molecule_species), dimension(:) :: species
        real(kind=mc_prec) :: distance
        type(straight_wall_absolute) :: wal
        real(kind=mc_prec), dimension(3) :: box,cp

        integer :: nc1,nc2,nconstrain
        integer :: i

        nc1 = size(c1)
        nc2 = size(c2)
        nconstrain = size(constrain)

        allocate(wal%c1(nc1))
        allocate(wal%c2(nc2))
        allocate(wal%constrain(nconstrain))

        wal%c1 = c1
        wal%c2 = c2
        wal%constrain = constrain

        wal%cm1 = (/species(1)%atom(1)%pos%X(c1(1)),&
                    species(1)%atom(1)%pos%Y(c1(1)),&
                    species(1)%atom(1)%pos%Z(c1(1))/)
        wal%cm2 = (/species(1)%atom(1)%pos%X(c2(1)),&
                    species(1)%atom(1)%pos%Y(c2(1)),&
                    species(1)%atom(1)%pos%Z(c2(1))/)


        do i = 2, nc1
            cp = (/species(1)%atom(1)%pos%X(c1(i)),&
                   species(1)%atom(1)%pos%Y(c1(i)),&
                   species(1)%atom(1)%pos%Z(c1(i))/)
            cp = closest_vector(to=cp,from=wal%cm1,box=box)
            wal%cm1 = wal%cm1 + cp/i
        end do
        do i = 2, nc2
            cp = (/species(1)%atom(1)%pos%X(c2(i)),&
                   species(1)%atom(1)%pos%Y(c2(i)),&
                   species(1)%atom(1)%pos%Z(c2(i))/)
            cp = closest_vector(to=cp,from=wal%cm2,box=box)
            wal%cm2 = wal%cm2 + cp/i
        end do

        wal%wvec = closest_vector(to=wal%cm2,from=wal%cm1,box=box)

        wal%distance = distance

        return

    end function

    logical function absolute_wall_passed(bar,frompos,topos,box)

        class(straight_wall_absolute) :: bar
        real(kind=mc_prec), dimension(3) :: frompos,topos,box
        real(kind=mc_prec), dimension(3) :: molvecf, molvect
        real(kind=mc_prec) :: norm

        molvecf = closest_vector(to=frompos,from=bar%cm1,box=box)

        molvect = closest_vector(to=topos,from=bar%cm1,box=box)
        norm = 1.d0/sqrt(dotprod(bar%wvec,bar%wvec))
        if(( dotprod(molvecf,bar%wvec)*norm-bar%distance )*( dotprod(molvect,bar%wvec)*norm-bar%distance ) .lt. 0.d0 ) then
            !The molecule is on the other side of the bar than before it moved, the bar has been passed
            absolute_wall_passed = .true.
        else
            absolute_wall_passed = .false.
        end if

        return

    end function

    logical function absolute_wall_shifted_past(bar,which_molecule,frompos,topos,species,box)

        !Returns true if moving the molecule belonging to which_center would cause a molecule to end up on the other side of the bar defined by centers 1 and 2

        class(straight_wall_absolute) :: bar
        real(kind=mc_prec), dimension(3) :: frompos,topos,box
        real(kind=mc_prec), dimension(3) :: nwallvec, ncm1, ncm2, nmolvec, omolvec
        type(molecule_species), dimension(:) :: species
        integer :: which_molecule

        integer :: i

        ncm1 = bar%cm1
        ncm2 = bar%cm2


        do i = 1, size(bar%c1)
            if(which_molecule.eq.bar%c1(i))then
                ncm1 = closest_vector(to=topos,from=frompos,box=box)/size(bar%c1) + bar%cm1
            end if
        end do
        do i = 1, size(bar%c2)
            if(which_molecule.eq.bar%c2(i))then
                ncm2 = closest_vector(to=topos,from=frompos,box=box)/size(bar%c2) + bar%cm2
            end if
        end do

        nwallvec = closest_vector(to=ncm2,from=ncm1,box=box)


        absolute_wall_shifted_past = .false.

        do i =1, size(bar%constrain)
            omolvec = (/species(3)%atom(1)%pos%X(bar%constrain(i)),&
                        species(3)%atom(1)%pos%Y(bar%constrain(i)),&
                        species(3)%atom(1)%pos%Z(bar%constrain(i))/)
            omolvec = closest_vector(to = omolvec,from=bar%cm1,box=box)
            nmolvec = (/species(3)%atom(1)%pos%X(bar%constrain(i)),&
                        species(3)%atom(1)%pos%Y(bar%constrain(i)),&
                        species(3)%atom(1)%pos%Z(bar%constrain(i))/)
            nmolvec = closest_vector(to = nmolvec,from=ncm1,box=box)
            if(( dotprod(omolvec,bar%wvec)/sqrt(dotprod(bar%wvec,bar%wvec))-bar%distance )&
                &*( dotprod(nmolvec,nwallvec)/sqrt(dotprod(nwallvec,nwallvec))-bar%distance ) .lt. 0.d0 ) then
                !The molecule is on the other side of the bar than before it moved, the bar has been passed
                absolute_wall_shifted_past = .true.
                return
            end if
        end do

        return

    end function

    subroutine absolute_wall_transport(bar,mol,box)

        class(straight_wall_absolute) :: bar
        type(single_molecule) :: mol
        real(kind=mc_prec), dimension(3) :: dummy, box
        integer :: i
        real(kind=mc_prec) :: norm

        norm = 1.d0/sqrt(dotprod(bar%wvec,bar%wvec))

        dummy = bar%cm1 + bar%wvec*(bar%distance*.9d0*norm)

        dummy = closest_vector(to=dummy,from=(/mol%X(1),mol%Y(1),mol%Z(1)/),box=box)

        call translate(mol,dummy)

    end subroutine

    function closest_vector(to,from,box) result(outval)
        !Find the vector to the closest image, given to box
        real(kind=mc_prec), dimension(3) :: outval,to,from,box, dd
        integer :: i, j

        do i = 1, 3
            dd(1) = to(i)-from(i)
            dd(2) = to(i)-from(i) - box(i)
            dd(3) = to(i)-from(i) + box(i)
            do j = 2,3
                if(dd(1)*dd(1).gt.dd(j)*dd(j))then
                    dd(1) = dd(j)
                end if
            end do
            outval(i) = dd(1)
        end do
        return
    end function 
    

    real(kind=mc_prec) function dotprod(a,b)

        real(kind=mc_prec), dimension(3) :: a,b

        dotprod = a(1)*b(1) + a(2)*b(2) + a(3)*b(3)

        return

    end function

end module



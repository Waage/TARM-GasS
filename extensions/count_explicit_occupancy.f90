module count_explicit_occupancy

    use precision_module
    use molecule_definitions_module
    implicit none

    integer, parameter :: n_cages = 64
    real(kind=mc_prec), parameter :: half = 0.5_mc_prec
    real(kind=mc_prec), dimension(n_cages) :: c_X, c_Y, c_Z 
    integer, dimension(n_cages) :: water_molecules_per_cage
    logical, dimension(n_cages) :: is_large
    integer, dimension(n_cages) :: guest_molecules_per_cage
    integer, dimension(5,n_cages) :: which_guest_molecules_in_cage
    integer, dimension(40,n_cages) :: cage_waters
    integer, dimension(2,n_cages+5) :: nearest_two
    integer, dimension(2,n_cages+5) :: nearest_two_distance

    real(kind=mc_prec), parameter :: large_radius_squared = 12.0 
    real(kind=mc_prec), parameter :: small_radius_squared = 10.0 

    integer, parameter :: water_index = 1, guest_index = 2

contains

    subroutine initialize_cages()
    
        integer :: nsmall, nlarge, n_per_small, n_per_large, cagefile
        integer :: i

        open(newunit=cagefile,file='cage_defs',status='old')
        read(cagefile,*) nsmall, n_per_small
        is_large(1:nsmall) = .false.
        water_molecules_per_cage(1:nsmall) = n_per_small
        do i = 1, nsmall
            read(cagefile,*)cage_waters(1:n_per_small,i)
        end do
        read(cagefile,*) nlarge, n_per_large
        is_large(nsmall+1:nsmall+nlarge) = .false.
        water_molecules_per_cage(nsmall+1:nsmall+nlarge) = n_per_large
        do i = 1, nlarge
            read(cagefile,*)cage_waters(1:n_per_large,i+nsmall)
        end do
        close(cagefile)
    
    end subroutine


    subroutine sample_occupancy(species, box, unit)
        type(molecule_species), dimension(:) :: species
        real(kind=mc_prec), dimension(3) :: box
        integer :: unit

        call update_centers(species(water_index)%atom(1)%pos%X,&
                            species(water_index)%atom(1)%pos%Y,&
                            species(water_index)%atom(1)%pos%Z,&
                            box)
    
        call find_nearest_N_cages(Nc = 2, Nm=species(guest_index)%nmol,&
                                  x = species(guest_index)%atom(1)%pos%X,&
                                  y = species(guest_index)%atom(1)%pos%Y,&
                                  z = species(guest_index)%atom(1)%pos%Z,&
                                  box = box)

        call set_occupied_cage(Nm=species(guest_index)%nmol)

        write(unit,'(64i10)') guest_molecules_per_cage

    end subroutine

    subroutine set_occupied_cage(Nm)
    
        integer :: i, j, Nm
        real(kind=mc_prec) :: d1, d2
        integer :: ad1, ad2

        guest_molecules_per_cage = 0
        which_guest_molecules_in_cage = -1
        do i = 1, Nm
            ad1 = nearest_two(1,i)
            ad2 = nearest_two(2,i)
            d1 = merge(large_radius_squared,small_radius_squared,is_large(ad1))
            d2 = merge(large_radius_squared,small_radius_squared,is_large(ad2))
            if(nearest_two_distance(1,i).lt.d1) then
                guest_molecules_per_cage(ad1) = guest_molecules_per_cage(ad1) +1
                which_guest_molecules_in_cage(guest_molecules_per_cage(ad1),ad1) = i
            else if (nearest_two_distance(2,i).lt.d2) then
                guest_molecules_per_cage(ad2) = guest_molecules_per_cage(ad2) +1
                which_guest_molecules_in_cage(guest_molecules_per_cage(ad2),ad2) = i
            else
                stop 'everything sucks ass'
            end if
                        
        end do
    
    end subroutine
    

    subroutine find_nearest_N_cages(Nc,Nm,x,y,z,box)
        integer :: Nc, Nm
        real(kind=mc_prec), dimension(:) :: X, Y, Z 
        real(kind=mc_prec), dimension(3) :: box
        real(kind=mc_prec) :: d2(Nm)

        integer :: i,j,k
    
        do i = 1, n_cages
            d2(1:Nm) = distsq(c_X(i),c_Y(i),c_Z(i),&
                        x(1:Nm),y(1:Nm),z(1:Nm),box)   
            do j = 1, Nm
                do k = 1, Nc
                    if(d2(j).lt.nearest_two_distance(k,j))then
                        if(.not.k.eq.Nc)then
                            nearest_two_distance(k+1:Nc,j)=&
                                cshift(nearest_two_distance(k+1:Nc,j),-1)
                            nearest_two(k+1:Nc,j)=&
                                cshift(nearest_two(k+1:Nc,j),-1)
                        end if
                        nearest_two_distance(k,j) = d2(j)
                        nearest_two(k,j) = i
                    end if
                end do
            end do
        end do
    
    end subroutine
    
    

    subroutine update_centers(x,y,z,box)
        
        integer :: i,j
        real(kind=mc_prec) :: d_X, d_Y, d_Z 
        real(kind=mc_prec), dimension(:) :: X, Y, Z 
        real(kind=mc_prec), dimension(3) :: box
        c_X = 0._mc_prec; c_Y = 0._mc_prec; c_Z = 0._mc_prec;
        do i = 1, n_cages
        
            do j = 1, water_molecules_per_cage(i)
                !This needs to take boundary conditions into account
                d_X = x(cage_waters(j,i)) - c_X(i)
                d_Y = y(cage_waters(j,i)) - c_Y(i)
                d_Z = z(cage_waters(j,i)) - c_Z(i)
                
                d_X = merge(d_X-box(1),&
                            merge(d_X+box(1),d_X,-d_X.gt.HALF*box(1)),&
                            d_X.gt.HALF*box(1))
                d_Y = merge(d_Y-box(2),&
                            merge(d_Y+box(2),d_Y,-d_Y.gt.HALF*box(2)),&
                            d_Y.gt.HALF*box(2))
                d_Z = merge(d_Z-box(3),&
                            merge(d_Z+box(3),d_Z,-d_Z.gt.HALF*box(3)),&
                            d_Z.gt.HALF*box(3))

                c_X(i) = (c_X(i)*(j-1) + d_X)/j
                c_Y(i) = (c_Y(i)*(j-1) + d_Y)/j
                c_Z(i) = (c_Z(i)*(j-1) + d_Z)/j

            end do

        end do
    
    end subroutine 


end module
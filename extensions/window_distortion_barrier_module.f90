module window_distortion_barrier_module
    use precision_module
    use simulation_utilities_module, only: distsq
    use barrier_module
    implicit none

    type, extends(barrier) :: window_distortion_barrier

        real(kind=mc_prec) :: distortion
        real(kind=mc_prec), dimension(:), allocatable :: bondlengths
        real(kind=mc_prec), dimension(:), allocatable :: delta_dist
        integer, dimension(:), allocatable :: ind1,ind2

        real(kind=mc_prec) :: distbar

    contains

        procedure :: allowed => distortion_allowed
        procedure :: sample => distortion_sample
        procedure :: update => distortion_update
        procedure :: printinfo => distortion_print
        procedure :: wrong_distortion
        procedure :: bias_value => window_bias_value
               

    end type

    interface window_distortion_barrier

        procedure :: new_window_distortion_barrier

    end interface

contains

    function new_window_distortion_barrier(ind1,ind2, constrain,distbar,species,box) result(win)

        integer, dimension(:) :: constrain, ind1, ind2
        type(molecule_species), dimension(:) :: species
        real(kind=mc_prec), dimension(3) :: pos1, pos2,box
        real(kind=mc_prec) :: distbar
        type(window_distortion_barrier) :: win

        integer :: nind1,nind2,nconstrain
        integer :: i, t1, t2
        t1 = 1; t2 = 1

        nind1 = size(ind1)
        nind2 = size(ind2)
        nconstrain = size(constrain)

        allocate(win%ind1(nind1))
        allocate(win%ind2(nind2))
        allocate(win%delta_dist(nind2))
        allocate(win%bondlengths(nind2))
        allocate(win%constrain(nconstrain))

        win%ind1 = ind1
        win%ind2 = ind2
        win%constrain = constrain

        win%distbar = distbar

        win%distortion = 0.d0 !Set window distortion equal to 0 - assumes that the initial "distortion" (i.e. ring configuration) is the relaxed state 
        do i = 1, size(win%ind1)
            pos1 = (/species(t1)%atom(1)%pos%X(ind1(i)),&
                     species(t1)%atom(1)%pos%Y(ind1(i)),&
                     species(t1)%atom(1)%pos%Z(ind1(i))/)
            pos2 = (/species(t2)%atom(1)%pos%X(ind2(i)),&
                     species(t2)%atom(1)%pos%Y(ind2(i)),&
                     species(t2)%atom(1)%pos%Z(ind2(i))/)
            win%bondlengths(i) = sqrt(distsq(pos1,pos2,box))
        end do

        return

    end function

    real(kind=mc_prec) function window_bias_value(bar, distortion)
        class(window_distortion_barrier) :: bar
        real(kind=mc_prec) :: distortion

        ! window_bias_value = get_bias(bar%distortion)

        !First just try a simple bias i guess

        if (bar%distortion.lt.6._mc_prec) then
        
            window_bias_value = (6._mc_prec - distortion)*4._mc_prec
        
        else

            window_bias_value = 0._mc_prec

        end if

    end function window_bias_value

    function distortion_allowed(bar,index,typind,frompos,topos,species,box, delta_bias) result(allowed)

        !Returns true if the move does not cause too big of a change to the window distortion
        class(window_distortion_barrier) :: bar
        real(kind=mc_prec), dimension(3) :: frompos,topos,box
        real(kind=mc_prec) :: delta_bias
        integer :: index, typind
        logical :: allowed
        type(molecule_species), dimension(:) :: species

        integer :: i


        !Somewhat redundant formulation
        allowed = .true.
        if(typind.eq.1)then
            do i = 1, size(bar%constrain)
                if(bar%constrain(i).eq.index)   allowed = .not.(bar%wrong_distortion(index,topos,species,box, delta_bias))
            end do
        end if

        return

    end function

    subroutine distortion_sample(bar,fileno, species,box)

        class(window_distortion_barrier) :: bar
        real(kind=mc_prec), dimension(3) :: box
        integer :: fileno
        type(molecule_species), dimension(:) :: species

        write(fileno,*) bar%distortion, bar%bias_value(bar%distortion)


    end subroutine

    subroutine distortion_update(bar,index,typind,frompos,topos,box)

        class(window_distortion_barrier) :: bar
        integer :: index, i, typind
        real(kind=mc_prec), dimension(3) :: frompos,topos,box !Comply with parent class, not used

        if(typind.eq.1)then
            do i = 1, size(bar%ind1)
                if(bar%ind1(i) .eq. index) then
                    bar%distortion = bar%distortion + bar%delta_dist(i)
                    bar%bondlengths(i) = bar%bondlengths(i) + bar%delta_dist(i)
                end if
            end do

            do i = 1, size(bar%ind2)
                if(bar%ind2(i) .eq. index) then
                    bar%distortion = bar%distortion + bar%delta_dist(i)
                    bar%bondlengths(i) = bar%bondlengths(i) + bar%delta_dist(i)
                end if
            end do
        end if

    end subroutine

    subroutine distortion_print(bar)

        class(window_distortion_barrier) :: bar

        write(*,*) bar%distbar
        write(*,*) bar%constrain
        write(*,*) bar%distortion
        write(*,*) bar%delta_dist
        write(*,*) bar%bondlengths
        write(*,*) bar%ind1
        write(*,*) bar%ind2

    end subroutine

    logical function wrong_distortion(bar,index,topos,species,box,delta_bias)

        class(window_distortion_barrier) :: bar
        integer :: i, j, index
        real(kind=mc_prec) :: tot_added_dist, delta_bias
        type(molecule_species), dimension(:) :: species
        real(kind=mc_prec), dimension(3) :: topos,box
        real(kind=mc_prec), dimension(3) :: curpos1, curpos2


        tot_added_dist = 0.d0
        do i = 1, size(bar%ind1)
            if(bar%ind1(i) .eq. index) then
                curpos2 = (/species(1)%atom(1)%pos%X(bar%ind2(i)),&
                            species(1)%atom(1)%pos%Y(bar%ind2(i)),&
                            species(1)%atom(1)%pos%Z(bar%ind2(i))/)
                bar%delta_dist(i) = sqrt(distsq(topos,curpos2,box)) - bar%bondlengths(i)
                tot_added_dist = tot_added_dist + bar%delta_dist(i)
            end if
        end do

        do i = 1, size(bar%ind2)
            if(bar%ind2(i) .eq. index) then
                curpos1 = (/species(1)%atom(1)%pos%X(bar%ind1(i)),&
                            species(1)%atom(1)%pos%Y(bar%ind1(i)),&
                            species(1)%atom(1)%pos%Z(bar%ind1(i))/)
                bar%delta_dist(i) = sqrt(distsq(topos,curpos1,box)) - bar%bondlengths(i)
                tot_added_dist = tot_added_dist + bar%delta_dist(i)
            end if
        end do

        !Dumb hack time
        delta_bias = delta_bias + bar%bias_value(bar%distortion) - bar%bias_value(bar%distortion+tot_added_dist)
        !Make sure the window stays as distorted as before
        if(bar%distortion.gt.bar%distbar) then
            wrong_distortion = (bar%distortion + tot_added_dist .lt. bar%distbar)
        else
            wrong_distortion = (bar%distortion + tot_added_dist .gt. bar%distbar)
        end if

        return

    end function

end module

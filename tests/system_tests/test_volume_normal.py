import unittest

from test_scripts.outputComparisons import OutputComparisonTest
from test_scripts.energyComparisons import EnergyComparisonTest
from test_scripts.volumeComparisons import VolumeComparisonTest
from test_scripts.configurationComparisons import ConfigComparisonTest
from test_scripts.runTARMGasS import run_TARM_GasS

class test_volume_normal(unittest.TestCase, OutputComparisonTest, EnergyComparisonTest, VolumeComparisonTest, ConfigComparisonTest):
    @classmethod
    def setUpClass(self):
        test_folder = "methane_hydrate_volume_changes"
        input_file = f"{test_folder}/trial_topol.promp"
        output_folder = f"{test_folder}/test_output"
        run_TARM_GasS(output_folder, input_file)

        self.setupOutputComparison(f"{output_folder}/output", f"{test_folder}/expected_output/output")
        self.setupEnergyComparison(f"{output_folder}/energies", f"{test_folder}/expected_output/energies")
        self.setupVolumeComparison(f"{output_folder}/volume", f"{test_folder}/expected_output/volume")
        self.setupConfigComparison([f"{output_folder}/EndCON_1.xyz"], [f"{test_folder}/expected_output/EndCON_1.xyz"])

from test_scripts.arrayComparisonFunctions import withinAcceptedLimits
import logging

class EnergyComparisonTest(object):
    @classmethod
    def setupEnergyComparison(self, testEnergyFile, expectedEnergyFile):
        try:
            with open(testEnergyFile, "r") as testFile:
                self.sampledEnergy = [[float(e) for e in line.strip().split()] for line in testFile.read().splitlines()]

            with open(expectedEnergyFile, "r") as expectedFile:
                self.expectedEnergy = [[float(e) for e in line.strip().split()] for line in expectedFile.read().splitlines()]

        except IOError as e:
            logging.error("Error when attempting to read sampled energy! \n")
            raise

    def test_sampledEnergy(self):
        cycle=0
        for testEnergies, expectedEnergies in zip(self.sampledEnergy, self.expectedEnergy):
            cycle += 1
            self.assertTrue(withinAcceptedLimits(testEnergies, expectedEnergies, 0.00001), msg="Energy diverged from expected in cycle " + str(cycle))
import logging
import re
import os
import unittest
from test_scripts.arrayComparisonFunctions import withinAcceptedLimits
from test_scripts.arrayComparisonFunctions import softlyLessThan

class OutputComparisonTest(object):
    @classmethod
    def setupOutputComparison(self, testOutputFile, expectedOutputFile):
        try:
            with open(testOutputFile, "r") as testFile:
                self.testLog = [line.strip() for line in testFile.read().splitlines()]
                with open(expectedOutputFile, "r") as expectedFile:
                    self.expectedLog = [line.strip() for line in expectedFile.read().splitlines()]
        except IOError as e:
            logging.error("Error when attempting to read output logs! \n")
            raise

    def test_TotalEnergyEnd(self):
        regex=r'^TOTAL ENERGY END:(.*)kcal\/mol'
        testEnergy = getFirstMatchAsArrayOfFloats(regex, self.testLog)
        expectedEnergy = getFirstMatchAsArrayOfFloats(regex, self.expectedLog)
        self.assertTrue(withinAcceptedLimits(testEnergy, expectedEnergy, 0.00001), msg="Failing test because the total energy at the end is different from expected!")

    def test_TotalEnergyEndIsSeflConsistent(self):
        regexTotal=r'^TOTAL ENERGY END:(.*)kcal\/mol'
        regexConsistent=r'^TOTAL ENERGY CONSISTENCY CHECK:(.*)kcal\/mol'
        testEnergy = getFirstMatchAsArrayOfFloats(regexTotal, self.testLog)
        consistentEnergy = getFirstMatchAsArrayOfFloats(regexConsistent, self.testLog)
        self.assertTrue(withinAcceptedLimits(testEnergy, consistentEnergy, 0.00001), msg="Failing test because the total energy at the end is not self-consistent!")


    @unittest.skipIf(os.getenv('TARMGASS_TEST_SKIP_TIMING', False), "TARMGASS_TEST_SKIP_TIMING has been set to True")
    def test_ExecutionTimingIsntMessedUpAndBad(self):
        regex=r'^Time = (.*)seconds'
        testTime = getFirstMatchAsArrayOfFloats(regex, self.testLog)
        acceptableTime = getFirstMatchAsArrayOfFloats(regex, self.expectedLog)
        self.assertTrue(softlyLessThan(testTime, acceptableTime, leeway=0.1), 
                        msg=f"Failing test because runtime {testTime} s is greater than permitted ({acceptableTime} s)!")

    def test_WarningsMatch(self):
        regex=r'^Warning:(.*)'
        testWarnings = getArrayOfMatchingLines(regex, self.testLog)
        expectedWarnings = getArrayOfMatchingLines(regex, self.expectedLog)
        self.assertEqual(testWarnings, expectedWarnings)

def getFirstMatchAsArrayOfFloats(regex, lines):
    return_array = []
    for line in lines:
        matches = re.match(regex, line)
        if(matches):
            return_array += [float(match) for match in matches.group(1).split()]
    return return_array

def getArrayOfMatchingLines(regex, lines):
    return_array = []
    for line in lines:
        matches = re.match(regex, line)
        if(matches):
            return_array += matches.group(0)

    return return_array

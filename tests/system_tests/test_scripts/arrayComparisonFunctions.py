import logging
def withinAcceptedLimits(testArray, expectedArray, limit):
    errorMessages = ["Expected {} +/- {}, but found {}".format(e,limit,t) for t,e in zip(testArray, expectedArray) if limit < abs(t - e) ]

    for msg in errorMessages:
        logging.error(msg)

    return len(errorMessages) == 0

def softlyLessThan(testArray, expectedArray, leeway=0.0):
    errorMessages = ["Expected less than {}, but found {}".format(e*(1. + leeway),t) for t,e in zip(testArray, expectedArray) if t > e*(1. + leeway) ]

    for msg in errorMessages:
        logging.error(msg)

    return len(errorMessages) == 0
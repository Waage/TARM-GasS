import logging
import re
from test_scripts.arrayComparisonFunctions import withinAcceptedLimits

class ConfigComparisonTest(object):
    @classmethod
    def setupConfigComparison(self, testConfigFiles, expectedConfigFiles):
        self.testConfigs = []
        self.expectedConfigs = []

        try:
            for testFile in testConfigFiles:
                with open(testFile, "r") as file:
                    self.testConfigs.append(xyzConfig(file))

            for expectedFile in expectedConfigFiles:
                with open(expectedFile, "r") as file:
                    self.expectedConfigs.append(xyzConfig(file))
        
        except IOError as e:
            logging.error("Error when attempting to read sampled configuration! \n")
            raise

    def test_EqualConfigurations(self):
        for test, expected in zip(self.testConfigs, self.expectedConfigs):
            self.assertEqual(test, expected, msg="End configuration is different from expected!")

class xyzConfig(object):
    def __init__(self, opened_file): #Just gonna go ahead and assume that the coordinate format is valid
        super().__init__()
        self.number_of_atoms = int(opened_file.readline())

        extended = opened_file.readline().strip()
        lattice, properties = re.match(r'Lattice="(.*)"(Properties.*)', extended).group(1,2)

        self.lattice = [float(x) for x in lattice.split()]
        self.coordinates = []

        for i in range(self.number_of_atoms):
            (atom, x, y, z) = opened_file.readline().split()
            self.coordinates.append([ float(x), float(y), float(z) ])

    def __eq__(self, other):
        if self.number_of_atoms != other.number_of_atoms:
            logging.error("Expected {} but got {}".format(self.number_of_atoms, other.number_of_atoms))
            return False

        if not withinAcceptedLimits(self.lattice, other.lattice, 0.000001):
            return False

        for myPos, yourPos in zip(self.coordinates, other.coordinates):
            if not withinAcceptedLimits(myPos, yourPos, 0.000001):
                return False

        return True

    def __ne__(self, other):
        return not self.__eq__(other)


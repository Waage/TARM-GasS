from test_scripts.arrayComparisonFunctions import withinAcceptedLimits
import logging

class MolNoComparisonTest(object):
    @classmethod
    def setupMolNoComparison(self, testMolNoFile, expectedMolNoFile):
        try:
            with open(testMolNoFile, "r") as testFile:
                self.sampledMolNo = [[float(e) for e in line.strip().split()] for line in testFile.read().splitlines()]

            with open(expectedMolNoFile, "r") as expectedFile:
                self.expectedMolNo = [[float(e) for e in line.strip().split()] for line in expectedFile.read().splitlines()]

        except IOError as e:
            logging.error("Error when attempting to read sampled MolNo! \n")
            raise

    def test_sampledMolNo(self):
        cycle=0
        for testMoleculeNumbers, expectedMoleculeNumbers in zip(self.sampledMolNo, self.expectedMolNo):
            cycle += 1
            self.assertTrue(withinAcceptedLimits(testMoleculeNumbers, expectedMoleculeNumbers, 0.00001), msg="Molecule numbers diverged from expected in cycle " + str(cycle))
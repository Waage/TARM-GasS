import os
import subprocess

def run_TARM_GasS(output_folder, input_file):
    TARM_GasS_exe = os.getenv('TARMGASS_EXECUTABLE')
    os.makedirs(output_folder, exist_ok=True)
    subprocess.call([TARM_GasS_exe, "--input_file", input_file, "--output_directory", output_folder])
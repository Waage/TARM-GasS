from test_scripts.arrayComparisonFunctions import withinAcceptedLimits
import logging

class VolumeComparisonTest(object):
    @classmethod
    def setupVolumeComparison(self, testVolumeFile, expectedVolumeFile):
        try:
            with open(testVolumeFile, "r") as testFile:
                self.sampledVolume = [[float(v) for v in line.strip().split()] for line in testFile.read().splitlines()]

            with open(expectedVolumeFile, "r") as expectedFile:
                self.expectedVolume = [[float(v) for v in line.strip().split()] for line in expectedFile.read().splitlines()]

        except IOError as e:
            logging.error("Error when attempting to read sampled volume! \n")
            raise

    def test_sampledVolume(self):
        cycle=0
        for testVolume, expectedVolume in zip(self.sampledVolume, self.expectedVolume):
            cycle += 1
            self.assertTrue(withinAcceptedLimits(testVolume, expectedVolume, 0.00001), msg="Volume diverged from expected in cycle " + str(cycle))
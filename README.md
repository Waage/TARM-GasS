TARM-GasS is a program for simulating gas molecules and other smallish molecules using molecular Monte Carlo simulations.

## Features/code goals:
* Core interactions fully SIMD-vectorized
* Concise code.
* Code actually organized into folders and stuff.

## Installing
Via command line from this directory, execute the following commands:

```
mkdir build
cd build
cmake ..
make
make install
```

## Building in debug mode
Instead of `cmake ..`, run `cmake .. --DCMAKE_BUILD_TYPE=Debug`
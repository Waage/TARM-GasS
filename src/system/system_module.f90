module system_module
    !Implementation of the system type
    use direct_energy_storage_module, only: direct_energy_storage
    use ewald_module, only: ewald_type
    use general_utilities_module, only: resize_0_indexed_2D
    use geometry_module, only: box_type
    use lennard_jones_module, only: lennard_jones_interaction
    use molecule_species_module, only: molecule_species
    use parameters_module, only: storage_increment, safety_pad
    use precision_module, only: mc_prec
    use reduced_dimensionality_energy_storage_module, only: reduced_dimensionality_energy_storage
    implicit none
    private

    public :: system

    type system
        
        !Contains system - specific information such as e.g. molecule, box dimensions, interaction energies, etc
        !(Inverse) temperature (times gas constant), pressure, volume
        real(kind=mc_prec) :: beta, pressure, V
        !Boundary conditions of system
        class(box_type), allocatable :: box
        ! (squared) cutoff radius
        real(kind=mc_prec) :: rc, rc2
        !Total number of molecule in the system, number of fractional components
        integer :: totmol
        !Positions and information on all the molecule species present in the system    
        type(molecule_species), dimension(:), allocatable :: species
        !Total energy of the system
        real(kind=mc_prec) :: total_energy
        !Direct interaction energy between all molecule of the system
        type(direct_energy_storage), allocatable :: energy_storage 

        type(lennard_jones_interaction) :: lennard_jones
        class(ewald_type), allocatable :: coulombic

    contains

        procedure :: set_n_types
        procedure :: update_boundary
        procedure :: reinsert
        procedure :: set_frac
        procedure :: set_nmol
        procedure :: update_energy_storage
        procedure :: change_nmol
        procedure :: update_running_difference
        procedure :: increase_size
        procedure :: remove_molecule => remove_molecule_from_system
        procedure :: is_valid

    end type

contains

    subroutine set_n_types(sys, n_types)
    
        class(system) :: sys
        integer, intent(in) :: n_types
        
        allocate(sys%species(n_types))

    end subroutine

    subroutine update_boundary(sys, inp_boundary)

        class(system) :: sys
        real(kind=mc_prec), intent(in), optional, dimension(3, 3) :: inp_boundary

        if(present(inp_boundary))then
            call sys%box%update_boundary(inp_boundary)
        end if
        sys%V = sys%box%get_volume()

        if(any(sys%species(1:size(sys%species))%ncharged > 0))then
            call sys%coulombic%update_boundary(sys%box)
        end if
    end subroutine

    subroutine reinsert(sys)
        !Reinsert all molecules of the system into the volume defined by the boundary box
        class(system) :: sys
        integer :: i

        do i = 1, size(sys%species)
            call sys%species(i)%reinsert(sys%box)
        end do
    end subroutine

    subroutine set_nmol(sys, which, val)
        class(system) :: sys
        integer :: which, val

        sys%totmol = sys%totmol + val - sys%species(which)%nmol
        call sys%species(which)%set_nmol(val)

    end subroutine

    subroutine set_frac(sys, which, val)
        class(system) :: sys
        integer :: which
        real(kind=mc_prec) :: val

        sys%species(which)%frac = val
        sys%species(which)%frac5 = val**5

    end subroutine

    subroutine remove_molecule_from_system(sys, specno, molno)
        !Handles the removal of molecule number molno from the system sys. Does not deal with arrays that are not members of sys.
        !If you need to also alter other arrays, be aware that this function will alter sys%totmol.
        !Energy changes are presumed to already have been altered
        use general_utilities_module, only: overwrite_array_col, overwrite_array_row
        class(system) :: sys
        integer, intent(in) :: specno, molno

        integer :: i, nmol

        nmol = sys%species(specno)%nmol

        !Overwrite energy storage
        call sys%energy_storage%remove_molecule(remove_type = specno, remove_index = molno, nmol = nmol)

        !Remove the molecule
        do i = 1, sys%species(specno)%nats
            sys%species(specno)%atom(i)%pos%X(molno:nmol) =&
             CSHIFT(sys%species(specno)%atom(i)%pos%X(molno:nmol), 1)

            sys%species(specno)%atom(i)%pos%Y(molno:nmol) =&
             CSHIFT(sys%species(specno)%atom(i)%pos%Y(molno:nmol), 1)
             
            sys%species(specno)%atom(i)%pos%Z(molno:nmol) =&
             CSHIFT(sys%species(specno)%atom(i)%pos%Z(molno:nmol), 1)
        end do

        call sys%change_nmol(specno, -1)
        
    end subroutine

    subroutine update_energy_storage(sys, specno, molno, new_storage, coulombic)

        class(system) :: sys
        integer, intent(in) :: specno, molno
        type(reduced_dimensionality_energy_storage) :: new_storage
        class(ewald_type), allocatable, optional :: coulombic

        call sys%energy_storage%update(species_index = specno, molecule_index = molno, new_storage = new_storage)

        if(present(coulombic)) then
            if(allocated(coulombic)) then
                call move_alloc(to = sys%coulombic, from = coulombic)
            end if
        end if
     
    end subroutine

    subroutine change_nmol(sys, specno, howmuch)

        class(system) :: sys
        integer :: specno, howmuch

        sys%species(specno)%nmol = sys%species(specno)%nmol + howmuch
        sys%totmol = sys%totmol + howmuch
        if(sys%species(specno)%nmol > sys%species(specno)%storage_space - safety_pad - 1)then
            !Make sure there is enough memory for future molecules
            call sys%increase_size(specno)
        end if

    end subroutine

    subroutine increase_size(sys, t)
        class(system) :: sys
        integer, intent(in) :: t

        call sys%energy_storage%increase_size(t)

        call sys%species(t)%increase_size()
        sys%species(t)%storage_space = sys%species(t)%storage_space + storage_increment
    end subroutine

    subroutine update_running_difference(sys, dele)
        !Update the energy
        class(system) :: sys
        real(kind=mc_prec), intent(in) :: dele
        
        sys%total_energy = sys%total_energy + dele
    
    end subroutine

    elemental logical function is_valid(sys)
        class(system), intent(in) :: sys
        is_valid = sys%box%is_valid(sys%rc)
    end function

end module

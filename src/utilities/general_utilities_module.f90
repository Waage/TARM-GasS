module general_utilities_module
    use precision_module
    implicit none

contains

    pure function cross_product(a, b) result(c)
        !Returns the cross prodcut of 3 - vectors a and b
        real(kind=mc_prec), dimension(3), intent(in) :: a, b
        real(kind=mc_prec), dimension(3) :: c
    
        c(1) = a(2)*b(3) - b(2)*a(3)
        c(2) = a(3)*b(1) - b(3)*a(1)
        c(3) = a(1)*b(2) - b(1)*a(2)

        return
    end function

    elemental logical function is_zero(arg, opt_tolerance)
        !Tests if a real number is zero (for our purposes)
        logical :: bool
        real(kind=mc_prec), intent(in), optional :: opt_tolerance
        real(kind=mc_prec), parameter :: default_tolerance = 1E-8
        real(kind=mc_prec) :: tolerance
        real(kind=mc_prec), intent(in) :: arg
        if (present(opt_tolerance)) then
            tolerance = opt_tolerance
        else
            tolerance = default_tolerance
        end if
        
        is_zero = (arg < tolerance).and.(arg > - tolerance)
    
        return
    end function

    elemental logical function is_nonzero(arg, opt_tolerance)
        real(kind=mc_prec), intent(in) :: arg
        real(kind=mc_prec), intent(in), optional :: opt_tolerance
        is_nonzero = .not. is_zero(arg, opt_tolerance)
    end function

    elemental logical function equal_enough(arg1, arg2, opt_tolerance)
        real(kind=mc_prec), intent(in) :: arg1, arg2
        real(kind=mc_prec), intent(in), optional :: opt_tolerance

        equal_enough = is_zero(arg1 - arg2, opt_tolerance)

    end function

    pure function first_multiple_greater(x, y) result(outval)
        integer :: outval
        integer, intent(in) :: x, y
        outval = int(x/y)*y + y    
        return
    end function

    pure subroutine overwrite_array_row(array, start1, end1, direction)
        real(kind=mc_prec), dimension(0:, 0:), intent(inout) :: array
        integer, intent(in) :: start1, end1, direction
        array(:, start1:end1) = CSHIFT(array(:, start1:end1), shift=direction, dim=2)

    end subroutine

    pure subroutine overwrite_array_col(array, start1, end1, direction)
        real(kind=mc_prec), dimension(0:, 0:), intent(inout) :: array
        integer, intent(in) :: start1, end1, direction
        array(start1:end1, :) = CSHIFT(array(start1:end1, :), shift=direction, dim=1)

    end subroutine

    pure subroutine resize_0_indexed_2D(array, old_shape, new_shape)
        real(kind=mc_prec), dimension(:, :), intent(inout), allocatable :: array
        integer, dimension(2), intent(in) :: old_shape, new_shape
        real(kind=mc_prec), dimension(:, :), allocatable :: tmp_array
        
        allocate(tmp_array(0:new_shape(1) - 1, 0:new_shape(2) - 1))
        tmp_array(0:old_shape(1) - 1, 0:old_shape(2) - 1) = array(0:old_shape(1) - 1, 0:old_shape(2) - 1)
        tmp_array(old_shape(1):new_shape(1) - 1, old_shape(2):new_shape(2) - 1) = 0._mc_prec
        deallocate(array)
        call move_alloc(tmp_array, array)   
    
    end subroutine

    pure subroutine resize_0_indexed_1D(array, old_shape, new_shape)
        real(kind=mc_prec), dimension(:), intent(inout), allocatable :: array
        integer, intent(in) :: old_shape, new_shape
        real(kind=mc_prec), dimension(:), allocatable :: tmp_array
        
        allocate(tmp_array(0:new_shape - 1))
        tmp_array(0:old_shape - 1) = array(0:old_shape - 1)
        tmp_array(old_shape:new_shape - 1) = 0._mc_prec
        deallocate(array)
        call move_alloc(tmp_array, array)   
    
    end subroutine

    subroutine pass_or_abort(condition, message)
        logical, intent(in) :: condition
        character(*), intent(in) :: message
        if(.not. condition) then
            write(*, *) message
            stop 1
        end if

    end subroutine

end module

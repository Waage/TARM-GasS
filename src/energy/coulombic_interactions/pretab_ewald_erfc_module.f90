module pretab_ewald_erfc_module
    !Not actually erfc, but erfc(alpha*r)/r
    use precision_module, only: mc_prec
    implicit none
    private

    public :: erfc_lookup

    type erfc_lookup  
        real(kind=mc_prec), dimension(:), allocatable :: array
        real(kind=mc_prec) :: rinv
    contains
        procedure :: lookup
    end type

    public:: new_erfc_lookup

    interface erfc_lookup
        module procedure :: new_erfc_lookup
    end interface

contains

    pure function new_erfc_lookup(alpha, rc) result(table)
        type(erfc_lookup) :: table
        real(kind=mc_prec), intent(in) :: rc, alpha
        real(kind=mc_prec) :: x
        integer :: i, ngrid

        ngrid = max(1000, int(rc/0.01_mc_prec + 0.5_mc_prec) + 4)
        !Initialize the lookup table
        allocate(table%array(0:3*ngrid)) !Add padding at the start and end in order to allow for 0 - and negative values, as well as values well beyond rcut
                                                                        !Those values should result in non - sense values, but that should be corrected for somewhere else
                                                                        !E.g. by explicitly rejecting values within a hard sphere cutoff, setting those values to 0, etc
        table%rinv = rc/(ngrid - 4)
        table%array = 0._mc_prec
        do i = 1, ngrid
            x = real(i, mc_prec)*table%rinv
            table%array(i) = erfc(alpha*x)/x
        end do
        table%rinv = 1._mc_prec/table%rinv

    end function

    elemental function lookup(table, x) result(outval)
        class(erfc_lookup), intent(in) :: table
        real(kind=mc_prec), intent(in) :: x
        integer :: index
        real(kind=mc_prec) :: outval
        real(kind=mc_prec) :: intpolpos
        real(kind=mc_prec) :: vk0, vk1, vk2

        !Looks up the value of erfc(alpha*x)/x in a precomputed table, 
        !interpolates with a cubic spline
        intpolpos = x*table%rinv
        index = int(intpolpos)
        intpolpos = intpolpos - real(index, mc_prec)
        vk0=table%array(index)
        vk1=table%array(index + 1)
        vk2=table%array(index + 2)

        outval=(vk0 + intpolpos*((vk1 - vk0) + ((vk0 - vk1) + (vk2 - vk1))*(intpolpos - 1._mc_prec)*.5_mc_prec) )
    end function

end module

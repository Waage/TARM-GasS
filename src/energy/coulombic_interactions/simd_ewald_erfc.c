/*GENERAL DESCRIPTION/RATIONALE: The ewald lookup ewald_erfc_table does not (yet) auto-vectorize. 
This is unfortunate, as it is slows down the simulation significantly, and while newer intel compilers/hardware supposedly supply a vector-erfc,
the scalar erfc is roughly three or four times slower than a lookup - there is little reason to assume the vector version is any different.

Known deficiencies:
_mm256_extract_epi32 is appearantly not ubiquitous, so the program does not compile in single precision on the some older computers. 
Assumes everything is unaligned - this does not seem to be a problem with 'recent' hardware 
*/
#include "simd_ewald_erfc.h"


//c.f. GROMACS' GMX_MM256_FULLTRANSPOSE4_PD
//Load four row vectors in the lookup table (one for each index value), then transpose them - we now have 4 column vectors. However, 
//only three are needed

#if N_PER_VEC == 4

    void get_ewald_lookup_values(c_real *table, c_simd_int v_index, c_simd_real* v_k0, c_simd_real* v_k1, c_simd_real* v_k2)
    {
                *v_k0 = simd_load_unaligned( table + simd_extract(v_index,0) );
                *v_k1 = simd_load_unaligned( table + simd_extract(v_index,1) );
                *v_k2 = simd_load_unaligned( table + simd_extract(v_index,2) );
                c_simd_real v_tmp = simd_load_unaligned( table + simd_extract(v_index,3) );
                c_simd_real v_tt0  = simd_unpack_low_lane((*v_k0), (*v_k1)); /* (0vk0), (0vk1), (0vk2), (0vk3)       (0vk0), (1vk1), (0vk2), (1vk3)*/
                c_simd_real v_tt1  = simd_unpack_high_lane((*v_k0), (*v_k1)); /* (1vk0), (1vk1), (1vk2), (1vk3)  -->  (0vk1), (1vk0), (0vk3), (1vk2)*/
                c_simd_real v_tt2  = simd_unpack_low_lane((*v_k2), (v_tmp)); /* (2vk0), (2vk1), (2vk2), (2vk3)       (2vk0), (3vk1), (2vk2), (3vk3)*/
                c_simd_real v_tt3  = simd_unpack_high_lane((*v_k2), (v_tmp)); /* (3vk0), (3vk1), (3vk2), (3vk3)       (2vk1), (3vk0), (2vk3), (3vk2)*/
                *v_k0 = simd_permute(v_tt0, v_tt2, 0x20); /*(0vk0), (1vk1), (0vk2), (1vk3)        (0vk0), (1vk0), (2vk0), (3vk0)*/
                *v_k1 = simd_permute(v_tt1, v_tt3, 0x20); /*(0vk1), (1vk0), (0vk3), (1vk2)   -->  (0vk1), (1vk0), (2vk1), (3vk1)*/
                *v_k2 = simd_permute(v_tt0, v_tt2, 0x31); /*(2vk0), (3vk1), (2vk2), (3vk3)       (0vk2), (1vk2), (2vk2), (3vk2)*/
    }
#elif N_PER_VEC == 8
    /* This is probably stupid and better replaced by a some vector intrinsic */
    /* (Shamelessly copied from stack overflow user Zboson's answer to http://stackoverflow.com/questions/25622745/transpose-an-8x8-float-using-avx-avx2) */
    /* We perform only a limited transpose, since we in the end only need the first three column vectors */
    /*Load a1 a2 a3 a4 a5 a6 a7 a8    a1 b1 a3 b3 a5 b5 a7 b7    a1 b1 c1 a3 a5 b5 c5 b7    a1 b1 c1 a3 e1 f1 g1 b7  
           b1 b2 b3 b4 b5 b6 b7 b8    a2 b2 a4 b4 a6 b6 a8 b8    a2 b2 c2 a4 a6 b6 c6 b8    a2 b2 c2 a4 e2 f2 g2 b8  
           c1 c2 c3 c4 c5 c6 c7 c8    c1 d1 c3 d3 c5 d5 c7 d7    a3 b3 c3 d3 a7 b7 c7 d7    a3 b3 c3 d3 e3 f3 g3 d7  
           d1 d2 d3 d4 d5 d6 d7 d8 to c2 d2 c4 d4 c6 d6 c8 d8 to a4 b4 c4 d4 a8 b8 c8 d8 to a4 b4 c4 d4 e4 f4 g4 d8   
           e1 e2 e3 e4 e5 e6 e7 e8    e1 f1 e3 f3 e5 f5 e7 f7    e1 f1 g1 f3 e5 f5 g5 f7    a5 b5 c5 f3 e5 f5 g5 f7  
           f1 f2 f3 f4 f5 f6 f7 f8    e2 f2 e4 f4 e6 f6 e8 f8    e2 f2 g2 f4 e6 f6 g6 f8    a6 b6 c6 f4 e6 f6 g6 f8  
           g1 g2 g3 g4 g5 g6 g7 g8    g1 h1 g3 h3 g5 h5 g7 h7    e3 f3 g3 h3 e7 f7 g7 h7    a7 b7 c7 h3 e7 f7 g7 h7  
           h1 h2 h3 h4 h5 h6 h7 h8    g2 h2 g4 h4 g6 h6 g8 h8    e4 f4 g4 h4 e8 f8 g8 h8    a8 b8 c8 h4 e8 f8 g8 h8   
    I.E: Note conspicuous lack of v_ttt3, v_ttt7, as those temporary variables are not needed to fill vk0, 1 or 2*/
    void get_ewald_lookup_values(c_real *table, c_simd_int v_index, c_simd_real* v_k0, c_simd_real* v_k1, c_simd_real* v_k2)
    {
                *v_k0 = simd_load_unaligned( table + simd_extract(v_index,0) );
                *v_k1 = simd_load_unaligned( table + simd_extract(v_index,1) );
                *v_k2 = simd_load_unaligned( table + simd_extract(v_index,2) );
                c_simd_real v_tmp3 = simd_load_unaligned( table + simd_extract(v_index,3) );
                c_simd_real v_tmp4 = simd_load_unaligned( table + simd_extract(v_index,4) );
                c_simd_real v_tmp5 = simd_load_unaligned( table + simd_extract(v_index,5) );
                c_simd_real v_tmp6 = simd_load_unaligned( table + simd_extract(v_index,6) );
                c_simd_real v_tmp7 = simd_load_unaligned( table + simd_extract(v_index,7) );
                c_simd_real v_tt0  = simd_unpack_low_lane(*v_k0, *v_k1);
                c_simd_real v_tt1  = simd_unpack_high_lane(*v_k0, *v_k1);
                c_simd_real v_tt2  = simd_unpack_low_lane(*v_k2, v_tmp3);
                c_simd_real v_tt3  = simd_unpack_high_lane(*v_k2, v_tmp3);
                c_simd_real v_tt4  = simd_unpack_low_lane(v_tmp4, v_tmp5);
                c_simd_real v_tt5  = simd_unpack_high_lane(v_tmp4, v_tmp5);
                c_simd_real v_tt6  = simd_unpack_low_lane(v_tmp6, v_tmp7);
                c_simd_real v_tt7  = simd_unpack_high_lane(v_tmp6, v_tmp7);
                c_simd_real v_ttt0  = simd_shuffle_bottop(v_tt0, v_tt2);
                c_simd_real v_ttt1  = simd_shuffle_topbot(v_tt0, v_tt2);
                c_simd_real v_ttt2  = simd_shuffle_bottop(v_tt1, v_tt3);
                c_simd_real v_ttt4  = simd_shuffle_bottop(v_tt4, v_tt6);
                c_simd_real v_ttt5  = simd_shuffle_topbot(v_tt4, v_tt6);
                c_simd_real v_ttt6  = simd_shuffle_bottop(v_tt5, v_tt7);
                *v_k0 = simd_permute(v_ttt0, v_ttt4, 0x20);
                *v_k1 = simd_permute(v_ttt1, v_ttt5, 0x20);
                *v_k2 = simd_permute(v_ttt2, v_ttt6, 0x20);
    }
#else
    #error N_PER_VEC has invalid value, only 4 and 8 is currently supported. Try compiling in double precision only!
#endif

c_simd_real perform_ewald_lookup(c_simd_real v_q1q2, c_simd_real v_distance_squared, c_simd_real v_inverse_erfc_table_scale, c_real *ewald_erfc_table)
{
    const c_simd_real v_ONE = simd_set_value(1.0);
    const c_simd_real v_HALF = simd_set_value(0.5);
    c_simd_real v_interpolation = simd_multiply_real(simd_square_root_real(v_distance_squared), v_inverse_erfc_table_scale);
    c_simd_int v_index = simd_convert_real_to_int_truncuate(v_interpolation);

    v_interpolation = simd_subtract_real(v_interpolation,simd_round_down_real(v_interpolation));

    c_simd_real v_k0, v_k1, v_k2;
    get_ewald_lookup_values(ewald_erfc_table, v_index, &v_k0, &v_k1, &v_k2);

    c_simd_real v_t0 = simd_subtract_real(v_k1, v_k0); //t0 = (vk1-vk0)
    c_simd_real v_t1 = simd_subtract_real(simd_subtract_real(v_k2, v_k1), v_t0); //t1 = (vk2-vk1-t0)
    v_t1 = simd_multiply_real(simd_multiply_real(simd_subtract_real(v_interpolation, v_ONE), v_HALF), v_t1);  // t1 = t1*(intpol-1._mc_prec)*.5_mc_prec

    return  simd_multiply_real(v_q1q2,simd_add_real(simd_multiply_real(v_interpolation, simd_add_real(v_t0, v_t1)), v_k0)); // el_stat=q1*q2*(vk0 + intpolpos*(t1 + t0) )
}
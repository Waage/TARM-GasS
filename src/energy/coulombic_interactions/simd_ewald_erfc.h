#ifndef SIMD_EWALD_ERFC_HEADER
#define SIMD_EWALD_ERFC_HEADER
#include "../simd_defines.h"

c_simd_real perform_ewald_lookup(c_simd_real v_q1q2, c_simd_real v_distance_squared, c_simd_real v_inverse_erfc_table_scale, c_real *ewald_erfc_table);
#endif
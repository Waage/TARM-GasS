module ewald_module
    !Contains subroutines for computing energy from ewald summation
    !The real - space part is contained seperately in non_bonded_interactions.f90
    !All routines are based on routines found in DL_POLY, GROMACS, etc
    use coulombic_interactions_module, only: coulombic_interaction
    use geometry_module, only: box_type
    use molecule_module, only: single_molecule
    use molecule_species_module, only: molecule_species
    use parameters_module, only: pi
    use precision_module, only: mc_prec
    use pretab_ewald_erfc_module, only: erfc_lookup
    implicit none
    private

    public :: ewald_type

    type, extends(coulombic_interaction) ::  ewald_type

        real(kind=mc_prec), dimension(:), private, allocatable :: ewald_self
        real(kind=mc_prec), dimension(:), private, allocatable :: internal_excluded

        !Ewald alpha parameter, reciprocal cut - off, prefactor that occurs in ewald summation
        real(kind=mc_prec), private :: alpha
        integer, dimension(3), private :: ncut
        real(kind=mc_prec), dimension(3), private :: prefac
        !Real/imaginary component of ewald fourier
        real(kind=mc_prec), dimension(:, :, :), private, allocatable :: rhore, rhoim
        !exp(k**2/(4alpha**2)/(k**2) lookup table
        real(kind=mc_prec), dimension(:, :, :), private, allocatable :: expfac
        !error function lookup table
        type(erfc_lookup) :: erfc_lookup

        contains

        procedure :: update_boundary

        procedure :: set_internal_corrections
        procedure :: set_ewald_self_correction

        procedure, nopass :: precompute_base_sines
        procedure, nopass :: precompute_sines_from_base
        procedure, nopass :: compute_fourier_energy_change

        procedure :: fourier_ewald_single_molecule
        procedure :: fourier_ewald_diff
        procedure :: fourier_ewald_total

        procedure :: correction_terms
    end type ewald_type

    interface ewald_type
        module procedure :: new_ewald_type
    end interface

contains

    pure function new_ewald_type(prototype, ewald_precision, rc, box) result(coul)
        class(ewald_type), allocatable :: coul
        class(single_molecule), dimension(:), intent(in) :: prototype
        real(kind=mc_prec), intent(in) :: ewald_precision, rc
        class(box_type), intent(in) :: box  
        integer :: n_types, max_n_atoms, i, j, ati, atj

        allocate(coul)
        n_types = size(prototype)
        max_n_atoms = 0
        do i = 1, n_types
            if(prototype(i)%nats > max_n_atoms) max_n_atoms = prototype(i)%nats
        end do
        allocate(coul%charge_charge(max_n_atoms, max_n_atoms, n_types, n_types))

        do i = 1, n_types
            do j = 1, n_types
                do ati = 1, prototype(i)%nats
                    do atj = 1, prototype(j)%nats
                        coul%charge_charge(atj, ati, j, i) = prototype(i)%charge(ati)*prototype(j)%charge(atj)
                    end do
                end do
            end do
        end do

        coul%alpha = ewald_convergence_parameter(ewald_precision, rc)
        coul%ncut = ewald_reciprocal_cutoff(ewald_precision, box, coul%alpha)
        allocate(coul%ewald_self(n_types), coul%internal_excluded(n_types))
        allocate(coul%expfac( -coul%ncut(3):coul%ncut(3), -coul%ncut(2):coul%ncut(2), 0:coul%ncut(1)))
        allocate(coul%rhore( -coul%ncut(3):coul%ncut(3), -coul%ncut(2):coul%ncut(2), 0:coul%ncut(1)))
        allocate(coul%rhoim( -coul%ncut(3):coul%ncut(3), -coul%ncut(2):coul%ncut(2), 0:coul%ncut(1)))
        coul%rhore = 0._mc_prec
        coul%rhoim = 0._mc_prec

        coul%erfc_lookup = erfc_lookup(coul%alpha, rc)

        call coul%set_ewald_self_correction()
        call coul%set_internal_corrections(prototype, box)

    end function

    pure subroutine set_ewald_self_correction(coul)
        class(ewald_type), intent(inout) :: coul
        integer :: i, j

        coul%ewald_self = 0._mc_prec
        do i = 1, size(coul%charge_charge, 4)
            do j = 1, size(coul%charge_charge, 2)
                coul%ewald_self(i) = coul%ewald_self(i) + coul%charge_charge(j, j, i, i)*coul%alpha/sqrt(pi)
            end do
        end do
    end subroutine

    !This should actually be called every time an internal degree of freedom is changed, 
    !so might want to think about how to deal with that

    !Probably best to make this a function that returns a per molecule value, then have external logic that handles 
    !Whether it needs to be called for all molecules/ every time/etc.

    !For now: Copmute and store in this class
    pure subroutine set_internal_corrections(coul, prototype, box)
        class(ewald_type), intent(inout) :: coul
        class(single_molecule), dimension(:), intent(in) :: prototype
        class(box_type), intent(in) :: box
        integer :: t1, at1, at2
        real(kind=mc_prec) :: rij

        coul%internal_excluded = 0._mc_prec

        do t1 = 1, size(coul%internal_excluded)
            !Corrections for internal molecular charges due to ewald summation
            do at1 = 1, prototype(t1)%nats
                if(.not.prototype(t1)%ischarged(at1))cycle 
                do at2 = at1 + 1, prototype(t1)%nats
                    if(.not.prototype(t1)%ischarged(at2))cycle
                    rij = box%distsq(prototype(t1)%X(at1), prototype(t1)%Y(at1), prototype(t1)%Z(at1), &
                                                prototype(t1)%X(at2), prototype(t1)%Y(at2), prototype(t1)%Z(at2))
                    rij = sqrt(rij)
                    coul%internal_excluded(t1) = coul%internal_excluded(t1) + &
                                                 coul%charge_charge(at2, at1, t1, t1)*(erf(coul%alpha*rij))/rij
                end do
            end do
        end do
    end subroutine

    pure function correction_terms(coul, type_index) result(correction_energy)
        class(ewald_type), intent(in) :: coul
        integer, intent(in) :: type_index
        real(kind=mc_prec) :: correction_energy

        correction_energy = - (coul%internal_excluded(type_index) + coul%ewald_self(type_index))
        return
    end function

    pure subroutine update_boundary(coul, box)
        class(ewald_type), intent(inout) :: coul
        class(box_type), intent(in) :: box
        integer :: nx, ny, nz
        real(kind=mc_prec), dimension(3, 3) :: iboundary
        real(kind=mc_prec) :: kx, ky, kz, ksq, ex, ey, ez, ex1, ey1, ez1

        iboundary = box%get_reciprocal()
        coul%prefac(1) = 2._mc_prec*pi*iboundary(1, 1)
        coul%prefac(2) = 2._mc_prec*pi*iboundary(2, 2)
        coul%prefac(3) = 2._mc_prec*pi*iboundary(3, 3)

        ex1 = exp( -(coul%prefac(1)/(2._mc_prec*coul%alpha))**2)
        ey1 = exp( -(coul%prefac(2)/(2._mc_prec*coul%alpha))**2)
        ez1 = exp( -(coul%prefac(3)/(2._mc_prec*coul%alpha))**2)
        do nx = 0, coul%ncut(1)
            kx = nx*coul%prefac(1)
            ex = ex1**int(nx*nx)
            do ny = - coul%ncut(2), coul%ncut(2)
                ky = ny*coul%prefac(2)
                ey = ey1**int(ny*ny)
                do nz = - coul%ncut(3), coul%ncut(3)
                    if(nz == 0 .and. ny == 0 .and. nx == 0) cycle
                    ez = ez1**int(nz*nz)*ey*ex
                    kz = nz*coul%prefac(3)
                    ksq = kx*kx + ky*ky + kz*kz
                    coul%expfac(nz, ny, nx) =ez/ksq*4._mc_prec*pi/box%get_volume()
                end do 
            end do 
        end do 

    end subroutine

    pure subroutine precompute_base_sines(ncharges, charge_index, coordinate, low, high, cosines, sines)
        integer, intent(in) :: ncharges, charge_index, low, high
        real(kind=mc_prec), intent(in) :: coordinate
        real(kind=mc_prec), dimension(ncharges, low:high), intent(inout) :: cosines, sines

        cosines(charge_index, 0) = 1._mc_prec
        sines(charge_index, 0) = 0._mc_prec
        cosines(charge_index, 1) = cos(coordinate)
        sines(charge_index, 1) = sin(coordinate)
        if(low == - high)then
            cosines(charge_index, -1) = cosines(charge_index, 1)
            sines(charge_index, -1) = - sines(charge_index, 1)
        end if

    end subroutine

    pure subroutine precompute_sines_from_base(ncharges, low, high, cosines, sines)
        integer, intent(in) :: ncharges, low, high
        real(kind=mc_prec), dimension(ncharges, low:high), intent(inout) :: cosines, sines
        integer :: i, j

        if(low == - high)then
            do j = 2, high
                do i = 1, ncharges
                    cosines(i, j) = cosines(i, j - 1)*cosines(i, 1) - sines(i, j - 1)*sines(i, 1)
                    sines(i, j) = sines(i, j - 1)*cosines(i, 1) + cosines(i, j - 1)*sines(i, 1)
                    cosines(i, -j) = cosines(i, j)
                    sines(i, -j) = - sines(i, j)
                end do
            end do
        else
            do j = 2, high
                do i = 1, ncharges
                    cosines(i, j) = cosines(i, j - 1)*cosines(i, 1) - sines(i, j - 1)*sines(i, 1)
                    sines(i, j) = sines(i, j - 1)*cosines(i, 1) + cosines(i, j - 1)*sines(i, 1)
                end do
            end do
        end if

    end subroutine

    pure subroutine fourier_ewald_single_molecule(coul, species_list, mol, molecule_type, fractional_old, fractional_new, &
                                                   delta_fourier_ewald_energy)
        !Fourier space contribution to ewald sum energy for 
        !changing the fractional parameter from fractional_old to fractional_new
        !Can also be used for inserting/removing a molecule by setting them to 0 and 1, respectively
        class(ewald_type), intent(inout) :: coul
        type(molecule_species), dimension(:), intent(in) :: species_list
        type(single_molecule), intent(in) :: mol
        integer, intent(in) :: molecule_type
        real(kind=mc_prec), intent(in) :: fractional_old, fractional_new
        real(kind=mc_prec), intent(out) :: delta_fourier_ewald_energy

        real(kind=mc_prec) :: chq
        integer :: at1, ncharges, charge_no

        real(kind=mc_prec), dimension(:, :), allocatable :: cosx, cosy, cosz, sinx, siny, sinz
        real(kind=mc_prec), dimension(:), allocatable :: local_q
        integer, dimension(3) :: ncut

        ncharges = species_list(molecule_type)%ncharged
        if(ncharges == 0)return

        associate(re => coul%rhore, im => coul%rhoim)
        ncut = coul%ncut
        chq = fractional_new**int(5) - fractional_old**int(5) !Gives the change in the chargevalue due to change in fraction

        allocate(local_q(ncharges) )
        allocate(cosx(ncharges, 0:ncut(1)), &
                         cosy(ncharges, -ncut(2):ncut(2)), &
                         cosz(ncharges, -ncut(3):ncut(3)), &
                         sinx(ncharges, 0:ncut(1)), & 
                         siny(ncharges, -ncut(2):ncut(2)), & 
                         sinz(ncharges, -ncut(3):ncut(3)) )

        charge_no = 0
        !precompute sines and cosines
        do at1 = 1, species_list(molecule_type)%nats
            if(.not.species_list(molecule_type)%atom(at1)%ischarged)cycle
            charge_no = charge_no + 1
            local_q(charge_no) = species_list(molecule_type)%atom(at1)%charge*chq
            call precompute_base_sines(ncharges, charge_no, coul%prefac(1)*mol%X(at1), 0, ncut(1), cosx, sinx)
            call precompute_base_sines(ncharges, charge_no, coul%prefac(2)*mol%Y(at1), -ncut(2), ncut(2), cosy, siny)
            call precompute_base_sines(ncharges, charge_no, coul%prefac(3)*mol%Z(at1), -ncut(3), ncut(3), cosz, sinz)
        end do
        call precompute_sines_from_base(ncharges, 0, ncut(1), cosx, sinx)
        call precompute_sines_from_base(ncharges, -ncut(2), ncut(2), cosy, siny)
        call precompute_sines_from_base(ncharges, -ncut(3), ncut(3), cosz, sinz)

        call compute_fourier_energy_change(ncharges, ncut(1), ncut(2), ncut(3), cosx, sinx, cosy, siny, cosz, sinz, &
                                           local_q, coul%expfac, re, im, delta_fourier_ewald_energy)
        end associate
    end subroutine

    pure subroutine fourier_ewald_diff(coul, species_list, newpos, oldpos, molecule_type, isfrac, delta_fourier_ewald_energy)
        !Calculate change in fourier part of ewald sum, when moving m1 to m2 (e.g. by rotation, translation)

        class(ewald_type), intent(inout) :: coul
        type(molecule_species), dimension(:), intent(in) :: species_list
        integer, intent(in) :: molecule_type
        type(single_molecule), intent(in) :: newpos, oldpos
        logical, intent(in) :: isfrac
        real(kind=mc_prec), intent(inout) :: delta_fourier_ewald_energy
        integer :: at1, index, ncharges, charge_no
        real(kind=mc_prec) :: dx, dy, dz
        real(kind=mc_prec), dimension(:, :), allocatable :: cosx, cosy, cosz, sinx, siny, sinz
        real(kind=mc_prec), dimension(:), allocatable :: local_q
        integer, dimension(3) :: ncut

        !NB factor 2 - once for old position and once for new position
        ncharges = 2*species_list(molecule_type)%ncharged 
        if(ncharges == 0)return
        
        associate(re => coul%rhore, im => coul%rhoim)

        ncut = coul%ncut
        !Count the number of charged atoms
        allocate(local_q(ncharges) )
        allocate(cosx(ncharges, 0:ncut(1)), &
                         cosy(ncharges, -ncut(2):ncut(2)), &
                         cosz(ncharges, -ncut(3):ncut(3)), &
                         sinx(ncharges, 0:ncut(1)), & 
                         siny(ncharges, -ncut(2):ncut(2)), & 
                         sinz(ncharges, -ncut(3):ncut(3)) )

        !precompute sines and cosines
        charge_no = 0
        do index = 1, 2
             do at1 = 1, newpos%nats
                    if(.not.species_list(molecule_type)%atom(at1)%ischarged)cycle
                    charge_no = charge_no + 1
                    if(index == 1) then
                        local_q(charge_no) = species_list(molecule_type)%atom(at1)%charge
                        dx = coul%prefac(1)*newpos%X(at1)
                        dy = coul%prefac(2)*newpos%Y(at1)
                        dz = coul%prefac(3)*newpos%Z(at1)
                    else
                        !Negative charge in order to subtract the contribution from the old position
                        local_q(charge_no) = - species_list(molecule_type)%atom(at1)%charge
                        dx = coul%prefac(1)*oldpos%X(at1)
                        dy = coul%prefac(2)*oldpos%Y(at1)
                        dz = coul%prefac(3)*oldpos%Z(at1)
                    end if            
                    call precompute_base_sines(ncharges, charge_no, dx, 0, ncut(1), cosx, sinx)
                    call precompute_base_sines(ncharges, charge_no, dy, -ncut(2), ncut(2), cosy, siny)
                    call precompute_base_sines(ncharges, charge_no, dz, -ncut(3), ncut(3), cosz, sinz)
             end do
        end do
        call precompute_sines_from_base(ncharges, 0, ncut(1), cosx, sinx)
        call precompute_sines_from_base(ncharges, -ncut(2), ncut(2), cosy, siny)
        call precompute_sines_from_base(ncharges, -ncut(3), ncut(3), cosz, sinz)

        if(isfrac)local_q = local_q*species_list(molecule_type)%frac5

        call compute_fourier_energy_change(ncharges, ncut(1), ncut(2), ncut(3), cosx, sinx, cosy, siny, cosz, sinz, &
                                           local_q, coul%expfac, re, im, delta_fourier_ewald_energy)
        end associate
    end subroutine

    pure subroutine compute_fourier_energy_change(ncharges, ncx, ncy, ncz, cosx, sinx, cosy, siny, cosz, sinz, &
                                             local_q, expfac, re, im, energy)
        integer, intent(in) :: ncharges, ncx, ncy, ncz
        real(kind=mc_prec), dimension(ncharges, 0:ncx), intent(in) :: cosx, sinx
        real(kind=mc_prec), dimension(ncharges, -ncy:ncy), intent(in) :: cosy, siny
        real(kind=mc_prec), dimension(ncharges, -ncz:ncz), intent(in) :: cosz, sinz
        real(kind=mc_prec), dimension( -ncz:ncz, -ncy:ncy, 0:ncx), intent(inout) :: re, im, expfac
        real(kind=mc_prec), intent(inout) :: energy
        real(kind=mc_prec), dimension(ncharges), intent(in) :: local_q

        integer :: symy, at1, maxnz, minz, ix, iy, iz, k_cutsq
        
        real(kind=mc_prec), dimension(ncharges) :: co, si

        k_cutsq = maxval([ncx*ncx, ncy*ncy, ncz*ncz])
        !Account for symmetry of sines/cosines to skip half the loops
        symy = 0
        do ix = 0, ncx
             do iy = symy, ncy
                    if(k_cutsq < iy*iy + ix*ix)cycle
                    do at1 = 1, ncharges
                        co(at1) = cosx(at1, ix)*cosy(at1, iy) - sinx(at1, ix)*siny(at1, iy)
                        si(at1) = sinx(at1, ix)*cosy(at1, iy) + cosx(at1, ix)*siny(at1, iy)
                    end do
                    maxnz = min(int(sqrt(dble(k_cutsq - iy*iy - ix*ix))), ncz)
                    minz = - maxnz
                    if(ix == 0.and.iy == 0) minz = 1
                    do iz = minz, maxnz
                         !Subtract energy from old fractional value
                         energy = energy - (re(iz, iy, ix)**2 + im(iz, iy, ix)**2)*expfac(iz, iy, ix)
                         !Determine new
                         do at1 = 1, ncharges
                                re(iz, iy, ix) = re(iz, iy, ix) + local_q(at1)*(co(at1)*cosz(at1, iz) - si(at1)*sinz(at1, iz) )
                                im(iz, iy, ix) = im(iz, iy, ix) + local_q(at1)*(si(at1)*cosz(at1, iz) + co(at1)*sinz(at1, iz) )
                         end do             
                         !Add energy from new fractional value
                         energy = energy + (re(iz, iy, ix)**2 + im(iz, iy, ix)**2)*expfac(iz, iy, ix)
                    end do
             end do
             symy = - ncy
        end do

    end subroutine

    pure subroutine fourier_ewald_total(coul, species_list, ewf)
        !Calculates the fourier component of the ewald summation for an entire system
        class(ewald_type), intent(inout) :: coul
        type(molecule_species), dimension(:), intent(in) :: species_list
        real(kind=mc_prec), intent(inout) :: ewf
        real(kind=mc_prec) :: dx, dy, dz
        integer :: nx, ny, nz
        integer :: index, symy, maxnz, minz, at1, t1

        real(kind=mc_prec), dimension(:, :), allocatable :: cosx, cosy, cosz, sinx, siny, sinz
        real(kind=mc_prec), dimension(:), allocatable :: co, si, local_q
        integer :: total_charges, charge_no
        integer :: k_cutsq
        integer, dimension(3) :: ncut

        ewf = 0._mc_prec
        ncut = coul%ncut
        k_cutsq = maxval(ncut*ncut)
        total_charges = 0
        do t1 = 1, size(species_list)
            total_charges = total_charges + species_list(t1)%ncharged*species_list(t1)%nmol
            if(species_list(t1)%fluctuating) total_charges = total_charges + species_list(t1)%ncharged
        end do
        if(total_charges == 0)return
        associate(re => coul%rhore, im => coul%rhoim)
        allocate(co(total_charges), si(total_charges), local_q(total_charges))
        allocate(cosx(total_charges, 0:ncut(1)), &
                         cosy(total_charges, -ncut(2):ncut(2)), &
                         cosz(total_charges, -ncut(3):ncut(3)), &
                         sinx(total_charges, 0:ncut(1)), &
                         siny(total_charges, -ncut(2):ncut(2)), &
                         sinz(total_charges, -ncut(3):ncut(3)))
        !precompute sines and cosines
        charge_no = 0
        do t1 = 1, size(species_list)
            do at1 = 1, species_list(t1)%nats
                if(.not.species_list(t1)%atom(at1)%ischarged)cycle
                if(species_list(t1)%fluctuating)then
                    charge_no = charge_no + 1
                    local_q(charge_no) = species_list(t1)%atom(at1)%charge*species_list(t1)%frac5

                    dx = coul%prefac(1)*species_list(t1)%fractional_molecule%X(at1)
                    dy = coul%prefac(2)*species_list(t1)%fractional_molecule%Y(at1)
                    dz = coul%prefac(3)*species_list(t1)%fractional_molecule%Z(at1)

                    call precompute_base_sines(total_charges, charge_no, dx, 0, ncut(1), cosx, sinx)
                    call precompute_base_sines(total_charges, charge_no, dy, -ncut(2), ncut(2), cosy, siny)
                    call precompute_base_sines(total_charges, charge_no, dz, -ncut(3), ncut(3), cosz, sinz)
                end if
                do index = 1, species_list(t1)%nmol
                    charge_no = charge_no + 1
                    local_q(charge_no) = species_list(t1)%atom(at1)%charge

                    dx = coul%prefac(1)*species_list(t1)%atom(at1)%pos%X(index)
                    dy = coul%prefac(2)*species_list(t1)%atom(at1)%pos%Y(index)
                    dz = coul%prefac(3)*species_list(t1)%atom(at1)%pos%Z(index)

                    call precompute_base_sines(total_charges, charge_no, dx, 0, ncut(1), cosx, sinx)
                    call precompute_base_sines(total_charges, charge_no, dy, -ncut(2), ncut(2), cosy, siny)
                    call precompute_base_sines(total_charges, charge_no, dz, -ncut(3), ncut(3), cosz, sinz)

                end do
            end do
        end do
        call precompute_sines_from_base(total_charges, 0, ncut(1), cosx, sinx)
        call precompute_sines_from_base(total_charges, -ncut(2), ncut(2), cosy, siny)
        call precompute_sines_from_base(total_charges, -ncut(3), ncut(3), cosz, sinz)

        symy = 0
        re = 0._mc_prec
        im = 0._mc_prec
        do nx = 0, ncut(1)
             do ny = symy, ncut(2)
                    if(k_cutsq < ny*ny + nx*nx)cycle
                    do index = 1, total_charges
                        co(index) = local_q(index)*(cosx(index, nx)*cosy(index, ny) - sinx(index, nx)*siny(index, ny))
                        si(index) = local_q(index)*(sinx(index, nx)*cosy(index, ny) + cosx(index, nx)*siny(index, ny))
                    end do
                    maxnz = min(int(sqrt(dble(k_cutsq - ny*ny - nx*nx))), ncut(3))
                    minz = - maxnz
                    if(ny == 0.and.nx == 0)minz = 1
                    do nz = minz, maxnz
                        re(nz, ny, nx) = re(nz, ny, nx) + sum(co(1:total_charges)*cosz(1:total_charges, nz)&
                                                        - si(1:total_charges)*sinz(1:total_charges, nz))
                        im(nz, ny, nx) = im(nz, ny, nx) + sum(si(1:total_charges)*cosz(1:total_charges, nz)&
                                                        + co(1:total_charges)*sinz(1:total_charges, nz))
                        !Energy due to change in fourier component (nz, ny, nx)
                        ewf = ewf + (re(nz, ny, nx)**2 + im(nz, ny, nx)**2)*coul%expfac(nz, ny, nx)
                    end do
             end do
             symy = - ncut(2)
        end do
        end associate
    end subroutine

    pure function ewald_convergence_parameter(epsilon, cutoff_radius) result(alpha)
        
        real(kind=mc_prec), intent(in) :: epsilon, cutoff_radius
        real(kind=mc_prec) :: alpha

        alpha = 2.0/cutoff_radius
        do while(erfc(alpha*cutoff_radius)/cutoff_radius > epsilon)
            alpha = alpha + 0.01/cutoff_radius
        end do
    end function

    pure function ewald_reciprocal_cutoff(epsilon, box, alpha) result(ncut)
    
        real(kind=mc_prec), intent(in) :: alpha, epsilon
        class(box_type), intent(in) :: box
        integer, dimension(3) :: ncut
    
        real(kind=mc_prec) :: kmax
        integer :: i
        real(kind=mc_prec), dimension(3, 3) :: invboundary

        invboundary = box%get_reciprocal()
        ncut = 1
        do i = 1, 3
            kmax = 2*pi*invboundary(i, i)*ncut(i)
            do while(exp( -kmax**2/(4*alpha**2))/(kmax**2) > epsilon)
                ncut(i) = ncut(i) + 1
                kmax = 2*pi*invboundary(i, i)*ncut(i)
            end do
        end do
    end function

end module

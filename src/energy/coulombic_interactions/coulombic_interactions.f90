module coulombic_interactions_module
    use precision_module, only: mc_prec
    implicit none
    private

    public :: coulombic_interaction

    type, abstract :: coulombic_interaction
        
        real(kind=mc_prec), dimension(:, :, :, :), allocatable :: charge_charge

    contains

        procedure(update_boundary), deferred :: update_boundary
        procedure(correction_terms), deferred :: correction_terms
    end type

    abstract interface  
        pure subroutine update_boundary(coul, box)
            use precision_module
            use geometry_module
            import coulombic_interaction
            class(coulombic_interaction), intent(inout) :: coul
            class(box_type), intent(in) :: box
        end subroutine
    end interface

    abstract interface  
        pure function correction_terms(coul, type_index) result(correction_energy)
            use precision_module
            import coulombic_interaction
            class(coulombic_interaction), intent(in) :: coul
            integer, intent(in) :: type_index
            real(kind=mc_prec) :: correction_energy
        end function
    end interface

end module

module non_bonded_c_interface

    !Interface to explicitly vectorized routines for electrostatic interactions.
    !Unfortunately, these must be written in c, the worst programming language
    interface  
        pure subroutine elstat_and_lj_normal(table, position, n_steps, table_scale, result, &
                                        charge, sigmasq, epsilon4, shift, rc2)&
                                        bind(C, name = "lookup_erfc_ewald_and_lennard_jones")
            use precision_module, only: c_mc_prec
            use iso_C_binding, only: c_int
            implicit none
            real(kind=c_mc_prec), dimension(*), intent(in) :: table, position
            real(kind=c_mc_prec), dimension(*), intent(inout) :: result
            integer(kind=c_int), value :: n_steps
            real(kind=c_mc_prec), value :: table_scale, charge,  sigmasq, epsilon4, shift, rc2
        end subroutine

        pure subroutine elstat_and_lj_fractional(table, position, n_steps, table_scale, result, &
                                            charge, sigmasq, epsilon4, ft1, shift, rc2)&
                                            bind(C, name = "lookup_erfc_ewald_and_lennard_jones_fractional")
            use precision_module, only: c_mc_prec
            use iso_C_binding, only: c_int
            implicit none
            real(kind=c_mc_prec), dimension(*), intent(in) :: table, position
            real(kind=c_mc_prec), dimension(*), intent(inout) :: result
            integer(kind=c_int), value :: n_steps
            real(kind=c_mc_prec), value :: table_scale, charge,  sigmasq, epsilon4, ft1, shift, rc2
        end subroutine

        pure subroutine elstat_only(table, position, n_steps, table_scale, result, charge, rc2)&
                                bind(C, name = "lookup_erfc_ewald")
            use precision_module, only: c_mc_prec
            use iso_C_binding, only: c_int
            implicit none
            real(kind=c_mc_prec), dimension(*), intent(in) :: table, position
            real(kind=c_mc_prec), dimension(*), intent(inout) :: result
            integer(kind=c_int), value :: n_steps
            real(kind=c_mc_prec), value :: table_scale, charge, rc2
        end subroutine
    end interface

end module

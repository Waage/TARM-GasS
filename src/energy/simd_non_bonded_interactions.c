#include "simd_defines.h"
#include "coulombic_interactions/simd_ewald_erfc.h"
#include "lennard_jones/simd_lennard_jones.h"

void lookup_erfc_ewald_and_lennard_jones(c_real *ewald_erfc_table, c_real *distance_squared, int nmol, c_real table_scale, c_real *result,
                                         c_real q1q2, c_real lj_sigma_squared, c_real lj_4_epsilon, c_real lj_shift_energy, c_real cutoff_squared)
{
    const c_simd_real v_inverse_erfc_table_scale = simd_set_value(table_scale); //Used to find the index in a lookup ewald_erfc_table
    const c_simd_real v_lj_shift_energy = simd_set_value(lj_shift_energy); //Lennard jones shift energy
    const c_simd_real v_lj_4_epsilon = simd_set_value(lj_4_epsilon);   //Lennard jones epsilon * 4
    const c_simd_real v_lj_sigma_squared = simd_set_value(lj_sigma_squared);   //Lennard jones sigma**2
    const c_simd_real v_q1q2 = simd_set_value(q1q2);     //q_1*q_2
    const c_simd_real v_cutoff_squared = simd_set_value(cutoff_squared);   //rc**2

    for (int i = 0; i < nmol; i += N_PER_VEC)
    {
        c_simd_real v_distance_squared = simd_load_unaligned(&distance_squared[i]);

        if( simd_any(simd_compare_less_than(v_distance_squared, v_cutoff_squared)))
        {
            c_simd_real v_elstat = perform_ewald_lookup(v_q1q2, v_distance_squared, v_inverse_erfc_table_scale, ewald_erfc_table);
            c_simd_real v_lj_ener = calculate_lennard_jones_interactions(v_distance_squared, v_lj_sigma_squared, v_lj_4_epsilon);

            c_simd_real v_energy_change = simd_add_real(v_elstat,simd_add_real(v_lj_ener,v_lj_shift_energy));

            c_simd_real v_rc2_mask = simd_compare_less_than(v_distance_squared,v_cutoff_squared);
            v_energy_change = simd_bitwise_and(v_energy_change,v_rc2_mask);

            v_energy_change = simd_add_real(simd_load_unaligned(&result[i]),v_energy_change);
            simd_store_unaligned(&result[i], v_energy_change);
        }
    }
}


void lookup_erfc_ewald_and_lennard_jones_fractional(c_real *ewald_erfc_table, c_real *distance_squared, int nmol, c_real table_scale, c_real *result,
                                              c_real q1q2, c_real lj_sigma_squared, c_real lj_4_epsilon, c_real fractional_distance_displacement, c_real lj_shift_energy, c_real cutoff_squared)
{
    /* Explicit simd routine for ewald real-space lookup + lj (fractional particle) */

    const c_simd_real v_inverse_erfc_table_scale = simd_set_value(table_scale); //Used to find the index in a lookup ewald_erfc_table
    const c_simd_real v_lj_shift_energy = simd_set_value(lj_shift_energy); //Lennard jones shift energy
    const c_simd_real v_lj_4_epsilon = simd_set_value(lj_4_epsilon);   //Lennard jones epsilon * 4
    const c_simd_real v_inverse_lj_sigma_squared = simd_set_value(1./lj_sigma_squared);   //Lennard jones inverse sigma**2
    const c_simd_real v_lj_fractional_distance_displacement =  simd_set_value(fractional_distance_displacement);   //Lennard jones fractional parameter
    const c_simd_real v_q1q2 = simd_set_value(q1q2);     //q_1*q_2
    const c_simd_real v_cutoff_squared = simd_set_value(cutoff_squared);   //rc**2

    for (int i = 0; i < nmol; i += N_PER_VEC)
    {
        c_simd_real v_distance_squared = simd_load_unaligned(&distance_squared[i]);

        if( simd_any(simd_compare_less_than(v_distance_squared, v_cutoff_squared)))
        {
            c_simd_real v_elstat = perform_ewald_lookup(v_q1q2, v_distance_squared, v_inverse_erfc_table_scale, ewald_erfc_table);
            c_simd_real v_lj_ener = calculate_fractional_lennard_jones_interactions(v_distance_squared, v_lj_fractional_distance_displacement, v_inverse_lj_sigma_squared, v_lj_4_epsilon);

            c_simd_real v_energy_change = simd_add_real(v_elstat,simd_add_real(v_lj_ener,v_lj_shift_energy));

            c_simd_real v_rc2_mask = simd_compare_less_than(v_distance_squared,v_cutoff_squared);
            v_energy_change = simd_bitwise_and(v_energy_change,v_rc2_mask);

            v_energy_change = simd_add_real(simd_load_unaligned(&result[i]),v_energy_change);
            simd_store_unaligned(&result[i], v_energy_change);
        }
    }
}

void lookup_erfc_ewald(c_real *ewald_erfc_table, c_real *distance_squared, int nmol, c_real table_scale, c_real *result, c_real q1q2, c_real cutoff_squared)
{
    /* Explicit simd routine for ewald real-space lookup  */ 
    const c_simd_real v_inverse_erfc_table_scale = simd_set_value(table_scale); //Used to find the index in a lookup ewald_erfc_table
    const c_simd_real v_q1q2 = simd_set_value(q1q2);     //q_1*q_2
    const c_simd_real v_cutoff_squared = simd_set_value(cutoff_squared);   //rc**2

    for (int i = 0; i < nmol; i += N_PER_VEC)
    {
        c_simd_real v_distance_squared = simd_load_unaligned(&distance_squared[i]);

        if (simd_any(simd_compare_less_than(v_distance_squared, v_cutoff_squared)))
        {
            c_simd_real v_energy_change = perform_ewald_lookup(v_q1q2, v_distance_squared, v_inverse_erfc_table_scale, ewald_erfc_table);

            c_simd_real v_rc2_mask = simd_compare_less_than(v_distance_squared,v_cutoff_squared);
            v_energy_change = simd_bitwise_and(v_energy_change,v_rc2_mask);

            v_energy_change = simd_add_real(simd_load_unaligned(&result[i]),v_energy_change);
            simd_store_unaligned(&result[i], v_energy_change);
        }
    }
}

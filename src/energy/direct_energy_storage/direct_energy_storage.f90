module direct_energy_storage_module
    use general_utilities_module
    use parameters_module, only: storage_increment, safety_pad
    use precision_module, only: mc_prec
    use reduced_dimensionality_energy_storage_module, only: reduced_dimensionality_energy_storage
    implicit none
    private

    public :: direct_energy_storage

    !Should stay hidden
    type dir_int_p
    
        real(kind=mc_prec), dimension(:, :), allocatable :: array
    
    end type

    type direct_energy_storage

        !Would preferably be private but eh
        type(dir_int_p), dimension(:, :), allocatable :: inner

    contains

        procedure :: update
        procedure :: remove_molecule
        procedure :: increase_size
        procedure :: array_sum
        procedure :: get_value

    end type

    interface direct_energy_storage
        module procedure new_storage
        module procedure new_storage_specific_species_and_range
    end interface

contains

    pure function new_storage(storage_space) result(storage)
        integer, dimension(:), intent(in) :: storage_space
        type(direct_energy_storage) :: storage
        integer :: i, j

        allocate(storage%inner(size(storage_space), size(storage_space)))
        do i = 1, size(storage_space)
            do j = i, size(storage_space)
                allocate( storage%inner(i, j)%array(0:storage_space(i), 0:storage_space(j)) )
                storage%inner(i, j)%array = 0._mc_prec
                if(.not.(i == j))then
                    allocate( storage%inner(j, i)%array(0:storage_space(j), 0:storage_space(i)) )
                    storage%inner(j, i)%array = 0._mc_prec
                end if
            end do
        end do
    end function

    pure function new_storage_specific_species_and_range(storage_space, type_index, molecule_index) result(storage)
        integer, dimension(:), intent(in) :: storage_space
        integer, intent(in) :: type_index, molecule_index
        type(direct_energy_storage) :: storage
        integer :: i

        allocate(storage%inner(size(storage_space), size(storage_space)))
        do i = 1, size(storage_space)
            allocate(storage%inner(i, type_index)%array(0:storage_space(i), molecule_index:molecule_index))
            storage%inner(i, type_index)%array(0:storage_space(i), molecule_index:molecule_index) = 0._mc_prec
        end do
    end function

    pure subroutine update(this, species_index, molecule_index, new_storage)
        class(direct_energy_storage), intent(inout) :: this
        type(reduced_dimensionality_energy_storage), intent(in) :: new_storage
        integer, intent(in) :: species_index, molecule_index
        integer :: i

        do i = 1, size(this%inner, 1)

            this%inner(i, species_index)%array(:, molecule_index) &
                    = new_storage%inner(i)%array(:)

            this%inner(species_index, i)%array(molecule_index, :) &
                    = new_storage%inner(i)%array(:)

        end do

    end subroutine

    pure subroutine increase_size(this, index)
        class(direct_energy_storage), intent(inout) :: this
        integer, intent(in) :: index
        integer :: i, old_space, new_space, conserved_space

        old_space = size(this%inner(index, index)%array, 1)
        new_space = old_space + storage_increment

        do i = 1, size(this%inner, 1)
            if(i == index)then
                call resize_0_indexed_2D(this%inner(i, index)%array, (/old_space, old_space/), (/new_space, new_space/))
            else
                conserved_space = size(this%inner(i, index)%array, 1)
                call resize_0_indexed_2D(this%inner(i, index)%array, (/conserved_space, old_space/), (/conserved_space, new_space/))
                call resize_0_indexed_2D(this%inner(index, i)%array, (/old_space, conserved_space/), (/new_space, conserved_space/))
            end if
        end do

    end subroutine

    pure subroutine remove_molecule(this, remove_type, remove_index, nmol)
        class(direct_energy_storage), intent(inout) :: this
        integer, intent(in) :: remove_type, remove_index, nmol
        integer :: i
        !Overwrite energy storage
        do i = 1, size(this%inner, 1)
            call overwrite_array_row(this%inner(i, remove_type)%array, remove_index, nmol, 1)
            call overwrite_array_col(this%inner(remove_type, i)%array, remove_index, nmol, 1)
        end do
    end subroutine

    pure real(kind=mc_prec) function array_sum(this, molecule_type_index, molecule_index, limits)
        class(direct_energy_storage), intent(in) :: this
        integer, intent(in) :: molecule_type_index, molecule_index
        integer, dimension(:), intent(in) ::limits
        integer :: i
        array_sum = 0._mc_prec
        do i = 1, size(this%inner, 1)
            array_sum = array_sum + sum( this%inner(i, molecule_type_index)%array(0:limits(i), molecule_index) )
        end do
    end function

    pure real(kind=mc_prec) function get_value(this, type_index_1, type_index_2, molecule_index_1, molecule_index_2)
        class(direct_energy_storage), intent(in) :: this
        integer, intent(in) :: type_index_1, type_index_2, molecule_index_1, molecule_index_2
        get_value = this%inner(type_index_1, type_index_2)%array(molecule_index_1, molecule_index_2)
    end function

end module

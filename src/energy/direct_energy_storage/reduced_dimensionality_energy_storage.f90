module reduced_dimensionality_energy_storage_module
    use general_utilities_module
    use parameters_module, only: storage_increment, safety_pad
    use precision_module, only: mc_prec
    implicit none
    private

    public :: reduced_dimensionality_energy_storage

    !Should stay hidden
    type red_dim_dir_int_p
    
        real(kind=mc_prec), dimension(:), allocatable :: array
    
    end type

    type reduced_dimensionality_energy_storage

        !Would preferably be private but eh
        type(red_dim_dir_int_p), dimension(:), allocatable :: inner

    contains

        procedure :: array_sum
        procedure :: get_value

    end type

    interface reduced_dimensionality_energy_storage
        module procedure new_storage
    end interface

contains

    pure function new_storage(storage_space) result(storage)
        integer, dimension(:), intent(in) :: storage_space
        type(reduced_dimensionality_energy_storage) :: storage
        integer :: i

        allocate(storage%inner(size(storage_space)))
        do i = 1, size(storage_space)
            allocate( storage%inner(i)%array(0:storage_space(i)) )
            storage%inner(i)%array = 0._mc_prec
        end do
    end function

    pure real(kind=mc_prec) function array_sum(this, limits)
        class(reduced_dimensionality_energy_storage), intent(in) :: this
        integer, dimension(:), intent(in) ::limits
        integer :: i
        array_sum = 0._mc_prec
        do i = 1, size(this%inner, 1)
            array_sum = array_sum + sum( this%inner(i)%array(0:limits(i)) )
        end do
    end function

    pure real(kind=mc_prec) function get_value(this, type_index, molecule_index)
        class(reduced_dimensionality_energy_storage), intent(in) :: this
        integer, intent(in) :: type_index, molecule_index
        get_value = this%inner(type_index)%array(molecule_index)
    end function

end module

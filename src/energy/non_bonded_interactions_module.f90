module non_bonded_interactions_module
    !Contains routines for direct interactions
    use atom_type_module, only: atom_type, single_atom
    use direct_energy_storage_module, only: direct_energy_storage
    use molecule_module, only: single_molecule
    use molecule_species_module, only: molecule_species
    use non_bonded_c_interface, only: elstat_and_lj_normal, elstat_and_lj_fractional, elstat_only
    use parameters_module, only: BIG_ENERGY, HC_OVERLAP_SQ, FRACTIONAL_INDEX, safety_pad, zeta
    use precision_module, only: mc_prec
    use reduced_dimensionality_energy_storage_module, only: reduced_dimensionality_energy_storage
    use system_module, only: system
    implicit none
    private

    public :: non_bonded_interactions, no_interaction, only_lj, only_elstat, lj_and_elstat, ljener

    interface direct_ewald 
        module procedure direct_ewald_normal_normal
    end interface

    interface non_bonded_interactions 
        module procedure non_bonded_interactions_normal
        module procedure non_bonded_interactions_fractional
        module procedure non_bonded_interactions_single_single
        module procedure non_bonded_interactions_all
    end interface

    !An enumerator for defining interaction types
    enum, bind(C)
        enumerator :: no_interaction,  only_lj, only_elstat, lj_and_elstat
    endenum

contains

    pure subroutine non_bonded_interactions_normal(sys, single_mol, t1, skip_index, energy_storage)

        class(system), intent(in) :: sys
        type(single_molecule), intent(in) :: single_mol
        integer, intent(in) :: t1, skip_index
        type(reduced_dimensionality_energy_storage), intent(inout) :: energy_storage
        real(kind=mc_prec), dimension(:), allocatable :: rsq
        integer :: at1, at2, t2, nmol

        do at1 = 1, sys%species(t1)%nats
            do t2 = 1, size(sys%species)
                nmol = sys%species(t2)%nmol
                allocate(rsq(nmol + safety_pad))
                rsq(nmol + 1:nmol + safety_pad) = sys%rc2 + 5._mc_prec !Padding to allow for vectorization later
                do at2 = 1, sys%species(t2)%nats
                    call non_bonded_inner_loop(sys, t1, at1, t2, at2, 1._mc_prec, 1._mc_prec, 0._mc_prec, single_mol, rsq, &
                                energy_storage%inner(t2)%array(:), skip_index, 1)
                end do
                deallocate(rsq)
            end do
        end do
        energy_storage%inner(t1)%array(skip_index) = 0._mc_prec

    end subroutine

    pure subroutine non_bonded_interactions_fractional(sys, single_mol, t1, frac1, skip_index, energy_storage)

        class(system), intent(in) :: sys
        type(single_molecule), intent(in) :: single_mol
        integer, intent(in) :: t1, skip_index
        real(kind=mc_prec), intent(in) :: frac1
        type(reduced_dimensionality_energy_storage), intent(inout) :: energy_storage
        real(kind=mc_prec) :: ft1, frac5
        real(kind=mc_prec), dimension(:), allocatable :: rsq
        integer :: at1, at2, t2, nmol

        frac5 = frac1**5
        ft1 = zeta*(1._mc_prec - frac1)**2
        do at1 = 1, sys%species(t1)%nats
            do t2 = 1, size(sys%species)
                nmol = sys%species(t2)%nmol
                allocate(rsq(nmol + safety_pad))
                rsq(nmol + 1:nmol + safety_pad) = sys%rc2 + 5._mc_prec !Padding to allow for vectorization later
                do at2 = 1, sys%species(t2)%nats
                    call non_bonded_inner_loop(sys, t1, at1, t2, at2, frac1, frac5, ft1, single_mol, rsq, &
                                energy_storage%inner(t2)%array(:), skip_index, 1)
                end do
            deallocate(rsq)
            end do
        end do
        energy_storage%inner(t1)%array(skip_index) = 0._mc_prec

    end subroutine

    pure subroutine non_bonded_interactions_all(sys, species, energy_storage)

        class(system), intent(in) :: sys
        type(molecule_species), dimension(:), intent(in) :: species
        type(direct_energy_storage), intent(inout) :: energy_storage

        integer :: t1, m1, at1, at2, t2, startat1, startat2
        real(kind=mc_prec) :: ft1, frac, frac5
        real(kind=mc_prec), dimension(:), allocatable :: rsq
        type(single_molecule) :: molecule_iterator

        do t1 = 1, size(sys%species)
            if(sys%species(t1)%fluctuating) then 
                startat1 = 0
            else
                startat1 = 1
            end if
            do at1 = 1, species(t1)%nats
                do t2 = t1, size(sys%species)
                    allocate(rsq(species(t2)%nmol + safety_pad))
                    rsq(species(t2)%nmol + 1:species(t2)%nmol + safety_pad) = sys%rc2 + 1._mc_prec
                    do at2 = 1, species(t2)%nats
                        
                        if(no_interaction == atoms_interact(species(t1)%atom(at1), species(t2)%atom(at2))) cycle

                        do m1 = startat1, species(t1)%nmol

                            molecule_iterator = species(t1)%get_molecule(m1)

                            if(t2 == t1)then
                                energy_storage%inner(t2, t1)%array(m1, m1) = 0._mc_prec
                                startat2 = m1 + 1
                            else
                                startat2 = 1
                            end if

                            if(m1 == FRACTIONAL_INDEX)then
                                frac5 = species(t1)%frac5
                                frac = species(t1)%frac
                                ft1 = zeta*(1._mc_prec - species(t1)%frac)**2
                            else
                                frac5 = 1._mc_prec
                                frac = 1._mc_prec
                                ft1 = 0._mc_prec
                            end if

                            call non_bonded_inner_loop(sys, t1, at1, t2, at2, frac, frac5, ft1, molecule_iterator, rsq, &
                                energy_storage%inner(t2, t1)%array(:, m1), m1, startat2)

                        end do
                    end do
                    deallocate(rsq)
                end do
            end do
        end do

    end subroutine

    pure subroutine non_bonded_interactions_single_single(sys, mol1, t1, i1, energy_storage_1, mol2, t2, i2, energy_storage_2)
        !Somewhat slow routine, not very general either. Consider changing?
        !Useful for simple corrections that anyway will not take long
        !Avoid using this routine for anything else
        class(system), intent(in) :: sys
        type(single_molecule), intent(in) :: mol1, mol2
        type(reduced_dimensionality_energy_storage), intent(inout) :: energy_storage_1, energy_storage_2
        integer, intent(in) :: i1, i2, t1, t2
        real(kind=mc_prec) :: ft1, ft2
        real(kind=mc_prec) :: mixsigsq, mixeps4, shiftener, mixcharge
        real(kind=mc_prec) :: rsq
        integer :: at1, at2, interaction

        associate(lj => sys%lennard_jones, coul => sys%coulombic)
        
        if(i1 == FRACTIONAL_INDEX)then
            ft1 = zeta*(1._mc_prec - sys%species(t1)%frac)**2
        else
            ft1 = 0._mc_prec
        end if
        if (i2 == FRACTIONAL_INDEX) then
            ft2 = zeta*(1._mc_prec - sys%species(t2)%frac)**2
        else
            ft2 = 0._mc_prec    
        end if
        do at1 = 1, mol1%nats
            do at2 = 1, mol2%nats
                interaction = atoms_interact(sys%species(t1)%atom(at1), sys%species(t2)%atom(at2))
                
                if(interaction == no_interaction) cycle

                mixsigsq = ( lj%sigma(at2, at1, t2, t1) )**2
                mixeps4 = 4._mc_prec*lj%epsilon(at2, at1, t2, t1)
                mixcharge = coul%charge_charge(at2, at1, t2, t1)
                shiftener = lj%shift_correction(at2, at1, t2, t1)
                if(i1 == FRACTIONAL_INDEX)then
                    mixcharge = mixcharge*sys%species(t1)%frac5
                    mixeps4 = mixeps4*sys%species(t1)%frac
                    shiftener = shiftener*sys%species(t1)%frac
                end if
                if(i2 == FRACTIONAL_INDEX)then
                    mixcharge = mixcharge*sys%species(t2)%frac5
                    mixeps4 = mixeps4*sys%species(t2)%frac
                    shiftener = shiftener*sys%species(t2)%frac
                end if
                
                rsq = sys%box%distsq(mol1%X(at1), mol1%Y(at1), mol1%Z(at1), &
                                     mol2%X(at2), mol2%Y(at2), mol2%Z(at2))
                if(rsq < sys%rc2)then
                    if(rsq < HC_OVERLAP_SQ)then
                        energy_storage_1%inner(t2)%array(i2) = energy_storage_1%inner(t2)%array(i2) + BIG_ENERGY
                    end if
                    energy_storage_1%inner(t2)%array(i2) = energy_storage_1%inner(t2)%array(i2)&
                                                       + ljener(mixsigsq, mixeps4, ft1 + ft2, rsq)&
                                                       + shiftener&
                                                       + direct_ewald(sys, mixcharge, rsq)
                    energy_storage_2%inner(t1)%array(i1) = energy_storage_1%inner(t2)%array(i2)
                end if
            end do
        end do

        end associate
    end subroutine

    pure subroutine compute_fractional_interactions(sys, t1, at1, t2, at2, frac1, frac5, ft1, single_mol, &
                                             energy_storage, skip_index)

        class(system), intent(in) :: sys
        integer, intent(in) :: t1, at1, t2, at2, skip_index
        real(kind=mc_prec), intent(in) :: frac1, frac5, ft1
        type(single_molecule), intent(in) :: single_mol
        real(kind=mc_prec), intent(inout) :: energy_storage

        integer :: interaction
        real(kind=mc_prec) :: mixsigsq, mixeps4, mixcharge, shiftener, rsq

        interaction = atoms_interact(sys%species(t1)%atom(at1), sys%species(t2)%atom(at2))

        if(interaction == no_interaction) return
        if(t1 == t2 .and. skip_index == fractional_index) return

        associate(lj => sys%lennard_jones, coul => sys%coulombic)

        mixsigsq = ( lj%sigma(at2, at1, t2, t1) )**2
        mixeps4 = frac1*4._mc_prec*lj%epsilon(at2, at1, t2, t1)
        mixcharge = frac5*coul%charge_charge(at2, at1, t2, t1)
        shiftener = frac1*lj%shift_correction(at2, at1, t2, t1)

        rsq = sys%box%distsq(single_mol%X(at1), single_mol%Y(at1), single_mol%Z(at1), &
                                    sys%species(t2)%fractional_molecule%X(at2), &
                                    sys%species(t2)%fractional_molecule%Y(at2), &
                                    sys%species(t2)%fractional_molecule%Z(at2))

        energy_storage = energy_storage &
            + fractional_molecule_interaction(sys, t2, ft1, mixsigsq, mixeps4, mixcharge, shiftener, rsq)

        end associate
    end subroutine

    pure subroutine non_bonded_inner_loop(sys, t1, at1, t2, at2, frac1, frac5, ft1, single_mol, &
                                             rsq, energy_storage, skip_index, startat2)
        class(system), intent(in) :: sys
        integer, intent(in) :: t1, at1, t2, at2, skip_index, startat2
        real(kind=mc_prec), intent(in) :: frac1, frac5, ft1
        type(single_molecule), intent(in) :: single_mol

        real(kind=mc_prec), dimension(:), intent(inout) :: rsq
        real(kind=mc_prec), dimension(0:), intent(inout) :: energy_storage
        integer :: nmol, interaction, i
        real(kind=mc_prec) :: mixsigsq, mixeps4, mixcharge, shiftener

        nmol = sys%species(t2)%nmol
        interaction = atoms_interact(sys%species(t1)%atom(at1), sys%species(t2)%atom(at2))

        if(interaction == no_interaction) return

        associate(lj => sys%lennard_jones, coul => sys%coulombic, pos => sys%species(t2)%atom(at2)%pos)

        mixsigsq = ( lj%sigma(at2, at1, t2, t1) )**2
        mixeps4 = frac1*4._mc_prec*lj%epsilon(at2, at1, t2, t1)
        mixcharge = frac5*coul%charge_charge(at2, at1, t2, t1)
        shiftener = frac1*lj%shift_correction(at2, at1, t2, t1)

        call compute_fractional_interactions(sys, t1, at1, t2, at2, frac1, frac5, ft1, single_mol, &
                                             energy_storage(0), skip_index)

        rsq(startat2:nmol) = sys%box%distsq_array(single_mol%X(at1), single_mol%Y(at1), single_mol%Z(at1), &
                                           pos%X(startat2:nmol), pos%Y(startat2:nmol), pos%Z(startat2:nmol))
        if(t1 == t2) then
            !"Skip" this one, by setting the distance to more than cut - off
            if(skip_index .ne. fractional_index) rsq(skip_index) = sys%rc2 + 5._mc_prec
        end if

        call interact(interaction, energy_storage(startat2:nmol), sys, &
                        mixcharge, mixsigsq, mixeps4, shiftener, ft1, rsq(startat2:nmol), nmol - startat2 + 1)
        end associate

        !Add a strong repulsive interaction at a hard - sphere cutoff, 
        !in order to avoid some gross non - physical stuff
        !that occurs with e.g. fractional water
        do i = 1, nmol
            if(rsq(i) < HC_OVERLAP_SQ)then
                energy_storage(i) = energy_storage(i) + BIG_ENERGY
            end if
        end do

    end subroutine

    pure subroutine interact(interaction, energy_storage, sys, mixcharge, mixsigsq, mixeps4, shiftener, ft1, rsq, nmol)
        class(system), intent(in) :: sys
        integer, intent(in) :: nmol, interaction
        real(kind=mc_prec), intent(in) :: mixcharge, mixsigsq, mixeps4, shiftener, ft1
        real(kind=mc_prec), dimension(:), intent(in) :: rsq
        real(kind=mc_prec), dimension(:), intent(inout) :: energy_storage
        integer :: i

        select case(interaction)
        case(lj_and_elstat)
            call lj_frac_elstat(energy_storage(1:nmol), sys, &
                            mixcharge, mixsigsq, mixeps4, ft1, shiftener, rsq(1:nmol), sys%rc2, nmol)
        case(only_lj)
            do i = 1, nmol
                if(rsq(i) < sys%rc2) then
                    energy_storage(i) = energy_storage(i)&
                                      + ljener(mixsigsq, mixeps4, ft1, rsq(i))&
                                      + shiftener
                end if
            end do
        case(only_elstat)
            call elstat_only(sys%coulombic%erfc_lookup%array, rsq(1:nmol), nmol, sys%coulombic%erfc_lookup%rinv, &
                            energy_storage(1:nmol), mixcharge, sys%rc2)
        case(no_interaction)
        case default
        end select
    end subroutine

    pure subroutine lj_frac_elstat(storage, sys, charge, sigmasq, epsilon4, ft1, shift, rsq, rc2, nmol) 

        class(system), intent(in) :: sys
        real(kind=mc_prec), intent(in) :: sigmasq, epsilon4, shift, rc2, charge
        integer, intent(in) :: nmol
        real(kind=mc_prec), intent(in) :: ft1
        real(kind=mc_prec), dimension(1:nmol), intent(in) :: rsq
        real(kind=mc_prec), dimension(1:nmol), intent(inout) :: storage

        if(nmol > 0)then
            call elstat_and_lj_fractional(sys%coulombic%erfc_lookup%array, &
                                rsq, nmol, sys%coulombic%erfc_lookup%rinv, storage, charge, sigmasq, epsilon4, ft1, shift, rc2)
        end if
    end subroutine

    elemental function ljener(sigmasq, epsilon4, ft1, rsq) result(energy)
        real(kind=mc_prec), intent(in) :: sigmasq, epsilon4, ft1, rsq
        real(kind=mc_prec) :: energy
        real(kind=mc_prec) :: ri6

        ri6 = 1._mc_prec/( (rsq/sigmasq)**3 + ft1 )

        energy = epsilon4*ri6*(ri6 - 1._mc_prec)
        return
    end function

    elemental function direct_ewald_normal_normal(sys, chargeFactor, rsq) result(energy)
        use pretab_ewald_erfc_module
        class(system), intent(in) :: sys
        real(kind=mc_prec), intent(in) :: chargeFactor, rsq
        real(kind=mc_prec) :: energy
        real(kind=mc_prec) :: r

        r = sqrt(rsq)
        energy = chargeFactor*sys%coulombic%erfc_lookup%lookup(r)
        return
    end function

    pure function fractional_molecule_interaction(sys, t2, ft1, mixsigsq, mixeps4, mixcharge, shiftener, rsq) result(retval)
        class(system), intent(in) :: sys
        integer, intent(in) :: t2
        real(kind=mc_prec), intent(in) :: ft1, mixsigsq, mixeps4, mixcharge, shiftener, rsq
        real(kind=mc_prec) :: ft2, retval

        retval = 0._mc_prec
        if(sys%species(t2)%fluctuating)then 
            ft2 = zeta*(1._mc_prec - sys%species(t2)%frac)**2
            if(rsq < sys%rc2)then
                if(rsq < HC_OVERLAP_SQ)then
                    retval = retval + BIG_ENERGY
                end if
                retval = retval + sys%species(t2)%frac*ljener(mixsigsq, mixeps4, ft1 + ft2, rsq)&
                                + sys%species(t2)%frac*shiftener &
                                + sys%species(t2)%frac5*direct_ewald(sys, mixcharge, rsq)

            end if
        end if
    end function

    pure function atoms_interact(atom_1, atom_2) result(interaction_type)
        class(atom_type), intent(in) :: atom_1, atom_2
        integer :: interaction_type

        interaction_type = no_interaction
        if (atom_1%isvdw.and.atom_2%isvdw) then
    
            if (atom_1%ischarged.and.atom_2%ischarged) then
                interaction_type = lj_and_elstat   
            else
                interaction_type = only_lj
            end if
        else
            if (atom_1%ischarged.and.atom_2%ischarged) then
                interaction_type = only_elstat
            end if
        end if    
        return
    end function

end module

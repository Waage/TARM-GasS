module energy_module
    !Be aware that the majority of functions returning a dele requires dele to be set to zero before calling them. This is so that they can be easily used in compound moves.
    use direct_energy_storage_module, only: direct_energy_storage
    use ewald_module, only: ewald_type
    use molecule_module, only: single_molecule
    use molecule_species_module, only: molecule_species
    use non_bonded_interactions_module, only: non_bonded_interactions
    use parameters_module, only: FRACTIONAL_INDEX
    use precision_module, only: mc_prec
    use reduced_dimensionality_energy_storage_module, only: reduced_dimensionality_energy_storage
    use system_module, only: system
    implicit none
    private

    public :: epart_pos, etot_cfc, swap_pos_ener, epart_frac_single, epart_frac_insert, epart_frac_remove, &
                        swap_frac_ener, idchange_ener, corr_frac, insert_ener, remove_ener, ener_print_times

    real(kind=mc_prec), save :: time_epartdir = 0.0, epartfour = 0.0
    real(kind=mc_prec), save :: efracdir = 0.0, efracfour = 0.0
    real(kind=mc_prec), save :: etotdir = 0.0, etotfour = 0.0

contains 
    subroutine ener_print_times(file)
        integer :: file

        write(file, *) 'Time spent calculating energies:'
        write(file, *) 'epart_pos:', time_epartdir + epartfour
        write(file, *) 'Of which direct:', time_epartdir, 'indirect:', epartfour  
        write(file, *) 'epart_frac_single:', efracdir + efracfour
        write(file, *) 'Of which direct:', efracdir, 'indirect:', efracfour    
        write(file, *) 'etot_cfc:', etotdir + etotfour
        write(file, *) 'Of which direct:', etotdir, 'indirect:', etotfour
    
    end subroutine

    subroutine epart_pos(sys, molno, t1, coulombic, newpos, delta_fourier_ewald_energy, energy_storage)

        class(system), intent(in) :: sys
        type(single_molecule), intent(in) :: newpos
        type(single_molecule), allocatable :: oldpos
        integer, intent(in) :: t1, molno
        class(ewald_type), intent(inout), allocatable :: coulombic
        type(reduced_dimensionality_energy_storage), intent(inout) :: energy_storage

        real(kind=mc_prec), intent(out) :: delta_fourier_ewald_energy

        real(kind=mc_prec) :: tstart, tend

        call cpu_time(tstart)
        if(molno == FRACTIONAL_INDEX) then
            call non_bonded_interactions(sys, newpos, t1, sys%species(t1)%frac, molno, energy_storage)
        else
            call non_bonded_interactions(sys, newpos, t1, molno, energy_storage)
        end if
        call cpu_time(tend)
        time_epartdir = time_epartdir - tstart + tend
        delta_fourier_ewald_energy = 0._mc_prec
        oldpos = sys%species(t1)%get_molecule(molno)
        if(allocated(coulombic)) then
            call coulombic%fourier_ewald_diff(sys%species, newpos, oldpos, t1, (molno == FRACTIONAL_INDEX), &
                                     delta_fourier_ewald_energy)
        end if
        call cpu_time(tstart)
        epartfour = epartfour + tstart - tend
    end subroutine

    subroutine etot_cfc(sys, species, coulombic, toten, energy_storage)
        
        class(system), intent(in) :: sys

        type(molecule_species), dimension(:), intent(in) :: species
        class(ewald_type), intent(inout), allocatable :: coulombic
        type(direct_energy_storage), intent(inout) :: energy_storage
        real(kind=mc_prec), intent(out) :: toten

        real(kind=mc_prec) :: ewf
        integer :: t1, m1, t2
        real(kind=mc_prec) :: tstart, tend

        toten = 0._mc_prec
        !First compute all direct interactions
        call cpu_time(tstart)
        do t1 = 1, size(sys%species)
            do t2 = 1, size(sys%species)
                energy_storage%inner(t1, t2)%array = 0._mc_prec
            end do
        end do
        call non_bonded_interactions(sys, species, energy_storage)
        do t1 = 1, size(sys%species)
            do m1 = 0, species(t1)%nmol
                toten = toten + sum(energy_storage%inner(t1, t1)%array(m1:species(t1)%nmol, m1))
                energy_storage%inner(t1, t1)%array(m1, m1:species(t1)%nmol) =&
                                    energy_storage%inner(t1, t1)%array(m1:species(t1)%nmol, m1)
                do t2 = t1 + 1, size(sys%species)
                    toten = toten + sum(energy_storage%inner(t2, t1)%array(0:sys%species(t2)%nmol, m1))
                    energy_storage%inner(t1, t2)%array(m1, 0:species(t2)%nmol) =&
                                    energy_storage%inner(t2, t1)%array(0:species(t2)%nmol, m1)
                end do
            end do
        end do
        call cpu_time(tend)
        etotdir = etotdir - tstart + tend
        
        ewf = 0._mc_prec
        if(allocated(coulombic)) then
            call coulombic%fourier_ewald_total(species, ewf)
        end if
        call cpu_time(tstart)
        etotfour = etotfour + tstart - tend
        toten = toten + ewf + corr_total(sys, coulombic)

    end subroutine

    subroutine epart_frac_single(sys, frac_mol, molecule_type, coulombic, fold, fnew, corr_en, &
                                                             energy_storage, storage_index_passed, skip_index_passed)

        class(system), intent(in) :: sys
        real(kind=mc_prec), intent(in) :: fold, fnew
        integer, intent(in) :: molecule_type
        integer, intent(in), optional :: storage_index_passed, skip_index_passed
        class(ewald_type),  intent(inout), allocatable :: coulombic
        type(reduced_dimensionality_energy_storage), intent(inout) :: energy_storage
        real(kind=mc_prec), intent(out) :: corr_en
        real(kind=mc_prec) :: delta_fourier_ewald_energy

        integer :: storage_index, skip_index
        type(single_molecule), intent(in) :: frac_mol
        real(kind=mc_prec) :: tstart, tend

        if(present(storage_index_passed)) then
            storage_index = storage_index_passed
        else
            storage_index = 0
        end if
        if(present(skip_index_passed)) then
            skip_index = skip_index_passed
        else
            skip_index = 0
        end if

        call cpu_time(tstart)
        call non_bonded_interactions(sys, frac_mol, molecule_type, fnew, skip_index, energy_storage)
        call cpu_time(tend)
        efracdir = efracdir - tstart + tend
        delta_fourier_ewald_energy = 0._mc_prec
        if(allocated(coulombic)) then
            call coulombic%fourier_ewald_single_molecule(sys%species, frac_mol, molecule_type, &
                                                         fold, fnew, delta_fourier_ewald_energy)
        end if
        corr_en = corr_frac(sys, coulombic, fold, fnew, molecule_type)
        call cpu_time(tstart)
        efracfour = efracfour + tstart - tend
        
        corr_en = delta_fourier_ewald_energy + corr_en

    end subroutine

    subroutine epart_frac_insert(sys, currfrac, insmol, molecule_type, coulombic, fold, fnew, corr_en, &
                                   direct_storage_normal, direct_storage_fractional)

        class(system), intent(in) :: sys
        real(kind=mc_prec), intent(in) ::fold, fnew
        class(ewald_type), intent(inout), allocatable :: coulombic
        integer, intent(in) :: molecule_type

        type(reduced_dimensionality_energy_storage), intent(inout) :: direct_storage_normal, direct_storage_fractional
        real(kind=mc_prec), intent(out) :: corr_en
        type(single_molecule), intent(in) :: currfrac, insmol

        real(kind=mc_prec) :: tmpcorr

        !Obtain change from making current fractional molecule a full molecule

        call epart_frac_single(sys, currfrac, molecule_type, coulombic, fold, 1._mc_prec, &
                                    corr_en, direct_storage_normal, sys%species(molecule_type)%nmol + 1, 0)

        !Calculate energy of inserted molecule
        call insert_ener(sys, insmol, molecule_type, coulombic, fnew, tmpcorr, direct_storage_fractional, 0)
        corr_en = corr_en + tmpcorr

        !Calculate direct interaction between old fractional molecule and inserted molecule
        call sys%set_frac(molecule_type, fnew) !Ugh ugly ugly, change hack
        call non_bonded_interactions(sys, currfrac, molecule_type, sys%species(molecule_type)%nmol + 1, direct_storage_normal, &
                                          insmol, molecule_type, 0, direct_storage_fractional)
        !Revert hack
        call sys%set_frac(molecule_type, fold)

        !Remaining tail - corrections
        call sys%change_nmol(molecule_type, 1) !Slight hack for correction
        corr_en = corr_en + corr_frac(sys, coulombic, 0._mc_prec, fnew, molecule_type)
        call sys%change_nmol(molecule_type, -1) !Revert slight hack

        corr_en = corr_en + sys%species(molecule_type)%nmol*&
                                sys%lennard_jones%tail_correction(molecule_type, molecule_type)/sys%V !Correction due to non - linear term not accounted for in corr_frac

    end subroutine

    subroutine epart_frac_remove(sys, currfrac, newfrac, molecule_type, coulombic, fold, fnew, &
                                 corr_en, direct_storage_fractional, newfr_index)

        class(system), intent(in) :: sys
        type(single_molecule), intent(in) :: currfrac, newfrac
        integer, intent(in) :: molecule_type, newfr_index
        real(kind=mc_prec), intent(in) :: fold, fnew
 
        class(ewald_type), intent(inout), allocatable :: coulombic
        type(reduced_dimensionality_energy_storage), intent(inout) :: direct_storage_fractional
        real(kind=mc_prec), intent(out) :: corr_en

        real(kind=mc_prec) :: delta_fourier_ewald_energy 

        
        delta_fourier_ewald_energy = 0._mc_prec
        !For molecule 1, we already know what the direct change is, but we must calculate the change due to ewald summation (delta_fourier_ewald_energy), and corrections
        call remove_ener(sys, currfrac, molecule_type, fold, coulombic, delta_fourier_ewald_energy) 
        !Change in tail corrections from changing the fraction from fold to 0
        corr_en = corr_frac(sys, coulombic, fold, 0._mc_prec, molecule_type) + delta_fourier_ewald_energy

        !Change in tail corrections due to moving from N to N - 1 integer molecules, fraction from 0 to 1
        corr_en = corr_en - (sys%species(molecule_type)%nmol - 1)&
                           *sys%lennard_jones%tail_correction(molecule_type, molecule_type)/sys%V

        call sys%set_frac(molecule_type, 1._mc_prec)!Check if this is redundant
        !Calculate change in energy due to molecule 2 being converted to a fractional molecule
        delta_fourier_ewald_energy = 0._mc_prec
        call epart_frac_single(sys, newfrac, molecule_type, coulombic, 1._mc_prec, fnew, &
                                                     delta_fourier_ewald_energy, direct_storage_fractional, 0, newfr_index)
        direct_storage_fractional%inner(molecule_type)%array(0) = 0._mc_prec
        call sys%set_frac(molecule_type, fold)
        !epart_frac_single computes tail corrections as though there were N molecules
        !in the system, but we have removed one. Correct for this by recomputing it with the
        !correct number of molecules
        corr_en = corr_en + delta_fourier_ewald_energy - corr_frac(sys, coulombic, 1._mc_prec, fnew, molecule_type)
        call sys%change_nmol(molecule_type, -1)
        corr_en = corr_en + corr_frac(sys, coulombic, 1._mc_prec, fnew, molecule_type)
        call sys%change_nmol(molecule_type, 1)

    end subroutine

    pure subroutine swap_pos_ener(sys, newpos1, newpos2, i1, i2, t1, t2, coulombic, delta_fourier_ewald_energy, &
                                                  direct_storage_1, direct_storage_2)
        !Calculate energy of two molecules swapping positions

        class(system), intent(in) :: sys
        integer, intent(in) :: i1, i2, t1, t2
        type(single_molecule), intent(in) :: newpos1, newpos2
        type(single_molecule), allocatable :: oldpos1, oldpos2

        class(ewald_type), intent(inout), allocatable :: coulombic
        real(kind=mc_prec), intent(out) :: delta_fourier_ewald_energy
        type(reduced_dimensionality_energy_storage), intent(inout) :: direct_storage_1, direct_storage_2

        !Compute direct interactions of swapped molecules against the rest of the system
        if(i1 == FRACTIONAL_INDEX) then
            call non_bonded_interactions(sys, newpos1, t1, sys%species(t1)%frac, i1, direct_storage_1)
        else
            call non_bonded_interactions(sys, newpos1, t1, i1, direct_storage_1)
        end if
        if(i2 == FRACTIONAL_INDEX) then
            call non_bonded_interactions(sys, newpos2, t2, sys%species(t2)%frac, i2, direct_storage_2)
        else
            call non_bonded_interactions(sys, newpos2, t2, i2, direct_storage_2)
        end if
        !Recompute direct interaction between swapped molecules.
        direct_storage_1%inner(t2)%array(i2) = 0._mc_prec
        direct_storage_2%inner(t1)%array(i1) = 0._mc_prec
        call non_bonded_interactions(sys, newpos1, t1, i1, direct_storage_1, newpos2, t2, i2, direct_storage_2)
        
        delta_fourier_ewald_energy = 0._mc_prec
        oldpos1 = sys%species(t1)%get_molecule(i1)
        oldpos2 = sys%species(t2)%get_molecule(i2)
        if(allocated(coulombic)) then
            call coulombic%fourier_ewald_diff(sys%species, newpos1, oldpos1, t1, (i1 == FRACTIONAL_INDEX), &
                                                delta_fourier_ewald_energy)
            call coulombic%fourier_ewald_diff(sys%species, newpos2, oldpos2, t2, (i2 == FRACTIONAL_INDEX), &
                                                delta_fourier_ewald_energy)
        end if

    end subroutine

    pure subroutine swap_frac_ener(sys, currfrac, newfrac, molecule_type, newfr_index, &
                                   coulombic, ewf_en, direct_storage_frac, direct_storage_newfr)
        !Possibly deprecated as swap pos can be used (evaluate time cost of different fourier transforms)
        class(system), intent(in) :: sys
        class(ewald_type), intent(inout), allocatable :: coulombic
        type(reduced_dimensionality_energy_storage), intent(inout) :: direct_storage_frac, direct_storage_newfr
        real(kind=mc_prec), intent(out) :: ewf_en

        integer, intent(in) :: molecule_type, newfr_index
        type(single_molecule), intent(in) :: currfrac, newfrac

        !Calculate direct interactions between the molecules and the rest of the system
        call non_bonded_interactions(sys, newfrac, molecule_type, sys%species(molecule_type)%frac, 0, direct_storage_frac)
        call non_bonded_interactions(sys, currfrac, molecule_type, newfr_index, direct_storage_newfr)
     
        !Calculating the new direct interaction between the molecules themselves  
        !is not necessary, as this must be symmetric. 
        !(Unless there is some dumb anisotropy, but that sounds hilariously unphysical)
        direct_storage_newfr%inner(molecule_type)%array(0) = &
                        sys%energy_storage%inner(molecule_type, molecule_type)%array(0, newfr_index)
        direct_storage_frac%inner(molecule_type)%array(newfr_index) = &
                        sys%energy_storage%inner(molecule_type, molecule_type)%array(0, newfr_index)

        !Calculate change in the fourier space contribution to energy
        ewf_en = 0._mc_prec
        if(allocated(coulombic)) then
            call coulombic%fourier_ewald_single_molecule(sys%species, currfrac, molecule_type, &
                                                             sys%species(molecule_type)%frac, 1._mc_prec, ewf_en)
            call coulombic%fourier_ewald_single_molecule(sys%species,  newfrac, molecule_type, &
                                                             1._mc_prec, sys%species(molecule_type)%frac, ewf_en)
        end if
        ewf_en = ewf_en

    end subroutine


    pure subroutine idchange_ener(sys, outmol, insmol, om, im, ot, it, coulombic, indirect_energy, direct_storage_in)
        !Calculates the change in energy due to changing the identity (molecule type) of one molecule

        class(system), intent(in) :: sys
        integer, intent(in) :: om, im, it, ot
        class(ewald_type), intent(inout), allocatable :: coulombic
        type(reduced_dimensionality_energy_storage), intent(inout) :: direct_storage_in
        real(kind=mc_prec), intent(out) :: indirect_energy

        integer :: i
        type(single_molecule), intent(in) :: insmol, outmol

        error stop "idchange_ener is not correctly implemented"
        call non_bonded_interactions(sys, insmol, it, im, direct_storage_in)
        direct_storage_in%inner(ot)%array(om) = 0._mc_prec

        indirect_energy = 0._mc_prec
        if(allocated(coulombic)) then
            call coulombic%fourier_ewald_single_molecule(sys%species, outmol, ot, 1._mc_prec, 0._mc_prec, indirect_energy)
            call coulombic%fourier_ewald_single_molecule(sys%species, insmol, it, 0._mc_prec, 1._mc_prec, indirect_energy)
        end if
        !Tail corrections, worst corrections:
        indirect_energy = indirect_energy + coulombic%correction_terms(it) - coulombic%correction_terms(ot)
        !Straightforward corrections due to addition/removal of molecule
        do i = 1, size(sys%species)
             indirect_energy = indirect_energy &
                             + (sys%lennard_jones%tail_correction(it, i) &
                                - sys%lennard_jones%tail_correction(ot, i))*(sys%species(i)%nmol + sys%species(i)%frac)/sys%V
        end do
        !Crossterms in removed and inserted moleculetype
        indirect_energy = indirect_energy + sys%lennard_jones%tail_correction(it, it)*(sys%species(it)%nmol + 1)/sys%V
        indirect_energy = indirect_energy + sys%lennard_jones%tail_correction(ot, ot)*(1 - sys%species(ot)%nmol)/sys%V
        indirect_energy = indirect_energy - sys%lennard_jones%tail_correction(it, ot)/sys%V


    end subroutine

    pure subroutine insert_ener(sys, insmol, molecule_type, coulombic, fnew, f_ewald_en, energy_storage, skip_index)
        !Energy from inserting a fractional molecule of type molecule_type with fractional parameter fnew into the box. Sans corrections. 
        class(system), intent(in) :: sys

        integer, intent(in) :: molecule_type, skip_index

        class(ewald_type), intent(inout), allocatable :: coulombic
        real(kind=mc_prec), intent(in) :: fnew
        real(kind=mc_prec), intent(out) :: f_ewald_en
        type(reduced_dimensionality_energy_storage), intent(inout) :: energy_storage
        type(single_molecule), intent(in) :: insmol


        call non_bonded_interactions(sys, insmol, molecule_type, fnew, skip_index, energy_storage)
        f_ewald_en = 0._mc_prec
        if(allocated(coulombic))then
            call coulombic%fourier_ewald_single_molecule(sys%species, insmol, molecule_type, 0._mc_prec, fnew, f_ewald_en)
        end if

    end subroutine

    pure subroutine remove_ener(sys, remmol, molecule_type, fold, coulombic, ewf)
        !Energy due to removing a molecule. 
        !Neglects the direct interaction energy of the removed molecule
        !as this is allready known and stored
        !Does calculate the fourier part of the ewald summation

        class(system), intent(in) :: sys
        type(single_molecule), intent(in) :: remmol
        integer, intent(in) :: molecule_type
        class(ewald_type), intent(inout), allocatable :: coulombic
        real(kind=mc_prec), intent(in) :: fold
        real(kind=mc_prec), intent(out) :: ewf

        ewf = 0._mc_prec
        if(allocated(coulombic))then
            call coulombic%fourier_ewald_single_molecule(sys%species, remmol, molecule_type, fold, 0._mc_prec, ewf)
        end if
    end subroutine

    pure real(kind=mc_prec) function corr_frac(sys, coulombic, fold, fnew, t)
        !Corrections to the fractional energy due to a change of the fractional parameter of type t from fold to fnew
        class(system), intent(in) :: sys
        real(kind=mc_prec), intent(in) :: fnew, fold
        class(ewald_type), intent(in), allocatable :: coulombic
        real(kind=mc_prec) :: ljcorr
        integer, intent(in) :: t
        integer :: i

        ljcorr = 0._mc_prec
        do i = 1, size(sys%species)
             if(i == t)then
                    ljcorr = ljcorr + sys%lennard_jones%tail_correction(t, i) &
                                     *(sys%species(i)%nmol*(fnew - fold) + (fnew*fnew - fold*fold))/sys%V
                    !This treats the long tail correction of the fractional molecule as though they do not have a length scaling.
             else
                    ljcorr = ljcorr + sys%lennard_jones%tail_correction(t, i)&
                                     *(sys%species(i)%nmol + sys%species(i)%frac)*(fnew - fold)/sys%V
             end if
        end do
        if(allocated(coulombic))then
            corr_frac = (fnew**10 - fold**10)*coulombic%correction_terms(t) + ljcorr
        else
            corr_frac = ljcorr
        end if
    end function

    pure real(kind=mc_prec) function corr_total(sys, coulombic)
        class(system), intent(in) :: sys
        class(ewald_type), intent(in), allocatable :: coulombic
        integer :: i, j
        real(kind=mc_prec) fraci, fracj, ni, nj
        corr_total = 0._mc_prec
        do i = 1, size(sys%species)
            ni = sys%species(i)%nmol
            if(sys%species(i)%fluctuating)then
                fraci = sys%species(i)%frac
                if(allocated(coulombic))then
                    corr_total = corr_total + coulombic%correction_terms(i)*fraci**10
                end if
            else
                fraci = 0._mc_prec
            end if
            corr_total = corr_total + coulombic%correction_terms(i)*ni
            corr_total = corr_total + sys%lennard_jones%tail_correction(i, i)*((ni + fraci)*ni + fraci*fraci)/sys%V
            do j = i + 1, size(sys%species)
                nj = sys%species(j)%nmol
                if(sys%species(j)%fluctuating)then
                    fracj = sys%species(j)%frac
                else
                    fracj = 0._mc_prec
                end if
                !LJ cut - off
                corr_total = corr_total + sys%lennard_jones%tail_correction(i, j)*(ni + fraci)*(nj + fracj)/sys%V
                !The above equations treats the long tail correction of the fractional molecule
                ! as though they do not have a length scaling.
            end do
        end do

    end function

end module energy_module

//Define the precision of the program. Default: double, optional single
#ifndef _C_DEFINITIONS
#define _C_DEFINITIONS
 /*Some of the macros use syntax which is valid c++, but nonstandard c (i.e. some, but not all c compilers will complain).
  In case we have to use a c++ compiler, we need to avoid mangling of the function names.*/
#ifdef __cplusplus 
extern "C" {
#endif

/*Uncomment to make single precision, comment to make double precision*/
// #define c_prec_single


#ifndef c_prec_single
#ifndef c_prec_double
	#define c_prec_double
#endif
#endif

#include <x86intrin.h>
#include "immintrin.h"

#define simd_shuffle_bottop(a,b) _mm256_shuffle_ps(a,b,_MM_SHUFFLE(1,0,1,0))
#define simd_shuffle_topbot(a,b) _mm256_shuffle_ps(a,b,_MM_SHUFFLE(3,2,3,2))

#ifdef c_prec_double

	#define c_real double
	//Define the simd architecture. Currently only supporting 256 bit simd lanes
	#define N_PER_VEC 4
	#define c_simd_real __m256d
	#define c_simd_int __m128i
	#define simd_load_unaligned _mm256_loadu_pd
	#define simd_store_unaligned _mm256_storeu_pd
	#define simd_set_value _mm256_set1_pd
	#define simd_add_real _mm256_add_pd
	#define simd_subtract_real _mm256_sub_pd
	#define simd_multiply_real _mm256_mul_pd
	#define simd_divide_real _mm256_div_pd
	#define simd_square_root_real _mm256_sqrt_pd
	#define simd_convert_real_to_int_truncuate _mm256_cvttpd_epi32
	#define simd_any _mm256_movemask_pd 
	#define simd_bitwise_and _mm256_and_pd
	#define simd_extract _mm_extract_epi32

	#define simd_compare_less_than(a,b) _mm256_cmp_pd(a,b,_CMP_LT_OQ)
	#define simd_round_down_real(a) _mm256_round_pd(a,_MM_FROUND_FLOOR)

	#define simd_unpack_low_lane _mm256_unpacklo_pd
	#define simd_unpack_high_lane _mm256_unpackhi_pd
	#define simd_permute _mm256_permute2f128_pd

#endif

#ifdef c_prec_single

	#define c_real float
	//Define the simd architecture. Currently only supporting 256 bit simd lanes
	#define N_PER_VEC 8
	#define c_simd_real __m256
	#define c_simd_int __m256i
	#define simd_load_unaligned _mm256_loadu_ps
	#define simd_store_unaligned _mm256_storeu_ps
	#define simd_set_value _mm256_set1_ps
	#define simd_add_real _mm256_add_ps
	#define simd_subtract_real _mm256_sub_ps
	#define simd_multiply_real _mm256_mul_ps
	#define simd_divide_real _mm256_div_ps
	#define simd_square_root_real _mm256_sqrt_ps
	#define simd_convert_real_to_int_truncuate _mm256_cvttps_epi32
	#define simd_any _mm256_movemask_ps 
	#define simd_bitwise_and _mm256_and_ps
	#define simd_extract _mm256_extract_epi32


	#define simd_compare_less_than(a,b) _mm256_cmp_ps(a,b,_CMP_LT_OQ)
	#define simd_round_down_real(a) _mm256_round_ps(a,_MM_FROUND_FLOOR)

	#define simd_unpack_low_lane _mm256_unpacklo_ps
	#define simd_unpack_high_lane _mm256_unpackhi_ps
	#define simd_permute _mm256_permute2f128_ps

#endif


/* Predeclarations of functions. Note that we don't actually *need* this for fortran linkage - but putting them here 
	lets us avoid ifdef guarding the .c file, which makes things a bit cleaner. Plus, there is no reason why these functions *shouldn't* be callable externally*/
void lookup_erfc_ewald_and_lennard_jones(c_real *table, c_real *position, int nmol, c_real table_scale, c_real *result,
											  c_real charge, c_real sigmasq, c_real epsilon4, c_real shiftener, c_real cutoff);
void lookup_erfc_ewald_and_lennard_jones_fractional(c_real *table, c_real *position, int nmol, c_real table_scale, c_real *result,
											  c_real charge, c_real sigmasq, c_real epsilon4, c_real ft1, c_real shiftener, c_real cutoff);
void lookup_erfc_ewald(c_real *table, c_real *position, int nmol, c_real table_scale, c_real *result, c_real charge, c_real cutoff);
#ifdef __cplusplus
}
#endif
#endif
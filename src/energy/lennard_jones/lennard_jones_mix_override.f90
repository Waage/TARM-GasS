module lennard_jones_mix_override_module
    !Extremely happy about how structs are handled in fortran
    use precision_module, only: mc_prec
    implicit none
    private

    public :: lennard_jones_mix_override

    type lennard_jones_mix_override

        integer, dimension(4) :: pair_id

        real(kind=mc_prec) :: value

    contains

        procedure :: apply_override
        
    end type

    interface lennard_jones_mix_override
        module procedure new_lennard_jones_mix_override
    end interface

contains

    pure function new_lennard_jones_mix_override(atom_index_1, atom_index_2, species_index_1, species_index_2, value)
        type(lennard_jones_mix_override) :: new_lennard_jones_mix_override
        integer, intent(in) :: atom_index_1, atom_index_2, species_index_1, species_index_2
        real(kind=mc_prec), intent(in) :: value

        new_lennard_jones_mix_override%value = value
        new_lennard_jones_mix_override%pair_id = [atom_index_1, atom_index_2, species_index_1, species_index_2]

    end function

    pure subroutine apply_override(or, array)
        class(lennard_jones_mix_override), intent(in) :: or
        real(kind=mc_prec), dimension(:, :, :, :), intent(inout) :: array

        !Interactions are symmetric so if (m1, a1) < - > (m2, a2) = value  then (m2, a2) < - > (m1, a1) = value
        array(or%pair_id(1), or%pair_id(2), or%pair_id(3), or%pair_id(4)) = or%value
        array(or%pair_id(2), or%pair_id(1), or%pair_id(4), or%pair_id(3)) = or%value

    end subroutine

end module

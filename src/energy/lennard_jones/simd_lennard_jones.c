#include "simd_lennard_jones.h"

c_simd_real calculate_lennard_jones_interactions(c_simd_real v_distance_squared, c_simd_real v_lj_sigma_squared, c_simd_real v_lj_4_epsilon)
{
    const c_simd_real v_ONE = simd_set_value(1.0);

    c_simd_real _ri2 = simd_divide_real(v_lj_sigma_squared, v_distance_squared);
    c_simd_real _ri6 = simd_multiply_real(simd_multiply_real(_ri2, _ri2), _ri2);
    return simd_multiply_real(v_lj_4_epsilon, simd_multiply_real(_ri6, simd_subtract_real(_ri6, v_ONE)));
}


c_simd_real calculate_fractional_lennard_jones_interactions(c_simd_real v_distance_squared, c_simd_real v_lj_fractional_distance_displacement, c_simd_real v_inverse_lj_sigma_squared, c_simd_real v_lj_4_epsilon)
{
    const c_simd_real v_ONE = simd_set_value(1.0);
    c_simd_real _ri2 = simd_multiply_real(v_inverse_lj_sigma_squared,v_distance_squared);
    c_simd_real _ri6 = simd_divide_real(v_ONE, simd_add_real(simd_multiply_real(simd_multiply_real(_ri2, _ri2), _ri2), v_lj_fractional_distance_displacement)); //x = 1/((r/sigma)**6 + zeta*(1-lambda)**2))
    return simd_multiply_real(v_lj_4_epsilon,simd_multiply_real(_ri6,simd_subtract_real(_ri6,v_ONE))); // 4*epsilon*((x)^2 - x)
}
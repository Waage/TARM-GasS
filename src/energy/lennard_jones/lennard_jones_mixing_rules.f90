module lennard_jones_mixing_rules
    use molecule_module, only: single_molecule
    use precision_module, only: mc_prec
    implicit none
    private
    public :: lennard_jones_mixing_rules_method

    type :: lennard_jones_mixing_rules_method

    contains

        procedure, nopass :: mix_sigma
        procedure, nopass :: mix_epsilon
        
    end type lennard_jones_mixing_rules_method

contains
    
    !Default mixing rules: Lorentz - Berthelot

    pure function mix_sigma(out_dimensions, molecule_1, molecule_2) result(sigma_mix)
        type(single_molecule), intent(in) :: molecule_1, molecule_2
        integer, intent(in) :: out_dimensions
        real(kind=mc_prec), dimension(out_dimensions, out_dimensions) :: sigma_mix
        integer :: i, j

        sigma_mix = 0._mc_prec
        do i = 1, molecule_1%nats
            do j = 1, molecule_2%nats
                sigma_mix(j, i) = 0.5_mc_prec*(molecule_1%lj_sigma(i) + molecule_2%lj_sigma(j))
            end do 
        end do 

    end function

    pure function mix_epsilon(out_dimensions, molecule_1, molecule_2) result(epsilon_mix)
        type(single_molecule), intent(in) :: molecule_1, molecule_2
        integer, intent(in) :: out_dimensions
        real(kind=mc_prec), dimension(out_dimensions, out_dimensions) :: epsilon_mix
        integer :: i, j
        epsilon_mix = 0._mc_prec
        do i = 1, molecule_1%nats
            do j = 1, molecule_2%nats
                epsilon_mix(j, i) = sqrt(molecule_1%lj_epsilon(i) * molecule_2%lj_epsilon(j))
            end do 
        end do 
    end function
    
end module lennard_jones_mixing_rules

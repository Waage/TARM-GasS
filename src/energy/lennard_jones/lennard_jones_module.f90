module lennard_jones_module
    use general_utilities_module, only: is_nonzero
    use lennard_jones_mixing_rules, only: lennard_jones_mixing_rules_method
    use lennard_jones_mix_override_module, only: lennard_jones_mix_override
    use molecule_module, only: single_molecule
    use parameters_module, only: pi
    use precision_module, only: mc_prec
    implicit none
    private

    public :: lennard_jones_interaction

    type lennard_jones_interaction
        
        real(kind=mc_prec), dimension(:, :, :, :), allocatable :: epsilon, sigma
        real(kind=mc_prec), dimension(:, :, :, :), allocatable :: shift_correction
        real(kind=mc_prec), dimension(:, :), allocatable :: tail_correction 

    contains

        procedure :: set_tail_correction
        procedure :: update_shift_correction

    end type

    interface lennard_jones_interaction
        module procedure :: new_lennard_jones_interaction
    end interface

contains

    pure function new_lennard_jones_interaction(prototype, sigma_override, epsilon_override, r_cut) result(lj)
        type(lennard_jones_interaction) :: lj
        class(single_molecule), dimension(:), intent(in) :: prototype       
        type(lennard_jones_mixing_rules_method) :: ljmm !TODO: Make this an input argument, polymorphic
        type(lennard_jones_mix_override), dimension(:), intent(in) :: sigma_override, epsilon_override
        real(kind=mc_prec), intent(in) :: r_cut
        integer :: n_types, max_n_atoms, i, j

        n_types = size(prototype)
        max_n_atoms = 0
        do i = 1, n_types
            if(prototype(i)%nats > max_n_atoms) max_n_atoms = prototype(i)%nats
        end do

        allocate(lj%tail_correction(max_n_atoms, n_types))
        allocate(lj%shift_correction(max_n_atoms, max_n_atoms, n_types, n_types))
        allocate(lj%epsilon(max_n_atoms, max_n_atoms, n_types, n_types))
        allocate(lj%sigma(max_n_atoms, max_n_atoms, n_types, n_types))

        lj%epsilon = 0._mc_prec
        lj%sigma = 0._mc_prec
        do i = 1, n_types
            do j = 1, n_types
                lj%epsilon(:, :, j, i) = ljmm%mix_epsilon(max_n_atoms, prototype(i), prototype(j))
                lj%sigma(:, :, j, i) = ljmm%mix_sigma(max_n_atoms, prototype(i), prototype(j))
            end do
        end do
        do i = 1, size(sigma_override)
            call sigma_override(i)%apply_override(lj%sigma)
        end do
        do i = 1, size(epsilon_override)
            call epsilon_override(i)%apply_override(lj%epsilon)
        end do

        call lj%update_shift_correction(r_cut)
        call lj%set_tail_correction(r_cut)

    end function

    pure subroutine update_shift_correction(lj, r_cut)
        class(lennard_jones_interaction), intent(inout) :: lj
        real(kind=mc_prec), intent(in) :: r_cut
        integer :: t1, t2, at1, at2
        real(kind=mc_prec) :: rci6
        real(kind=mc_prec) :: epsilon4

        !NB: The fractional molecule lennard - jones shift correction is always equal to the 
        !non - fractional molecule' shift correction
        !Pro: simplifies program's logic
        !Con: shift correction isn't completely accurate for a fractional parameter different from 0 or 1
        !Reason why con is ok for now: fractional molecule is non - physical, non - physicality is hoped to disappear in
        !thermodynamic limit, this *should* not alter that 

        !This routine isn't time - critical, so doing lots of suboptimal stuff here 
        lj%shift_correction = 0._mc_prec
        do t1 = 1, size(lj%sigma, dim=4)
            do t2 = t1, size(lj%sigma, dim=3)
                do at1 = 1, size(lj%sigma, dim=2)
                    do at2 = 1, size(lj%sigma, dim=1)
                        epsilon4 = 4._mc_prec*lj%epsilon(at2, at1, t2, t1)
                        if(.not.is_nonzero(epsilon4)) cycle
                        rci6 = (lj%sigma(at2, at1, t2, t1)/r_cut)**6
                        lj%shift_correction(at2, at1, t2, t1) = - epsilon4*(rci6*rci6 - rci6)
                        lj%shift_correction(at1, at2, t1, t2) = - epsilon4*(rci6*rci6 - rci6)
                    end do
                end do
            end do
        end do

    end subroutine

    pure subroutine set_tail_correction(lj, r_cut)
        class(lennard_jones_interaction), intent(inout) :: lj
        real(kind=mc_prec), intent(in) :: r_cut
        integer :: t1, t2, at1, at2
        real(kind=mc_prec) :: rci3, rci6
        real(kind=mc_prec) :: sigma, epsilon_var 
        !epsilon_var is a stupid name but apparently epsilon is an intrinsic so just preemptively avoiding a nameclash

        lj%tail_correction = 0._mc_prec
        !Almost certainly not time - critical, so using size for program simplicity
        do t1 = 1, size(lj%sigma, dim=4)
            do t2 = t1, size(lj%sigma, dim=3)
                do at1 = 1, size(lj%sigma, dim=2)
                    do at2 = 1, size(lj%sigma, dim=1)
                        epsilon_var = lj%epsilon(at2, at1, t2, t1)
                        if(.not.is_nonzero(epsilon_var)) cycle
                        sigma = lj%sigma(at2, at1, t2, t1)
                        rci3 = (sigma/r_cut)**3
                        rci6 = rci3*rci3
                        lj%tail_correction(t2, t1) = lj%tail_correction(t2, t1) + 8._mc_prec*pi/3._mc_prec *&
                            (& 
                               epsilon_var*sigma**3*rci3*(1/3._mc_prec*rci6 - 1._mc_prec) & !"Normal" lj term
                             + epsilon_var*(r_cut**3)*rci6*(rci6 - 1._mc_prec) & !Shift correction term
                            )
                    end do
                end do
                lj%tail_correction(t1, t2) = lj%tail_correction(t2, t1)

            end do
        end do

    end subroutine

end module

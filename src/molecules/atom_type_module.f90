module atom_type_module
    use geometry_module, only: box_type
    use precision_module, only: mc_prec
    implicit none
    private

    public :: atom_list, atom_type, single_atom

    type atom_list 
        !List of atom positions. Note that the coordinates are stored in three seperate arrays; i.e
        !they are stored as XXXXXXXXXXXXXXXYYYYYYYYYYYYYYYZZZZZZZZZZZZZZZ
        !And not as XYZXYZXYZXYZ which would intuitively have been better, but breaks auto - vectorization

        real(kind=mc_prec), dimension(:), allocatable :: X, Y, Z
    
    end type 

    type atom_type
        !Contains information about the lennard jones parameters and charge of a single atom type
        !As well as the positions of all the atoms of this type
        !In one system, each molecule type (potentially) present in the system should be instantiated with N member variables of type atom_type
        !The list of the atom type should contain the positions  of all atoms of this type currently present in the system
        character(8) :: name = 'tmpAtNam'
        logical :: ischarged, isvdw
        real(kind=mc_prec) :: lj_sigma, lj_epsilon, charge, weight
        type(atom_list) :: pos

    contains

        procedure :: reinsert
    
    end type

    type single_atom
        !A single molecule, useful for locality when e.g. calculating energy of displacing one molecule, insertion, etc
        logical :: ischarged, isvdw
        integer :: internal_index !Which atom in the species this atom corresponds to (i.e. in tip4p water stored as O, H, H, M; the internal_index is €{1..4}, with 1 correspoinding to Oxygen)
        real(kind=mc_prec) :: lj_sigma, lj_epsilon, charge
        real(kind=mc_prec) :: X, Y, Z
    contains

    end type

    interface atom_list
        module procedure new_atom_list
    end interface

contains

    pure type(atom_list) function new_atom_list(n)
        integer, intent(in) :: n

        allocate(new_atom_list%X(n))
        allocate(new_atom_list%Y(n))
        allocate(new_atom_list%Z(n))
    
    end function

    subroutine reinsert(atom, box)
        class(atom_type) :: atom
        class(box_type), intent(in) :: box

        call box%apply_boundary_conditions(atom%pos%X(:), atom%pos%Y(:), atom%pos%Z(:))
    end subroutine
    
end module atom_type_module

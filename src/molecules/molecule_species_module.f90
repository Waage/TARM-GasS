module molecule_species_module
    use atom_type_module, only: atom_type, atom_list
    use geometry_module, only: box_type
    use general_utilities_module, only: is_nonzero, first_multiple_greater, resize_0_indexed_1D
    use molecule_module, only: single_molecule
    use parameters_module, only: safety_pad, storage_increment
    use precision_module, only: mc_prec
    use wang_landau_bias_module, only: wang_landau_bias
    implicit none
    private

    public :: molecule_species

    type molecule_species
        !Contains information about the number of molecules, and its constituent atoms
        type(single_molecule), allocatable :: prototype
        type(single_molecule), allocatable :: fractional_molecule
        integer :: nmol, nats, nvdw, ncharged
        integer :: storage_space
        integer :: rot_type = 3 !Spatial as default
        logical :: fluctuating = .false.
        type(atom_type), dimension(:), allocatable :: atom
        real(kind=mc_prec) :: mu = - 1000
        real(kind=mc_prec) :: frac = 0._mc_prec, frac5 = 0._mc_prec
        character(8) :: molname
        type(wang_landau_bias) :: bias
        !To be implemented
        ! integer :: n_angles
        ! integer, dimension(:, :) :: allocatable :: angle_indices 
        ! real(kind=mc_prec), dimension(:), allocatable :: angle_bond_strength
        ! integer :: n_flex_bonds
        ! integer, dimension(:, :) :: allocatable :: bond_indices 
        ! real(kind=mc_prec), dimension(:), allocatable :: bond_neutral_length, bond_strength

    contains

        procedure :: initialize
        procedure :: reinsert
        procedure :: increase_size
        procedure :: assemble
        procedure :: rescale
        procedure :: set_nmol
        procedure :: can_rotate
        procedure :: has_movable_molecule
        procedure :: get_molecule
        procedure :: set_molecule
        procedure :: get_fractional_molecule
        procedure :: set_fractional_molecule
        procedure :: get_prototype

    end type
contains

    elemental subroutine initialize(species, prototype)
        !A mildly idiotic approach, due to nmol being stored only in the species container. Duplicate information might fix, but eh?
        class(molecule_species), intent(inout) :: species
        type(single_molecule), intent(in) :: prototype
        integer :: i

        species%prototype = prototype

        species%nats = prototype%nats 
        species%ncharged = prototype%ncharged 
        species%nvdw = prototype%nvdw

        species%rot_type = prototype%rot_type

        allocate(species%atom(species%nats))
        do i = 1, species%nats
          species%atom(i)%name = prototype%name(i)
          species%atom(i)%weight = prototype%weight(i)
          species%atom(i)%isvdw = prototype%isvdw(i)
          species%atom(i)%lj_epsilon = prototype%lj_epsilon(i)
          species%atom(i)%lj_sigma = prototype%lj_sigma(i)
          species%atom(i)%ischarged = prototype%ischarged(i)
          species%atom(i)%charge = prototype%charge(i)
          allocate(species%atom(i)%pos%X(species%storage_space))
          allocate(species%atom(i)%pos%Y(species%storage_space))
          allocate(species%atom(i)%pos%Z(species%storage_space))
        end do
        species%molname = prototype%molname

        species%fractional_molecule = prototype
        species%bias = wang_landau_bias(nBins = 10, is_operational_bias = species%fluctuating) !TODO: Make less hardcoded

    end subroutine

    subroutine reinsert(species, box)
        class(molecule_species) :: species
        class(box_type), intent(in) :: box

        integer :: i
        do i = 1, species%nats
            call species%atom(i)%reinsert(box)
        end do

    end subroutine

    subroutine increase_size(spec)
        class(molecule_species) :: spec
        integer :: i

        do i = 1, spec%nats
            call resize_0_indexed_1D(spec%atom(i)%pos%X, spec%storage_space, spec%storage_space + storage_increment)
            call resize_0_indexed_1D(spec%atom(i)%pos%Y, spec%storage_space, spec%storage_space + storage_increment)
            call resize_0_indexed_1D(spec%atom(i)%pos%Z, spec%storage_space, spec%storage_space + storage_increment)
        end do

    end subroutine

    subroutine assemble(species, box)
        class(molecule_species) :: species
        class(box_type), intent(in) :: box
        integer :: i, m
        real(kind=mc_prec), dimension(3) :: dr

        if (allocated(species%fractional_molecule)) then
            call species%fractional_molecule%assemble(box)
        endif

        do i = 2, species%nats
            do m = 1, species%nmol
                dr = box%nearest_vector(species%atom(i)%pos%X(m), species%atom(1)%pos%X(m), &
                                        species%atom(i)%pos%Y(m), species%atom(1)%pos%Y(m), &
                                        species%atom(i)%pos%Z(m), species%atom(1)%pos%Z(m))

                species%atom(i)%pos%X(m) = dr(1) + species%atom(1)%pos%X(m)
                species%atom(i)%pos%Y(m) = dr(2) + species%atom(1)%pos%Y(m)
                species%atom(i)%pos%Z(m) = dr(3) + species%atom(1)%pos%Z(m)
            end do
        end do
    end subroutine

    pure subroutine rescale(species, scale)
        !The first atom is shifted by a direct rescaling, the remaining atoms are translated such as
        !to preserve the shape of the molecule      
        real(kind=mc_prec), dimension(3, 3), intent(in) :: scale
        class(molecule_species), intent(inout) :: species
        real(kind=mc_prec) :: dx, dy, dz
        integer :: index
        integer :: j

        if(allocated(species%fractional_molecule))then
            call species%fractional_molecule%rescale(scale)
        end if

        do index = 1, species%nmol
            dx = species%atom(1)%pos%X(index)*(scale(1, 1) - 1._mc_prec)
            dy = species%atom(1)%pos%Y(index)*(scale(2, 2) - 1._mc_prec)
            dz = species%atom(1)%pos%Z(index)*(scale(3, 3) - 1._mc_prec)
            do j = 1, species%nats
                species%atom(j)%pos%X(index) = species%atom(j)%pos%X(index) + dx
                species%atom(j)%pos%Y(index) = species%atom(j)%pos%Y(index) + dy
                species%atom(j)%pos%Z(index) = species%atom(j)%pos%Z(index) + dz
            end do
        end do
    end subroutine

    subroutine set_nmol(species, n_mol)
        class(molecule_species) :: species
        integer, intent(in) :: n_mol

        integer :: i, old_s

        species%nmol = n_mol

        old_s = species%storage_space
        !Mess around a bit with this at some point, it might be speed limiting
        species%storage_space = first_multiple_greater(n_mol + 1 + safety_pad, storage_increment) 

        if (allocated(species%atom)) then
            do i = 1, species%nats 
                call resize_0_indexed_1D(species%atom(i)%pos%X, old_s, species%storage_space)
                call resize_0_indexed_1D(species%atom(i)%pos%Y, old_s, species%storage_space)
                call resize_0_indexed_1D(species%atom(i)%pos%Z, old_s, species%storage_space)
            end do
        end if

    end subroutine

    pure logical function can_rotate(species)
        class(molecule_species), intent(in) :: species
        can_rotate = species%rot_type > 1
    end function

    pure logical function has_movable_molecule(species)
        class(molecule_species), intent(in) :: species
        has_movable_molecule = species%nmol > 0 .or. species%fluctuating
    end function

    pure subroutine set_molecule(species, molecule, index)
        class(molecule_species), intent(inout) :: species
        integer, intent(in) :: index
        type(single_molecule), intent(in) :: molecule
        integer :: i

        if(index == 0) then
            call species%set_fractional_molecule(molecule)
            return
        end if

        do i = 1, species%nats
            species%atom(i)%pos%X(index) = molecule%X(i)
            species%atom(i)%pos%Y(index) = molecule%Y(i)
            species%atom(i)%pos%Z(index) = molecule%Z(i)
        end do

    end subroutine

    pure subroutine set_fractional_molecule(species, molecule)
        class(molecule_species), intent(inout) :: species
        type(single_molecule), intent(in) :: molecule

        species%fractional_molecule = molecule
    end subroutine

    pure function get_fractional_molecule(species) result(molecule)
        class(molecule_species), intent(in) :: species
        type(single_molecule) :: molecule

        molecule = species%fractional_molecule
    end function

    pure function get_molecule(species, index) result(molecule)
        class(molecule_species), intent(in) :: species
        integer, intent(in) :: index
        type(single_molecule) :: molecule
        integer :: i

        if(index == 0) then
            molecule = species%get_fractional_molecule()
            return
        end if

        molecule = single_molecule(species%nats)

        molecule%nvdw = species%nvdw
        molecule%ncharged = species%ncharged
        molecule%rot_type = species%rot_type
        do i = 1, species%nats
          molecule%name(i) = species%atom(i)%name
          molecule%weight(i) = species%atom(i)%weight
          molecule%isvdw(i) = species%atom(i)%isvdw
          molecule%lj_epsilon(i) = species%atom(i)%lj_epsilon
          molecule%lj_sigma(i) = species%atom(i)%lj_sigma
          molecule%ischarged(i) = species%atom(i)%ischarged
          molecule%charge(i) = species%atom(i)%charge
        end do

        do i = 1, species%nats
            molecule%X(i) = species%atom(i)%pos%X(index)
            molecule%Y(i) = species%atom(i)%pos%Y(index)
            molecule%Z(i) = species%atom(i)%pos%Z(index)
        end do

    end function

    elemental function get_prototype(species) result(molecule)
        class(molecule_species), intent(in) :: species
        type(single_molecule) :: molecule

        molecule = species%prototype

    end function

end module

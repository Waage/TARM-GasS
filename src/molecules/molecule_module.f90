module molecule_module
    use precision_module, only: mc_prec
    use geometry_module, only: box_type
    implicit none
    private

    public :: single_molecule

    type single_molecule
        !A single molecule, useful for locality when e.g. calculating energy of displacing one molecule, insertion, etc
        integer :: nats, nvdw, ncharged
        real(kind=mc_prec), dimension(:), allocatable :: X, Y, Z
        real(kind=mc_prec), dimension(:), allocatable :: lj_sigma, lj_epsilon, charge, weight
        character(8) :: molname
        character(8), dimension(:), allocatable :: name
        logical, dimension(:), allocatable :: ischarged, isvdw
        integer :: rot_type = 3 !Spatial as default
    contains

        procedure :: translate
        procedure :: set_natoms
        procedure :: reinsert
        procedure :: assemble
        procedure :: rescale
        procedure :: can_rotate
        procedure :: set_molname
    end type

    interface single_molecule
        module procedure new_single_molecule
    end interface

contains

    pure type(single_molecule) function new_single_molecule(n)
        integer, intent(in) :: n

        call new_single_molecule%set_natoms(n)
    end function

    pure subroutine set_natoms(mol, n_atoms)
        class(single_molecule), intent(inout) :: mol
        integer, intent(in) :: n_atoms

        mol%nats = n_atoms
        if(allocated(mol%X))then
            deallocate(mol%X)
        end if
        allocate(mol%X(n_atoms))
        if(allocated(mol%Y))then
            deallocate(mol%Y)
        end if
        allocate(mol%Y(n_atoms))
        if(allocated(mol%Z))then
            deallocate(mol%Z)
        end if
        allocate(mol%Z(n_atoms))

        if(allocated(mol%lj_sigma))then
            deallocate(mol%lj_sigma)
        end if
        allocate(mol%lj_sigma(n_atoms))
        if(allocated(mol%lj_epsilon))then
            deallocate(mol%lj_epsilon)
        end if
        allocate(mol%lj_epsilon(n_atoms))
        if(allocated(mol%charge))then
            deallocate(mol%charge)
        end if
        allocate(mol%charge(n_atoms))

        if(allocated(mol%name))then
            deallocate(mol%name)
        end if
        allocate(mol%name(n_atoms))

        if(allocated(mol%ischarged))then
            deallocate(mol%ischarged)
        end if
        allocate(mol%ischarged(n_atoms))

        if(allocated(mol%isvdw))then
            deallocate(mol%isvdw)
        end if
        allocate(mol%isvdw(n_atoms))

        if(allocated(mol%weight))then
            deallocate(mol%weight)
        end if
        allocate(mol%weight(n_atoms))

    end subroutine

    subroutine reinsert(mol, box)
        class(single_molecule) :: mol
        class(box_type), intent(in) :: box

        integer :: i

        do i = 1, mol%nats
            call box%apply_boundary_conditions(mol%X(i), mol%Y(i), mol%Z(i))
        end do   
    
    end subroutine

    subroutine assemble(mol, box) 
        !Collect a single molecule, so that it can be rotated etc
        !I.E:
        ! | - - - - - - - - - - - - - - - -|         | - - - - - - - - - - - - - - - -| 
        ! |                |         |                |
        ! | o              |         | o              |
        ! |               x|   =>   x|                |
        ! |                |         |                |
        ! | - - - - - - - - - - - - - - - -|         | - - - - - - - - - - - - - - - -|
        !Needs to be changed to correctly handle molecules
        !that are larger than half the box length
        class(single_molecule) :: mol
        class(box_type), intent(in) :: box
        integer :: i
        real(kind=mc_prec), dimension(3) :: dr

        do i = 2, mol%nats
            dr = box%nearest_vector(mol%X(i), mol%X(1), mol%Y(i), mol%Y(1), mol%Z(i), mol%Z(1))

            mol%X(i) = dr(1) + mol%X(1)
            mol%Y(i) = dr(2) + mol%Y(1)
            mol%Z(i) = dr(3) + mol%Z(1)
        end do

    end subroutine

    pure subroutine rescale(mol, scale)
        !The first atom is shifted by a direct rescaling, the remaining atoms are translated such as
        !to preserve the shape of the molecule      
        real(kind=mc_prec), dimension(3, 3), intent(in) :: scale
        class(single_molecule), intent(inout) :: mol
        real(kind=mc_prec) :: dx, dy, dz
        integer :: j

        dx = mol%X(1)*(scale(1, 1) - 1._mc_prec)
        dy = mol%Y(1)*(scale(2, 2) - 1._mc_prec)
        dz = mol%Z(1)*(scale(3, 3) - 1._mc_prec)
        do j = 1, mol%nats
            mol%X(j) = mol%X(j) + dx
            mol%Y(j) = mol%Y(j) + dy
            mol%Z(j) = mol%Z(j) + dz
        end do

    end subroutine

    elemental logical function can_rotate(mol)
        class(single_molecule), intent(in) :: mol
        can_rotate = mol%rot_type > 1
    end function

    subroutine set_molname(mol, name)
    
        class(single_molecule) :: mol
        character(*) :: name

        mol%molname = name
    
    end subroutine

    pure subroutine translate(mol, dr)
        class(single_molecule), intent(inout) :: mol
        real(kind=mc_prec), dimension(3), intent(in) :: dr
        integer :: j

        do j = 1, mol%nats
           mol%X(j) = dr(1) + mol%X(j)
           mol%Y(j) = dr(2) + mol%Y(j)
           mol%Z(j) = dr(3) + mol%Z(j)
        end do 
    
    end subroutine
    
end module molecule_module

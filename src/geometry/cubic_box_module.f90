module cubic_box_module
    use geometry_module, only: box_type
    use precision_module, only: mc_prec
    implicit none
    private
    public :: cubic_box_type

    type, extends(box_type) :: cubic_box_type
        !An orthorhombic box
        real(kind=mc_prec), dimension(3) :: boundary
    
    contains
        procedure :: get_boundary
        procedure :: get_reciprocal
        procedure :: get_volume
        procedure :: update_boundary
        procedure :: apply_boundary_conditions
        procedure :: distsq
        procedure :: distsq_array
        procedure :: is_valid
    end type

    interface cubic_box_type
        module procedure :: new_cubic_box
    end interface cubic_box_type

contains

    pure function new_cubic_box(inp_boundary) result(box)
        !Constructor for the orthorhombic box class
        type(cubic_box_type) :: box
        real(kind=mc_prec), dimension(3, 3), intent(in) :: inp_boundary

        call box%update_boundary(inp_boundary)

        return
    end function

    elemental subroutine apply_boundary_conditions(box, x, y, z)
        !Alters x, y, z so the point they represent are "in" the box
        class(cubic_box_type), intent(in) :: box
        real(kind=mc_prec), intent(inout) :: x, y, z

        x = boundary_shift(x, box%boundary(1))
        y = boundary_shift(y, box%boundary(2))
        z = boundary_shift(z, box%boundary(3))

    end subroutine

    pure function get_boundary(box) result(boundary)
        !Kronglete
        class(cubic_box_type), intent(in) :: box
        real(kind=mc_prec), dimension(3, 3) :: boundary
        boundary = 0._mc_prec
        boundary(1, 1) = box%boundary(1)
        boundary(2, 2) = box%boundary(2)
        boundary(3, 3) = box%boundary(3)
        return
    end function

    pure function get_reciprocal(box) result(rec_vec)
        !Kronglete
        class(cubic_box_type), intent(in) :: box
        real(kind=mc_prec), dimension(3, 3) :: rec_vec
        rec_vec = 0._mc_prec
        rec_vec(1, 1) = 1._mc_prec/box%boundary(1)
        rec_vec(2, 2) = 1._mc_prec/box%boundary(2)
        rec_vec(3, 3) = 1._mc_prec/box%boundary(3)
        return
    end function

    pure function get_volume(box) result(V)
        !Kronglete
        class(cubic_box_type), intent(in) :: box
        real(kind=mc_prec) :: V
        V = box%boundary(1)*box%boundary(2)*box%boundary(3)
        return
    end function

    pure subroutine update_boundary(box, inp_boundary)
    
        class(cubic_box_type), intent(inout) :: box
        real(kind=mc_prec), dimension(3, 3), intent(in) :: inp_boundary

        box%boundary(1) = inp_boundary(1, 1)
        box%boundary(2) = inp_boundary(2, 2)
        box%boundary(3) = inp_boundary(3, 3)

    end subroutine

    elemental real(kind=mc_prec) function boundary_shift(x, side)
        real(kind=mc_prec), intent(in) :: x, side

        boundary_shift = merge(x - side, merge(x + side, x, 2*x < - side), 2*x > side)

    end function

    elemental real(kind=mc_prec) function distsq(box, ax, ay, az, bx, by, bz)
        class(cubic_box_type), intent(in) :: box
        real(kind=mc_prec), intent(in) :: ax, ay, az, bx, by, bz
        real(kind=mc_prec) :: dx, dy, dz

        dx = ax - bx
        dy = ay - by
        dz = az - bz

        call box%apply_boundary_conditions(dx, dy, dz)

        distsq = dx*dx + dy*dy + dz*dz

    end function

    pure function distsq_array(box, ax, ay, az, bx, by, bz) result(outval)
        !Sometimes, Gfortran's autovectorizer is frustratingly dense so this function has been required
        class(cubic_box_type), intent(in) :: box
        real(kind=mc_prec), intent(in) :: ax, ay, az
        real(kind=mc_prec), dimension(:), intent(in) :: bx
        real(kind=mc_prec), dimension(size(bx)), intent(in) :: by, bz
        real(kind=mc_prec), dimension(size(bx)) :: dx, dy, dz
        real(kind=mc_prec), dimension(size(bx)) :: outval
        integer :: i

        do i = 1, size(bx)
            dx(i) = ax - bx(i)
            dx(i) = boundary_shift(dx(i), box%boundary(1))
            dy(i) = ay - by(i)
            dy(i) = boundary_shift(dy(i), box%boundary(2))
            dz(i) = az - bz(i)
            dz(i) = boundary_shift(dz(i), box%boundary(3))
        end do

        outval = dx*dx + dy*dy + dz*dz

        return
    end function

    pure logical function is_valid(box, radial_cutoff)
        class(cubic_box_type), intent(in) :: box
        real(kind=mc_prec), intent(in) :: radial_cutoff

        is_valid = box%boundary(1) > 2._mc_prec*radial_cutoff .and. &
                   box%boundary(2) > 2._mc_prec*radial_cutoff .and. &
                   box%boundary(3) > 2._mc_prec*radial_cutoff

        return
    end function

end module

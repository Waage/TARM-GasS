module geometry_module
    !Module containing types that describe the geometry of the simulation box
    use precision_module, only: mc_prec
    implicit none
    private

    public :: box_type

    type, abstract :: box_type
    
    contains
    
        procedure(get_boundary), deferred :: get_boundary
        procedure(get_reciprocal), deferred :: get_reciprocal
        procedure(get_volume), deferred :: get_volume 
        procedure(update_boundary_conditions), deferred :: update_boundary 
        procedure(apply_boundary_conditions), deferred :: apply_boundary_conditions 
        procedure(is_valid), deferred :: is_valid
        procedure :: distsq
        procedure :: distsq_array

        ! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        procedure, non_overridable :: nearest_vector

    end type

    abstract interface  
        elemental subroutine apply_boundary_conditions(box, x, y, z)
            use precision_module
            import box_type
            class(box_type), intent(in) :: box
            real(kind=mc_prec), intent(inout) :: x, y, z
        end subroutine
    end interface
    
    abstract interface  
        pure subroutine update_boundary_conditions(box, inp_boundary)
            use precision_module
            import box_type
            class(box_type), intent(inout) :: box
            real(kind=mc_prec), dimension(3, 3), intent(in) :: inp_boundary
        end subroutine
    end interface

    abstract interface  
        pure function get_boundary(box) result(boundary)
            use precision_module
            import box_type
            class(box_type), intent(in) :: box
            real(kind=mc_prec), dimension(3, 3) :: boundary
        end function
    end interface

    abstract interface  
        pure function get_reciprocal(box) result(inv_boundary)
            use precision_module
            import box_type
            class(box_type), intent(in) :: box
            real(kind=mc_prec), dimension(3, 3) :: inv_boundary
        end function
    end interface

    abstract interface  
        pure function get_volume(box) result(V)
            use precision_module
            import box_type
            class(box_type), intent(in) :: box
            real(kind=mc_prec) :: V
        end function
    end interface

    abstract interface  
        pure logical function is_valid(box, radial_cutoff)
            use precision_module
            import box_type
            class(box_type), intent(in) :: box
            real(kind=mc_prec), intent(in) :: radial_cutoff
        end function
    end interface

contains

    elemental real(kind=mc_prec) function distsq(box, ax, ay, az, bx, by, bz) result(outval)
        class(box_type), intent(in) :: box
        real(kind=mc_prec), intent(in) :: ax, ay, az, bx, by, bz
        real(kind=mc_prec) :: dx, dy, dz

        dx = ax - bx
        dy = ay - by
        dz = az - bz

        call box%apply_boundary_conditions(dx, dy, dz)

        outval = dx*dx + dy*dy + dz*dz

        return
    end function

    pure function distsq_array(box, ax, ay, az, bx, by, bz) result(outval)
        class(box_type), intent(in) :: box
        real(kind=mc_prec), intent(in) :: ax, ay, az
        real(kind=mc_prec), dimension(:), intent(in) :: bx
        real(kind=mc_prec), dimension(size(bx)), intent(in) :: by, bz
        real(kind=mc_prec), dimension(size(bx)) :: dx, dy, dz
        real(kind=mc_prec), dimension(size(bx)) :: outval

        dx = ax - bx
        dy = ay - by
        dz = az - bz

        call box%apply_boundary_conditions(dx, dy, dz)

        outval = dx*dx + dy*dy + dz*dz

        return
    end function

    pure function nearest_vector(box, x_to, x_from, y_to, y_from, z_to, z_from) result(dr)
        !Returns the nearest vector
        class(box_type), intent(in) :: box
        real(kind=mc_prec), intent(in) :: x_to, x_from, y_to, y_from, z_to, z_from
        real(kind=mc_prec), dimension(3) :: dr

        dr(1) = x_to - x_from
        dr(2) = y_to - y_from
        dr(3) = z_to - z_from       

        call box%apply_boundary_conditions(dr(1), dr(2), dr(3))

        return
    end function

end module

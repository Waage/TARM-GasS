module geometry_interface_module
    use precision_module, only: mc_prec
    use geometry_module, only: box_type
    use cubic_box_module, only: cubic_box_type
    implicit none
    private
    public :: create_box

contains

    subroutine create_box(box, inp_boundary)
        !Modify this to incorporate more types of boxes, for now, all boxes are orthorhombic

        class(box_type), allocatable :: box
        real(kind=mc_prec), dimension(3, 3), intent(in) :: inp_boundary

        allocate(box, source = cubic_box_type(inp_boundary))

        !Future work: 
        !allocate(box, source = triclinic_box(inp_boundary))
    
    end subroutine


end module

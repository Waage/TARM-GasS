module triclinic_box_module !!!!!!UNFINISHED
    use geometry_module, only: box_type
    use precision_module, only: mc_prec
    use general_utilities_module, only: cross_product, pass_or_abort, is_zero
    implicit none
    private
    public :: triclinic_box_type

    type, extends(box_type) :: triclinic_box_type
        ! General shape
        real(kind=mc_prec), dimension(3, 3) :: boundary
    
    contains
        procedure :: get_boundary
        procedure :: get_reciprocal
        procedure :: get_volume
        procedure :: update_boundary
        procedure :: apply_boundary_conditions
        procedure :: distsq
        procedure :: distsq_array
        procedure :: is_valid
    end type

    interface triclinic_box_type
        module procedure :: new_triclinic_box
    end interface triclinic_box_type

contains

    function new_triclinic_box(inp_boundary) result(box)
        !Constructor for the orthorhombic box class
        type(triclinic_box_type) :: box
        real(kind=mc_prec), dimension(3, 3), intent(in) :: inp_boundary

        call pass_or_abort(inp_boundary(1, 1) > 0._mc_prec, "a_x must be greater than 0")
        call pass_or_abort(is_zero(inp_boundary(1, 2)), "a_y must be 0")
        call pass_or_abort(is_zero(inp_boundary(1, 3)), "a_z must be 0")
        call pass_or_abort(inp_boundary(2, 1) < 0.5_mc_prec*inp_boundary(1, 1), "a_x must be greater than 2*b_x")
        call pass_or_abort(inp_boundary(2, 2) > 0._mc_prec, "b_y must be greater than 0")
        call pass_or_abort(is_zero(inp_boundary(2, 3)), "b_z must be 0")
        call pass_or_abort(inp_boundary(3, 1) < 0.5_mc_prec*inp_boundary(1, 1), "a_x must be greater than 2*c_x")
        call pass_or_abort(inp_boundary(3, 2) < 0.5_mc_prec*inp_boundary(2, 2), "b_y must be greater than 2*c_y")
        call pass_or_abort(inp_boundary(3, 3) > 0._mc_prec, "c_z must be greater than 0")

        call box%update_boundary(inp_boundary)

        return
    end function

    elemental subroutine apply_boundary_conditions(box, x, y, z)
        !Alters x, y, z so the vector they represent are "in" the box
        !Finds the image that is the closest to the origin (0, 0, 0) and sets that as the vector
        class(triclinic_box_type), intent(in) :: box
        real(kind=mc_prec), intent(inout) :: x, y, z
        real(kind=mc_prec) :: tx, ty, tz, imx, imy, imz
        real(kind=mc_prec) :: dsq, min_dsq
        integer :: ix, iy, iz

        min_dsq = 100*maxval(box%boundary*box%boundary)

        do ix = - 1, 1
            do iy = - 1, 1
                do iz = - 1, 1
                    imx = x + box%boundary(1, 1)*ix + box%boundary(1, 2)*iy + box%boundary(1, 3)*iz
                    imy = y + box%boundary(2, 1)*ix + box%boundary(2, 2)*iy + box%boundary(2, 3)*iz
                    imz = z + box%boundary(3, 1)*ix + box%boundary(3, 2)*iy + box%boundary(3, 3)*iz
                    dsq = imx*imx + imy*imy + imz*imz
                    if(dsq < min_dsq) then
                        min_dsq = dsq
                        tx = imx
                        ty = imy
                        tz = imz       
                    end if
                end do
            end do
        end do
        x = tx
        y = ty
        z = tz

    end subroutine

    pure function get_boundary(box) result(boundary)
        !Kronglete
        class(triclinic_box_type), intent(in) :: box
        real(kind=mc_prec), dimension(3, 3) :: boundary
        boundary = box%boundary
        return
    end function

    pure function get_reciprocal(box) result(rec_vec)
        !Kronglete
        class(triclinic_box_type), intent(in) :: box
        real(kind=mc_prec), dimension(3, 3) :: rec_vec
        real(kind=mc_prec) :: volume
        volume = box%get_volume()
        rec_vec(1:3, 1) = cross_product(box%boundary(1:3, 2), box%boundary(1:3, 3))/volume
        rec_vec(1:3, 2) = cross_product(box%boundary(1:3, 3), box%boundary(1:3, 1))/volume
        rec_vec(1:3, 3) = cross_product(box%boundary(1:3, 1), box%boundary(1:3, 2))/volume
        return
    end function

    pure function get_volume(box) result(V)
        class(triclinic_box_type), intent(in) :: box
        real(kind=mc_prec) :: V
        V = dot_product(box%boundary(1:3, 1), cross_product(box%boundary(1:3, 2), box%boundary(1:3, 3)))
        return
    end function

    pure subroutine update_boundary(box, inp_boundary)
        class(triclinic_box_type), intent(inout) :: box
        real(kind=mc_prec), dimension(3, 3), intent(in) :: inp_boundary

        box%boundary = inp_boundary  

    end subroutine

    elemental real(kind=mc_prec) function boundary_shift(x, delta, comp_1, comp_2)
        real(kind=mc_prec), intent(in) :: x, delta, comp_1, comp_2
        boundary_shift = merge(x - delta, merge(x + delta, x, 2*comp_1 < - comp_2), 2*comp_1 > comp_2 )
    end function

    elemental real(kind=mc_prec) function distsq(box, ax, ay, az, bx, by, bz)
        class(triclinic_box_type), intent(in) :: box
        real(kind=mc_prec), intent(in) :: ax, ay, az, bx, by, bz
        real(kind=mc_prec) :: dx, dy, dz

        dx = ax - bx
        dy = ay - by
        dz = az - bz
        ! With our constraints on the radial cutoff, the below conditions give the correct distance, provided that
        ! the distance is less than the radial cutoff distance. If it is not, the distance will be wrong, but still
        ! greater than cutoff, so that is fine.
        dx = boundary_shift(dx, box%boundary(3, 1), dz, box%boundary(3, 3))
        dy = boundary_shift(dy, box%boundary(3, 2), dz, box%boundary(3, 3))
        dz = boundary_shift(dz, box%boundary(3, 3), dz, box%boundary(3, 3))

        dx = boundary_shift(dx, box%boundary(2, 1), dy, box%boundary(2, 2))
        dy = boundary_shift(dy, box%boundary(2, 2), dy, box%boundary(2, 2))

        dx = boundary_shift(dx, box%boundary(1, 1), dx, box%boundary(1, 1))

        distsq = dx*dx + dy*dy + dz*dz

    end function

    pure function distsq_array(box, ax, ay, az, bx, by, bz) result(outval)
        class(triclinic_box_type), intent(in) :: box
        real(kind=mc_prec), intent(in) :: ax, ay, az
        real(kind=mc_prec), dimension(:), intent(in) :: bx
        real(kind=mc_prec), dimension(size(bx)), intent(in) :: by, bz
        real(kind=mc_prec) :: dx, dy, dz
        real(kind=mc_prec), dimension(size(bx)) :: outval
        integer :: i

        do i = 1, size(bx)
            dx = ax - bx(i)
            dy = ay - by(i)
            dz = az - bz(i)
            dx = boundary_shift(dx, box%boundary(3, 1), dz, box%boundary(3, 3))
            dy = boundary_shift(dy, box%boundary(3, 2), dz, box%boundary(3, 3))
            dz = boundary_shift(dz, box%boundary(3, 3), dz, box%boundary(3, 3))

            dx = boundary_shift(dx, box%boundary(2, 1), dy, box%boundary(2, 2))
            dy = boundary_shift(dy, box%boundary(2, 2), dy, box%boundary(2, 2))

            dx = boundary_shift(dx, box%boundary(1, 1), dx, box%boundary(1, 1))
            outval(i) = dx*dx + dy*dy + dz*dz
        end do
        return
    end function

    pure logical function is_valid(box, radial_cutoff)
        class(triclinic_box_type), intent(in) :: box
        real(kind=mc_prec), intent(in) :: radial_cutoff
            is_valid = box%boundary(1, 1) > 2*radial_cutoff &
                 .and. box%boundary(2, 2) > 2*radial_cutoff &
                 .and. box%boundary(3, 3) > 2*radial_cutoff
        return
    end function

end module

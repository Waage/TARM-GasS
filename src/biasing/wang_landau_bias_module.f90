module wang_landau_bias_module
    use precision_module, only: mc_prec
    implicit none
    private
    public ::  wang_landau_bias

    real(kind=mc_prec), parameter :: flatness = 0.5_mc_prec, BIAS_FINEST_SCALE = 0.000001
    integer, parameter :: min_visits_before_rescale = 10000

    type wang_landau_bias
        private
        integer, dimension(:), allocatable :: frac_bin_visit
        real(kind=mc_prec), dimension(:), allocatable :: frac_bias
        real(kind=mc_prec) :: scale_magnitude
        integer :: bin_moves
        logical :: equilibrating, is_operational_bias
    contains
        procedure :: increment_bin_visited
        procedure :: update_scale
        procedure :: coupling_bias
        procedure :: has_reached_finest_scale
        procedure :: current_bias
        procedure :: get_maximal_visit
        procedure :: normalize
        procedure :: test_flatness
        procedure :: toggle_equilibrating

    end type

    interface wang_landau_bias
        module procedure new_wang_landau_bias
    end interface
    
contains

    pure type(wang_landau_bias) function new_wang_landau_bias(nBins, is_operational_bias)
        integer, intent(in) :: nBins
        logical, intent(in) :: is_operational_bias

        allocate(new_wang_landau_bias%frac_bin_visit(nBins), &
                 new_wang_landau_bias%frac_bias(nBins))
        new_wang_landau_bias%frac_bin_visit = 0
        new_wang_landau_bias%frac_bias = 0._mc_prec

        new_wang_landau_bias%scale_magnitude = .1_mc_prec
        new_wang_landau_bias%bin_moves = 0
        new_wang_landau_bias%equilibrating = .true.
        new_wang_landau_bias%is_operational_bias = is_operational_bias
        
    end function

    elemental subroutine toggle_equilibrating(bias, onoff)
        class(wang_landau_bias), intent(inout) :: bias
        logical, intent(in) :: onoff

        bias%equilibrating = onoff
    end subroutine

    elemental logical function has_reached_finest_scale(bias)
        class(wang_landau_bias), intent(in) :: bias

        ! Always return true when we are not actually applying any bias
        ! Because if so, we are not going to get to any finer scale than this.
        has_reached_finest_scale = .not. (bias%is_operational_bias .and. bias%scale_magnitude > BIAS_FINEST_SCALE)
        return
    end function
    
    pure subroutine increment_bin_visited(bias, lambda)
        class(wang_landau_bias), intent(inout) :: bias
        real(kind=mc_prec), intent(in) :: lambda
        integer :: bin_nr
        
        bin_nr = int(lambda*size(bias%frac_bias)) + 1
        !Update counters
        bias%frac_bin_visit(bin_nr) = bias%frac_bin_visit(bin_nr) + 1
        bias%bin_moves = bias%bin_moves + 1
        !if equilibrating, alter bias%frac_bias
        if(bias%equilibrating) then
            bias%frac_bias(bin_nr) = bias%frac_bias(bin_nr) - bias%scale_magnitude
        end if
    end subroutine
    
    pure integer function get_maximal_visit(bias) result(max)
        !Return the maximally visited bin's visitation number
        class(wang_landau_bias), intent(in) :: bias
        max = maxval(bias%frac_bin_visit)
    end function

    logical function test_flatness(bias, max) result(rescale)
        class(wang_landau_bias) :: bias
        integer, intent(in) :: max
        integer :: j

        if(bias%bin_moves < min_visits_before_rescale)then
            rescale =.false.
        end if

        !Check for flatness, reset counter
        bias%bin_moves = 0
        rescale = .true.
        do j = 1, size(bias%frac_bias)
            if(bias%frac_bin_visit(j) < max*.4_mc_prec)then
                rescale =.false.
            end if
        end do

        return
    end function

    pure subroutine normalize(bias, normval)
        !Normalize the bins' bias by subtracting normval
        class(wang_landau_bias), intent(inout) :: bias
        real(kind=mc_prec), intent(in) :: normval

        bias%frac_bias(:) = bias%frac_bias(:) - normval
    end subroutine
    
    pure subroutine update_scale(bias)
        class(wang_landau_bias), intent(inout) :: bias
        !Update the variable used to scale the bias on visiting a bin
        bias%scale_magnitude = bias%scale_magnitude*.5_mc_prec
        !Reset the histogram
        bias%frac_bin_visit(:) = 0

    end subroutine

    pure real(kind=mc_prec) function coupling_bias(bias, lambda_new, lambda_old)
        !Obtain the bias associated with moving to a specific coupling value
        !This value is to be used as the argument of an exponent, c.f. Shi and Maginn: exp(eta_i - eta_j)
        class(wang_landau_bias), intent(in) :: bias
        real(kind=mc_prec), intent(in) :: lambda_new, lambda_old !Implicit expectation: both lambdas are between 0 and 1
        integer :: new_bin, old_bin

        new_bin = int(lambda_new*size(bias%frac_bias)) + 1
        old_bin = int(lambda_old*size(bias%frac_bias)) + 1
        coupling_bias = (bias%frac_bias(new_bin) - bias%frac_bias(old_bin))
        return
    end function

    real(kind=mc_prec) function current_bias(bias, lambda)
        !Obtain the current bias factor of a single bin
        class(wang_landau_bias), intent(in) :: bias
        real(kind=mc_prec), intent(in) :: lambda
        current_bias = bias%frac_bias(int(lambda*size(bias%frac_bias)) + 1)
        return
    end function

end module wang_landau_bias_module

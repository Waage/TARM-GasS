module argument_parser_module
implicit none
private
public :: parse_command_line

contains
    subroutine parse_command_line(input_file_name, input_directory, output_directory)
        character(256), intent(inout) :: input_directory, input_file_name, output_directory
        character(512) :: argument_value
        integer :: nargs, i, arglen, input_directory_index
        character(1), parameter :: folder_separator = '/'

        input_file_name = 'trial_topol.promp'
        input_directory = ''
        output_directory = ''
        nargs = command_argument_count()
        i = 0
        do while(i < nargs)
            i = i + 1
            call get_command_argument(i, argument_value, arglen)
            if (arglen > 512) error stop "Unexpectedly long command line argument: "//trim(argument_value)
            if (argument_value == "--input_file") then
                i = i + 1
                call get_command_argument(i, input_file_name, arglen)
                if (arglen > 256) error stop "Unexpectedly long input directory: "//trim(input_file_name)
                input_file_name = trim(adjustl(input_file_name))
                input_directory_index = scan(input_file_name, folder_separator, back=.true.)
                if(input_directory_index > 0) then
                    input_directory = input_file_name(1:input_directory_index)
                end if
            else if (argument_value == "--output_directory") then
                i = i + 1
                call get_command_argument(i, output_directory, arglen)
                if (arglen > 256) error stop "Unexpectedly long input directory: "//trim(output_directory)
                output_directory = trim(adjustl(output_directory))//folder_separator
            else
                write(*, *) "Unrecognized command line argument: "//trim(argument_value)
                write(*, *) "Syntax: TARM_GasS [--input_file <input_file> ] [--output_directory <output_directory>]"
                error stop "Terminating with error"
            end if
        end do

    end subroutine
end module

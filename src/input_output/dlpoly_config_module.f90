module dlpoly_config_module
    use geometry_interface_module, only: create_box
    use precision_module, only: mc_prec
    use system_module, only: system
    implicit none
    private

    public :: read_dlpoly_configuration_from_file, write_dlpoly_configuration_to_file

contains

    subroutine read_dlpoly_configuration_from_file(conf_file, sys)
    
        type(system) :: sys
        integer, intent(in) :: conf_file
        real(kind=mc_prec), dimension(3, 3) :: inp_boundary
        character(8) :: tmp_name
        integer :: dum_int
        integer :: i, j, k

        read(conf_file, *) !Title 
        read(conf_file, *) !Information not currently used. Important: one of these integers specifies whether the box is 
                          !orthogonal, another informs us about the total number of atoms in the file, which is a handy failproof 

        do i = 1, 3
            read(conf_file, *) inp_boundary(1:3, i)
        end do
        call create_box(sys%box, inp_boundary)
        do k = 1, size(sys%species)
            do i = 1, sys%species(k)%nmol
                do j = 1, sys%species(k)%nats
                    read(conf_file, *) tmp_name, dum_int !Possible to do some sanity - checks here, confirm that the read name corresponds to expectations
                    read(conf_file, *) sys%species(k)%atom(j)%pos%X(i), &
                                      sys%species(k)%atom(j)%pos%Y(i), &
                                      sys%species(k)%atom(j)%pos%Z(i)
                end do
            end do
            if(sys%species(k)%fluctuating)then
                do j = 1, sys%species(k)%nats
                    read(conf_file, *)
                    read(conf_file, *) sys%species(k)%fractional_molecule%X(j), &
                                      sys%species(k)%fractional_molecule%Y(j), &
                                      sys%species(k)%fractional_molecule%Z(j)
                end do
            else
                do j = 1, sys%species(k)%nats
                    sys%species(k)%fractional_molecule%X(j) = 0._mc_prec
                    sys%species(k)%fractional_molecule%Y(j) = 0._mc_prec
                    sys%species(k)%fractional_molecule%Z(j) = 0._mc_prec
                end do
            end if
        end do

        !Finished reading the file
    end subroutine

    subroutine write_dlpoly_configuration_to_file(conf_file, sys)
        !Need to add some format keys so this is compliant with dlpoly format specifications
        type(system) :: sys
        integer, intent(in) :: conf_file
        real(kind=mc_prec), dimension(3, 3) :: boundary
        integer :: dum_int
        integer :: i, j, k
        integer :: totats

        totats = 0
        do i = 1, size(sys%species)
            totats = sys%species(i)%nats*sys%species(i)%nmol + totats
            if(sys%species(i)%fluctuating)then 
                totats = totats + sys%species(i)%nats
            end if
        end do

        write(conf_file, '(a80)') 'Placeholder title from TARM_GasS' 
        write(conf_file, '(3i10)') 0, 0, totats

        boundary = sys%box%get_boundary()
        do i = 1, 3
            write(conf_file, *) boundary(1:3, i)
        end do
        dum_int = 0
        do k = 1, size(sys%species)
            do i = 1, sys%species(k)%nmol
                do j = 1, sys%species(k)%nats
                    dum_int = dum_int + 1
                    write(conf_file, '(a8, i10)') sys%species(k)%atom(j)%name, dum_int
                    write(conf_file, '(3(f0.15, x))') sys%species(k)%atom(j)%pos%X(i), &
                                      sys%species(k)%atom(j)%pos%Y(i), &
                                      sys%species(k)%atom(j)%pos%Z(i)
                end do
            end do
            if(sys%species(k)%fluctuating)then
                do j = 1, sys%species(k)%nats
                    dum_int = dum_int + 1
                    write(conf_file, '(a8, i10)') "fr_"//sys%species(k)%atom(j)%name, dum_int
                    write(conf_file, '(3(f0.15, 1x))') sys%species(k)%fractional_molecule%X(j), &
                                      sys%species(k)%fractional_molecule%Y(j), &
                                      sys%species(k)%fractional_molecule%Z(j)
                end do
                
            end if

        end do

        !Finished writing the file
    end subroutine


end module

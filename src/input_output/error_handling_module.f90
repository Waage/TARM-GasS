module error_handling_module
    implicit none

    private
    public :: warn, warn_missing, warn_unknown, error, set_max_warn, get_max_warn, &
              get_n_warn, get_n_error

    integer :: max_warn = 1
    integer, save :: n_warn = 0
    integer, save :: n_error = 0
    integer, parameter :: max_string_length = 128 !Max length of an input line

contains

    subroutine warn(message)
    
        Character(*) :: message

        write(*, *) "Warning: "//trim(adjustl(message))
        n_warn = n_warn + 1  
    
    end subroutine


    subroutine warn_missing(object, default_value)
    
        Character(*) :: object, default_value

        write(*, *) "Warning, "//trim(adjustl(object))//"not found,  set to default of "//trim(adjustl(default_value))
        n_warn = n_warn + 1  

    end subroutine

    subroutine warn_unknown(name, object)
    
        Character(*) :: object, name

        write(*, *) "Warning, unknown"//trim(adjustl(name))//" "//trim(adjustl(object))//" ignored"
        n_warn = n_warn + 1  

    end subroutine
    
    subroutine error(object, mistake, fix)
    
        Character(*) :: object, mistake, fix
        write(*, *) "Error, "//trim(adjustl(object))//" "//trim(adjustl(mistake))//"\n Fix by "//trim(adjustl(fix))
        n_error = n_error + 1
        stop

    end subroutine

    subroutine set_max_warn(mw)
        integer, intent(in) :: mw
        max_warn = mw
    
    end subroutine
    
    function get_max_warn( ) result(mw)
        integer :: mw
        mw = max_warn    
        return
    end function

    function get_n_warn( ) result(nw)
        integer :: nw
        nw = n_warn    
        return
    end function

    function get_n_error( ) result(ne)
        integer :: ne
        ne = n_error    
        return
    end function
    

end module

module gro_file_module
    use geometry_interface_module, only: create_box
    use input_output_module, only: split
    use precision_module, only: mc_prec
    use system_module, only: system
    implicit none
    private
    public :: read_gro_file, write_gro_file 

contains

    subroutine read_gro_file(fil, sys)
        !Not tested yet but ought to work
        implicit none
        !Rudimentary printout system, extended .xyz format
        integer :: fil, totats, i, j, t
        class(system) :: sys
        integer :: currat, currmol
        real(kind=mc_prec), dimension(3, 3) :: boundary
        real(kind=mc_prec), dimension(3) :: dummy_real
        Character(5) :: molname, atname
        Character(100) :: dummy
        Character(100), dimension(9) :: terrible_format

        read(fil, *) !Title
        read(fil, *)totats
        currat = 0
        do t = 1, size(sys%species)
            do i = 1, sys%species(t)%nmol
            do j = 1, sys%species(t)%nats
                currat = currat + 1
                read(fil, '(i5, 2a5, i5, 3f8.3, 3f8.4)')&
                     currmol, molname, atname, currat, & !Potential sanity check goes here
                     sys%species(t)%atom(j)%pos%X(i), &
                     sys%species(t)%atom(j)%pos%Y(i), &
                     sys%species(t)%atom(j)%pos%Z(i), &
                     dummy_real
                sys%species(t)%atom(j)%pos%X(i) = sys%species(t)%atom(j)%pos%X(i)*10._mc_prec 
                sys%species(t)%atom(j)%pos%Y(i) = sys%species(t)%atom(j)%pos%Y(i)*10._mc_prec 
                sys%species(t)%atom(j)%pos%Z(i) = sys%species(t)%atom(j)%pos%Z(i)*10._mc_prec
            end do
            end do
            if(sys%species(t)%fluctuating)then
            currat = currat + 1
            do j = 1, sys%species(t)%nats
                read(fil, '(i5, 2a5, i5, 3f8.3, 3f8.4)')&
                     currmol, molname, atname, currat, & !Potential sanity check goes here
                     sys%species(t)%fractional_molecule%X(j), &
                     sys%species(t)%fractional_molecule%Y(j), &
                     sys%species(t)%fractional_molecule%Z(j), &
                     dummy_real
                sys%species(t)%atom(j)%pos%X(i) = sys%species(t)%atom(j)%pos%X(i)*10._mc_prec 
                sys%species(t)%atom(j)%pos%Y(i) = sys%species(t)%atom(j)%pos%Y(i)*10._mc_prec 
                sys%species(t)%atom(j)%pos%Z(i) = sys%species(t)%atom(j)%pos%Z(i)*10._mc_prec
            
            end do
            else
                do j = 1, sys%species(t)%nats
                    sys%species(t)%fractional_molecule%X(j) = 0
                    sys%species(t)%fractional_molecule%Y(j) = 0
                    sys%species(t)%fractional_molecule%Z(j) = 0
                end do
                
            end if
        end do
        !Literally the worst choice for boundary conditions
        !As though being allowed to omit triclinic coordinates is a superuseful good thing
        boundary = 0._mc_prec
        read(fil, '(A)') dummy
        terrible_format = split(dummy, ' ', 9)
        read(terrible_format(1), *) boundary(1, 1)
        read(terrible_format(2), *) boundary(2, 2)
        read(terrible_format(3), *) boundary(3, 3)
        if(.not.terrible_format(4) == '') read(terrible_format(4), *) boundary(2, 1)
        if(.not.terrible_format(5) == '') read(terrible_format(5), *) boundary(3, 1)
        if(.not.terrible_format(6) == '') read(terrible_format(6), *) boundary(1, 2)
        if(.not.terrible_format(7) == '') read(terrible_format(7), *) boundary(3, 2)
        if(.not.terrible_format(8) == '') read(terrible_format(8), *) boundary(1, 3)
        if(.not.terrible_format(9) == '') read(terrible_format(9), *) boundary(2, 3)
        boundary = boundary*10._mc_prec
        call create_box(sys%box, boundary)
    end subroutine

    subroutine write_gro_file(fil, sys)
        !Not tested yet but ought to work
        implicit none
        !Rudimentary printout system, extended .xyz format
        integer :: fil, totats, i, j, t
        class(system) :: sys
        integer :: currat, currmol
        real(kind=mc_prec), dimension(3, 3) :: boundary
        Character(5) :: molname, atname

        totats = 0
        do i = 1, size(sys%species)
           totats = sys%species(i)%nats*sys%species(i)%nmol + totats
           if(sys%species(i)%fluctuating)then 
              totats = totats + sys%species(i)%nats
           end if
        end do

        write(fil, *) 'Mordi e mannæ' !Title
        write(fil, *)totats
        currat = 0
        currmol = 0
        do t = 1, size(sys%species)
            do i = 1, sys%species(t)%nmol
                currmol = currmol + 1
                molname = sys%species(t)%molname(1:5)
                do j = 1, sys%species(t)%nats
                    currat = currat + 1
                    atname = sys%species(t)%atom(j)%name(1:5)
                    write(fil, '(i5, 2a5, i5, 3f8.3, 3f8.4)')&
                         currmol, molname, atname, currat, & 
                         sys%species(t)%atom(j)%pos%X(i)/10._mc_prec, &
                         sys%species(t)%atom(j)%pos%Y(i)/10._mc_prec, &
                         sys%species(t)%atom(j)%pos%Z(i)/10._mc_prec, &
                         0._mc_prec, 0._mc_prec, 0._mc_prec
                end do
            end do
            if(sys%species(t)%fluctuating)then
                do j = 1, sys%species(t)%nats
                    currat = currat + 1
                    atname = sys%species(t)%atom(j)%name(1:5)
                    write(fil, '(i5, 2a5, i5, 3f8.3, 3f8.4)')&
                         currmol, molname, atname, currat, & 
                         sys%species(t)%fractional_molecule%X(j)/10._mc_prec, &
                         sys%species(t)%fractional_molecule%Y(j)/10._mc_prec, &
                         sys%species(t)%fractional_molecule%Z(j)/10._mc_prec, &
                         0._mc_prec, 0._mc_prec, 0._mc_prec
                end do                
            end if
        end do
        !Literally the worst choice for boundary conditions
        !As though being allowed to omit triclinic coordinates is a superuseful good thing
        boundary = sys%box%get_boundary()/10._mc_prec
        write(fil, '(9f8.3)') boundary(1, 1), &
                             boundary(2, 2), &
                             boundary(3, 3), &
                             boundary(2, 1), &
                             boundary(3, 1), &
                             boundary(1, 2), &
                             boundary(3, 2), &
                             boundary(1, 3), &
                             boundary(2, 3)
    end subroutine

end module

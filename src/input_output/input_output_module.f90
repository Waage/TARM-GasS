module input_output_module
    use error_handling_module, only: error
    use precision_module, only: mc_prec
    implicit none
    private

    public :: get_argument, get_next_line, get_to_position, split

    interface get_argument 
        module procedure get_argument_real
        module procedure get_argument_real_array
        module procedure get_argument_integer
        module procedure get_argument_integer_array
        module procedure get_argument_string_array
        module procedure get_argument_line
    end interface
contains

    function split(line, key, n_segments) result(out_array)
        integer, intent(in) :: n_segments
        character(1) :: key
        character(*) :: line
        character(:), allocatable :: tmp
        character(100), dimension(n_segments) :: out_array
        integer :: i, index

        allocate(tmp, source=line)
        tmp = trim(adjustl(tmp))
        do i = 1, n_segments - 1
            index = scan(tmp, key)
            if(index == 0)then
                out_array(i) = tmp
                out_array(i + 1:n_segments) = ''
                return
            end if
            out_array(i) = trim(adjustl(tmp(1:index - 1)))
            tmp = trim(adjustl(tmp(index + 1:)))
        end do
        out_array(n_segments) = trim(adjustl(tmp))
        deallocate(tmp)
        return
    end function

    subroutine get_argument_integer(file_unit, keyword, arg, notfound)
        
        integer, intent(in) :: file_unit
        character(*), intent(in) :: keyword
        logical, intent(out) :: notfound
        integer :: ierr
        integer, intent(out) :: arg


        character(100) :: argument

        call get_argument_line(file_unit, keyword, argument, notfound)
        if(.not.notfound) then
            read(argument, *, iostat = ierr) arg
            if (is_iostat_end(ierr)) then
                call error("Failed to read what we wanted for ", keyword, "not writing "//argument//"as argument")
            end if
        end if


    end subroutine

    subroutine get_argument_integer_array(file_unit, keyword, arg, notfound)
        
        integer, intent(in) :: file_unit
        character(*), intent(in) :: keyword
        logical, intent(out) :: notfound
        integer :: ierr
        integer, dimension(:), intent(out) :: arg


        character(100) :: argument

        call get_argument_line(file_unit, keyword, argument, notfound)

        if(.not.notfound) then
            read(argument, *, iostat = ierr) arg
            if (is_iostat_end(ierr)) then
                call error("Failed to read what we wanted for ", keyword, "not writing "//argument//"as argument")
            end if
        end if
    end subroutine

    subroutine get_argument_real(file_unit, keyword, arg, notfound)
        
        integer, intent(in) :: file_unit
        character(*), intent(in) :: keyword
        logical, intent(out) :: notfound
        integer :: ierr
        real(kind=mc_prec), intent(out) :: arg


        character(100) :: argument

        call get_argument_line(file_unit, keyword, argument, notfound)

        if(.not.notfound) then
            read(argument, *, iostat = ierr) arg
            if (is_iostat_end(ierr)) then
                call error("Failed to read what we wanted for ", keyword, "not writing "//argument//"as argument")
            end if
        end if
    end subroutine

    subroutine get_argument_real_array(file_unit, keyword, arg, notfound)
        
        integer, intent(in) :: file_unit
        character(*), intent(in) :: keyword
        logical, intent(out) :: notfound
        integer :: ierr
        real(kind=mc_prec), dimension(:), intent(out) :: arg


        character(100) :: argument

        call get_argument_line(file_unit, keyword, argument, notfound)

        if(.not.notfound) then
            read(argument, *, iostat = ierr) arg
            if (is_iostat_end(ierr)) then
                call error("Failed to read what we wanted for ", keyword, "not writing "//argument//"as argument")
            end if
        end if
    end subroutine

    subroutine get_argument_string_array(file_unit, keyword, arg, notfound)
        
        integer, intent(in) :: file_unit
        character(*), intent(in) :: keyword
        logical, intent(out) :: notfound
        character(*), dimension(:), intent(out) :: arg


        character(100) :: argument

        call get_argument_line(file_unit, keyword, argument, notfound)

        arg = split(argument, " ", size(arg))

    end subroutine

    subroutine get_argument_line(file_unit, keyword, arg, notfound)
        
        integer, intent(in) :: file_unit
        character(*), intent(in) :: keyword
        logical, intent(out) :: notfound
        character(100), intent(out) :: arg

        integer :: ierr
        logical at_end
        character(100) :: line
        character(100), dimension(2) :: words

        rewind(file_unit)
        at_end = .false.
        notfound = .true.
        do while ( .not.at_end )    
            read(file_unit, '(A)', iostat = ierr) line 
            at_end = is_iostat_end(ierr)
            if(line_is_empty(line)) cycle

            line = trim(adjustl(line))
            words = split(line, ' ', 2)
            if(trim(adjustl(words(1))) == keyword)then
                arg = words(2)
                words = split(arg, '!', 2) !Peel away any comments
                arg = trim(adjustl(words(1)))
                notfound = .false.
                exit
            end if
        end do
    
    end subroutine

    subroutine get_to_position(file_unit, keyword, notfound)
        
        integer, intent(in) :: file_unit
        character(*), intent(in) :: keyword
        logical, intent(out) :: notfound

        logical :: at_end
        integer :: ierr
        character(100) :: line
        character(100), dimension(2) :: words

        rewind(file_unit)
        at_end = .false.
        notfound = .true.
        do while ( .not.at_end )    
            read(file_unit, '(A)', iostat = ierr) line 
            at_end = is_iostat_end(ierr)
            if(at_end)exit !A bit dumb ordering of things here
            if(line_is_empty(line)) cycle

            line = trim(adjustl(line))
            words = split(line, ' ', 2)
            if(words(1) == keyword)then
                notfound = .false.
                exit
            end if
        end do
    
    end subroutine


    subroutine get_next_line(file_unit, line, ierr)
        !Get next non - empty (blank or pure comment) line
        integer, intent(in) :: file_unit
        character(*) :: line
        character(100), dimension(2) :: string_array
        integer :: ierr
        logical :: empty_line

        empty_line = .true.
        do while(empty_line)        
            read(file_unit, '(A)', iostat = ierr) line
            if(is_iostat_end(ierr)) then
                return
            end if
            if(line_is_empty(line)) then
                cycle
            else
                empty_line = .false.
            end if
            string_array = split(line, '!', 2) !Peel away any comments
            line = trim(adjustl(string_array(1)))
        end do
        return
    
    end subroutine
    
    
    function line_is_empty(line)
        !Determine if a line is a comment
        logical :: line_is_empty
        character(100) :: line
        
        line = adjustl(trim(line))
        line_is_empty = (line(1:1) == '!').or.(line == "")
    
        return
    end function

end module

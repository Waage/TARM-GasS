module initialize_module
    use direct_energy_storage_module, only: direct_energy_storage
    use dlpoly_config_module, only: read_dlpoly_configuration_from_file
    use error_handling_module, only: error, get_n_error, get_max_warn, get_n_warn, set_max_warn, warn_missing, warn_unknown
    use ewald_module, only: ewald_type
    use general_utilities_module, only: is_nonzero
    use gro_file_module, only: read_gro_file
    use input_output_module, only: get_argument, get_next_line, get_to_position, split
    use lennard_jones_module, only: lennard_jones_interaction
    use lennard_jones_mixing_rules, only: lennard_jones_mixing_rules_method
    use lennard_jones_mix_override_module, only: lennard_jones_mix_override
    use mc_moves_module, only: mc_moves_container
    use molecule_module, only: single_molecule
    use parameters_module, only: chargeconv, bar_to_kcal_per_mol, k_to_kcal_per_mol
    use precision_module, only: mc_prec
    use simulation_top_level_module, only: simulation_top_level_container
    use system_module, only: system
    use xyz_file_module, only: read_xyz_file
    implicit none
    private

    public :: simulator_initialize_from_file

contains

    subroutine simulator_initialize_from_file(file_unit, input_directory, top)
        type(simulation_top_level_container) :: top
        integer :: file_unit
        integer :: read_int
        logical :: notfound
        real(kind=mc_prec) :: real_dummy 
        integer :: i, m1, m2, a1, a2
        integer :: n_types
        type(single_molecule), dimension(:), allocatable :: prototype
        character(256), intent(in) :: input_directory

        !Blergh
        real(kind=mc_prec), dimension(:, :, :), allocatable :: transrot_limits

        !Set maximum number of warnings (this should be done differently but whatever)
        call get_argument(file_unit, "max_warn", i, notfound)
        if(notfound) then
            call set_max_warn(0) 
        else
            call set_max_warn(i)
        end if
        call get_argument(file_unit, "seed", top%seed, notfound)
        if(notfound) call system_clock(top%seed) !Well I for one dont like this

        call get_argument(file_unit, "n_equil", top%n_equil, notfound)
        if(notfound) then
            call warn_missing("n_equil", "0")
            top%n_equil = 0
        end if

        call get_argument(file_unit, "n_prod", top%n_prod, notfound)
        if(notfound) then
            call warn_missing("n_prod", "0")
            top%n_prod = 0
        end if

        call get_argument(file_unit, "n_systems", read_int, notfound)
        if(notfound) then
            call warn_missing("n_systems", "1")
            read_int = 1
        end if
        call top%set_n_systems(read_int)

        call get_argument(file_unit, "Lennard_Jones_cutoff", top%cutoff_radius, notfound)
        if(notfound) then
            call error("Lennard_Jones_cutoff", "not found", &
                "set to a number using the syntax Lennard_Jones_cutoff X, where X is the cut - off in Aangstrom")
        end if

        call get_argument(file_unit, "ewald_precision", top%ewald_precision, notfound)
        if(notfound) then
            call warn_missing("ewald_precision", "1E-5")
            top%ewald_precision = 1E-5
        end if

        call get_argument(file_unit, "temperature", top%temperature, notfound)
        if(notfound) then
            call error("temperature",  "not found", &
                "set to a number using the syntax temperature X, where X is the temperature in Kelvin")
        end if

        call get_argument(file_unit, "pressure", top%pressure, notfound)
        if(notfound) then
            call error("pressure",  "not found", &
                "set to a number using the syntax pressure X, where X is the pressure in bar")
        end if
        top%pressure = top%pressure*bar_to_kcal_per_mol

        call get_argument(file_unit, "n_molecule_types", n_types, notfound)
        if(notfound) then
            call error("n_molecule_types",  "not found", &
                "set to a number using the syntax n_molecule_types X, where X is the number of molecule types")
        end if
        !This should all go into a type bound routine
        allocate(prototype(n_types))
        allocate(transrot_limits(size(top%systems), n_types, 2))
        transrot_limits = 0.1_mc_prec

        do i = 1, size(top%systems)
            call top%systems(i)%set_n_types(n_types)
        end do
        call get_to_position(file_unit, "molecule_definitions", notfound)
        if(notfound) then
            call error("molecule_definitions block",  "not found", &
                "this block must be defined, see example file")
        end if

        do i = 1, n_types
            call define_molecules(file_unit, top, prototype(i), i, transrot_limits)            
        end do

        call get_argument(file_unit, "lj_sigma_override", read_int, notfound)
        if(notfound) read_int = 0
        allocate(top%lj_sigma_override(read_int))
        do i = 1, read_int
            read(file_unit, *) a1, a2, m1, m2, real_dummy
            top%lj_sigma_override(i) = lennard_jones_mix_override(a1, a2, m1, m2, real_dummy)
        end do

        call get_argument(file_unit, "lj_epsilon_override", read_int, notfound)
        if(notfound) read_int = 0
        allocate(top%lj_epsilon_override(read_int))
        do i = 1, read_int
            read(file_unit, *) a1, a2, m1, m2, real_dummy
            top%lj_epsilon_override(i) = lennard_jones_mix_override(a1, a2, m1, m2, real_dummy)
        end do

        call get_argument(file_unit, "configuration_files", top%conf_files, notfound)
        if(notfound) then
            call error("configuration_files",  "not found", &
                "these files must be specified, see examples for more details")
        end if
        call initialize_systems(top, input_directory, prototype)

        top%moves = mc_moves_container(top%systems, file_unit, transrot_limits)

        if (get_n_warn() > get_max_warn()) then
            call error("number of warnings", &
                "exceeds maximal number of warnings", &
                "changing maximum number of warnings or making less mistakes")
        end if

        if(get_n_error() > 0)then
            write(*, *) get_n_error(), 'errors found! Aborting simulation.'
            stop
        end if

    end subroutine
    
    subroutine define_molecules(file_unit, top, prototype, current_species, transrot_limits)
    
        integer, intent(in) :: file_unit
        type(simulation_top_level_container), intent(inout) :: top
        type(single_molecule), intent(inout) :: prototype
        integer, intent(in) :: current_species
        real(kind=mc_prec), dimension(:, :, :), intent(inout) :: transrot_limits
        character(100) :: line, command, argument
        character(100), dimension(2) :: string_array
        integer :: tmp_unit
        integer, dimension(:), allocatable :: integer_array 
        real(kind=mc_prec), dimension(:), allocatable :: real_array 
        logical :: finished
        integer :: ierr
        integer :: i

        allocate(integer_array(size(top%systems)))
        allocate(real_array(size(top%systems)))
        finished = .false.
        do while(.not.finished)
            call get_next_line(file_unit, line, ierr)
            if(is_iostat_end(ierr)) then
                call error("topology file", &
                           "reached end of file while defining molecules", &
                           "adding the end molecule_definitions directive at an appropriate line")
                return
            end if
            string_array = split(line, ' ', 2)
            command = string_array(1)
            argument = string_array(2)
            select case(command)
            case("molecule")
                !Potential for sanitycheck here
            case("name")
                call prototype%set_molname(argument)
            case("n_molecules")
                read(argument, *) integer_array
                do i = 1, size(top%systems)
                    call top%systems(i)%set_nmol(current_species, integer_array(i))
                end do
            case("definition")
                select case(argument)
                case("define_in_file")
                    call define_from_file(file_unit, prototype)
                case default
                    open(newunit=tmp_unit, file=argument, status='old') 
                    call define_from_file(tmp_unit, prototype)
                    close(tmp_unit)
                end select
            case("fractional_molecule")
                select case(argument)
                case("none")
                    do i = 1, size(top%systems)
                        top%systems(i)%species(current_species)%fluctuating = .false.
                        call top%systems(i)%set_frac(current_species, 0._mc_prec)
                    end do
                case default
                    read(argument, *) real_array
                    do i = 1, size(top%systems)
                        !Maybe add a sanity check here?
                        top%systems(i)%species(current_species)%fluctuating = .true.
                        call top%systems(i)%set_frac(current_species, real_array(i))
                    end do
                end select
            case("chemical_potential")
                read(argument, *) real_array
                do i = 1, size(top%systems)
                    top%systems(i)%species(current_species)%mu = real_array(i)
                end do
            case("max_disp")
                read(argument, *) real_array
                do i = 1, size(top%systems)
                    transrot_limits(i, current_species, 1) = real_array(i)
                end do
            case("max_rot")
                read(argument, *) real_array
                do i = 1, size(top%systems)
                    transrot_limits(i, current_species, 2) = real_array(i)
                end do
            case("end")
                string_array = split(argument, ' ', 2)
                if (string_array(1) == "molecule") then
                    finished = .true.
                else 
                    call error("end "//argument, &
                        "encountered when searching for end molecule", &
                        " checking the order of arguments")
                end if
            case default
                call warn_unknown("directive ", command)

            end select
        end do
    end subroutine

    subroutine define_from_file(file_unit, prototype)
    
        integer, intent(in) :: file_unit
        type(single_molecule), intent(inout) :: prototype

        character(100) :: line, command, argument
        character(100), dimension(5) :: string_array
        integer :: ierr, n_atoms
        logical :: finished
        integer :: i

        finished = .false.
        do while(.not.finished)
            call get_next_line(file_unit, line, ierr)
            if(is_iostat_end(ierr)) then
                call error("molecule definition", &
                           "reached end of file while defining molecules", &
                           "adding the end definition directive at an appropriate line")
                return
            end if
            string_array(1:2) = split(line, ' ', 2)
            command = trim(string_array(1))
            argument = string_array(2)

            select case(command)
            case("n_atoms")
                read(argument, *) n_atoms
                call prototype%set_natoms(n_atoms)
                prototype%nvdw = 0
                prototype%ncharged = 0
                do i = 1, n_atoms
                    call get_next_line(file_unit, line, ierr)
                    if(is_iostat_end(ierr)) then
                        call error(prototype%molname, &
                                   "reached end of file while defining atoms", &
                                   "make sure the correct amount of atoms are present")
                        return
                    end if
                    string_array = split(line, ' ', 5) 
                    read(string_array(1), *) prototype%name(i) 
                    read(string_array(2), *) prototype%weight(i) 
                    read(string_array(3), *) prototype%charge(i) 
                    if(is_nonzero(prototype%charge(i))) then
                        prototype%ischarged(i) = .true.
                        prototype%ncharged = prototype%ncharged + 1
                        prototype%charge(i) = chargeconv*prototype%charge(i)
                    else
                        prototype%ischarged(i) = .false.
                    end if
                    read(string_array(4), *) prototype%lj_epsilon(i)
                    if(is_nonzero(prototype%lj_epsilon(i))) then
                        prototype%isvdw(i) = .true.
                        prototype%nvdw = prototype%nvdw + 1
                    else
                        prototype%isvdw(i) = .false.
                    end if
                    read(string_array(5), *) prototype%lj_sigma(i) 
                end do
            case("rotation_type")
                select case(argument)
                case("point_particle")
                    prototype%rot_type = 1
                case("linear")
                    prototype%rot_type = 2
                case("spatial")
                    prototype%rot_type = 3
                case default
                    call error("unknown rotation_type", argument, &
                                "using either point_particle, linear or spatial")
                end select
            case("geometry")
                do i = 1, prototype%nats 
                    call get_next_line(file_unit, line, ierr)
                    if(is_iostat_end(ierr)) then
                        call error(prototype%molname, &
                                   "reached end of file while defining atom geometry", &
                                   "make sure the correct amount of atoms are present")
                        return
                    end if
                    string_array(1:4) = split(line, ' ', 4) 
                    !First is the atom name, can use for sanity check
                    read(string_array(2), *) prototype%X(i)
                    read(string_array(3), *) prototype%Y(i)
                    read(string_array(4), *) prototype%Z(i)
                end do
            case("end")
                if (argument == "definition") then
                    finished = .true.
                else 
                    call error("end "//argument, &
                               "encountered when searching for end definition", &
                               "checking the order of arguments")
                end if
            case default
                call warn_unknown("directive when defining a molecule", command)
            end select

        end do

    end subroutine

    subroutine initialize_systems(top, input_directory, prototype)

        type(simulation_top_level_container) :: top
        type(single_molecule), dimension(:) :: prototype
        character(100), dimension(2) :: words
        character(256), intent(in) :: input_directory
        integer :: conf_unit

        integer :: current_system

        do current_system = 1, size(top%systems)
            associate(sys=> top%systems(current_system))
            !First copy duplicate information from top to system - specific places, and allocate whatever needs allocating
            sys%pressure = top%pressure
            sys%beta = 1._mc_prec/top%temperature/K_to_kcal_per_mol !T => 1/RT
            sys%rc = top%cutoff_radius
            sys%rc2 = sys%rc*sys%rc 

            call sys%species%initialize(prototype)
            sys%totmol = sum(sys%species%nmol) + count(sys%species%fluctuating)
            
            !Now read the current configuration
            words = split(top%conf_files(current_system), '.', 2)
            open(newunit=conf_unit, file=trim(input_directory)//top%conf_files(current_system), status='old')
            select case(trim(words(2))) !Check the suffix for known configuration files
            case("dlpolyconf")
                call read_dlpoly_configuration_from_file(conf_unit, sys)
            case("xyz")
                call read_xyz_file(conf_unit, sys)
            case("gro")
                call read_gro_file(conf_unit, sys)
            case default
                call error("Unknown format for configuration .", words(2), "using known formats (.dlpolyconf)")
                return
            end select
            call top%set_output_conf_format(suff=words(2), system=current_system)
            close(conf_unit)

            sys%lennard_jones = lennard_jones_interaction(prototype, &
                                                          top%lj_sigma_override, &
                                                          top%lj_epsilon_override, &
                                                          top%cutoff_radius&
                                                          )
            sys%coulombic = ewald_type(prototype, top%ewald_precision, sys%rc, sys%box)
            

            sys%energy_storage = direct_energy_storage(sys%species(:)%storage_space)
 
            call sys%update_boundary()
            call sys%reinsert()

            end associate
        end do

    end subroutine

end module

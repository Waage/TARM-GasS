module xyz_file_module
    use precision_module, only: mc_prec
    use system_module, only: system
    use geometry_interface_module, only: create_box
    implicit none
    private
    public :: read_xyz_file, write_xyz_file


contains

    subroutine read_xyz_file(fil, sys)
        !Seems to work
        implicit none
        !Extended .xyz format
        integer :: fil, totats, i, j, t
        class(system) :: sys
        integer :: currat
        real(kind=mc_prec), dimension(3, 3) :: boundary

        Character(300) :: dummy
        Character(300) :: extended_info 
        integer :: index1, index2

        read(fil, *)totats
        read(fil, '(A)')extended_info
        !Assuming that " only occurs when defining the lattice parameter,
        !which is probably a really good assumption that won't backfire
        index1 = index(extended_info, '"')
        index2 = index(extended_info, '"', back=.true.)
        dummy = extended_info(index1 + 1:index2 - 1)
        read(dummy, *) boundary(1:3, 1), boundary(1:3, 2), boundary(1:3, 3)
        call create_box(sys%box, boundary)
        currat = 0
        do t = 1, size(sys%species)
          do i = 1, sys%species(t)%nmol
            do j = 1, sys%species(t)%nats
              currat = currat + 1
              read(fil, *) dummy(1:3), & !Name of atom
                          sys%species(t)%atom(j)%pos%X(i), &
                          sys%species(t)%atom(j)%pos%Y(i), &
                          sys%species(t)%atom(j)%pos%Z(i)
            end do
          end do
          if(sys%species(t)%fluctuating)then
            currat = currat + 1
            do j = 1, sys%species(t)%nats
              read(fil, *) dummy(1:3), & !Name of atom
                          sys%species(t)%fractional_molecule%X(j), &
                          sys%species(t)%fractional_molecule%Y(j), &
                          sys%species(t)%fractional_molecule%Z(j)
            end do
            else
                do j = 1, sys%species(t)%nats
                    sys%species(t)%fractional_molecule%X(j) = 0._mc_prec
                    sys%species(t)%fractional_molecule%Y(j) = 0._mc_prec
                    sys%species(t)%fractional_molecule%Z(j) = 0._mc_prec
                end do
                
            end if
        end do
    end subroutine

    subroutine write_xyz_file(fil, sys)
        implicit none
        !Rudimentary printout system, extended .xyz format
        integer :: fil, totats, i, j, t
        class(system) :: sys
        integer :: currat
        real(kind=mc_prec), dimension(3, 3) :: boundary

        Character(100) :: dummy
        Character(300) :: extended_info 

        boundary = sys%box%get_boundary()
        totats = 0
        do i = 1, size(sys%species)
           totats = sys%species(i)%nats*sys%species(i)%nmol + totats
           if(sys%species(i)%fluctuating)then 
              totats = totats + sys%species(i)%nats
           end if
        end do
        !Build a string to print out information for the extended .xyz file format
        extended_info='Lattice="'
        do i = 1, 3
            write(dummy, *) boundary(1:3, i)
            extended_info = trim(adjustl(extended_info))//" "//trim(adjustl(dummy))
        end do
        extended_info = trim(adjustl(extended_info))//'"Properties=species:S:'
        write(dummy, *) size(sys%species)
        extended_info = trim(adjustl(extended_info))//trim(adjustl(dummy))//':pos:R3' 
        !Write the output file
        write(fil, *)totats
        write(fil, *)trim(adjustl(extended_info))
        currat = 0
        do t = 1, size(sys%species)
          do i = 1, sys%species(t)%nmol
            do j = 1, sys%species(t)%nats
              currat = currat + 1
              write(fil, *)sys%species(t)%atom(j)%name(1:1), &
                          sys%species(t)%atom(j)%pos%X(i), &
                          sys%species(t)%atom(j)%pos%Y(i), &
                          sys%species(t)%atom(j)%pos%Z(i)
            end do
          end do
          if(sys%species(t)%fluctuating)then
            currat = currat + 1
            do j = 1, sys%species(t)%nats
              write(fil, *)sys%species(t)%atom(j)%name(1:1), &
                          sys%species(t)%fractional_molecule%X(j), &
                          sys%species(t)%fractional_molecule%Y(j), &
                          sys%species(t)%fractional_molecule%Z(j)
            end do
          end if
        end do
    end subroutine

end module

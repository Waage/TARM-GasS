target_sources(TARM_GasS_LIB PUBLIC 
    ${CMAKE_CURRENT_SOURCE_DIR}/argument_parser_module.f90
	${CMAKE_CURRENT_SOURCE_DIR}/gro_file_module.f90
 	${CMAKE_CURRENT_SOURCE_DIR}/xyz_file_module.f90
	${CMAKE_CURRENT_SOURCE_DIR}/dlpoly_config_module.f90
	${CMAKE_CURRENT_SOURCE_DIR}/initialize_module.f90
	${CMAKE_CURRENT_SOURCE_DIR}/error_handling_module.f90
	${CMAKE_CURRENT_SOURCE_DIR}/input_output_module.f90
	 )

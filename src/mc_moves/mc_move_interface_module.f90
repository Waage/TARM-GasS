module mc_move_interface_module
    use mc_move_class_simple_module, only: mc_move_class
    use translate_rotate_module, only: translate_rotate_move
    use volume_change_module, only: volume_change_move
    use swap_positions_module, only: swap_positions_move
    use change_fractional_parameter_module, only: change_fractional_parameter_move
    use swap_fractional_molecule_module, only: swap_fractional_molecule_move
    use identity_change_module, only: identity_change_move
    use precision_module, only: mc_prec
    use system_module, only: system
    implicit none
    private
    public :: mc_move_interface

    type mc_move_interface
        ! private
        class(mc_move_class), allocatable :: m
    contains
            
        procedure :: execute
        procedure :: update_limits
        procedure :: set_limits
        procedure :: print_success_rate
        procedure :: print_limits
        procedure :: print_time
        procedure :: get_rate
        procedure :: set_rate
        procedure :: no_rate_set
        procedure :: get_name
        procedure :: set_name

    end type

    interface mc_move_interface
        module procedure new_mc_move_interface
    end interface

contains

    function new_mc_move_interface(move_name, systems) result(move_interface)
        character(*), intent(in) :: move_name
        type(system), dimension(:), intent(in) :: systems
        type(mc_move_interface) :: move_interface

        select case(move_name)
        case("translate_rotate")
            allocate(move_interface%m, source = translate_rotate_move(systems))
        case("volume_change")
            allocate(move_interface%m, source = volume_change_move(systems))
        case("swap_positions")
            allocate(move_interface%m, source = swap_positions_move(systems))
        case("change_fractional_parameter")
            allocate(move_interface%m, source = change_fractional_parameter_move(systems))
        case("swap_fractional_molecule")
            allocate(move_interface%m, source = swap_fractional_molecule_move(systems))
        case("identity_change")
            allocate(move_interface%m, source = identity_change_move(systems))
        end select

        return
    end function

    subroutine execute(move_interface, systems)
        class(mc_move_interface) :: move_interface
        type(system), dimension(:), intent(inout) :: systems

        call move_interface%m%execute(systems)
    end subroutine

    subroutine update_limits(move_interface)
        class(mc_move_interface), intent(inout) :: move_interface

        call move_interface%m%update_limits()
    end subroutine

    subroutine set_limits(move_interface, limits)
        class(mc_move_interface), intent(inout) :: move_interface
        real(kind=mc_prec), dimension(:), intent(in) :: limits

        call move_interface%m%set_limits(limits)
    end subroutine

    subroutine print_success_rate(move_interface, file_unit)
        class(mc_move_interface), intent(in) :: move_interface
        integer, intent(in) :: file_unit

        call move_interface%m%print_success_rate(file_unit)
    end subroutine

    subroutine print_limits(move_interface, file_unit)
        class(mc_move_interface), intent(in) :: move_interface
        integer, intent(in) :: file_unit

        call move_interface%m%print_limits(file_unit)
    end subroutine

    subroutine print_time(move_interface, file_unit)
        class(mc_move_interface), intent(inout) :: move_interface
        integer, intent(in) :: file_unit
    
        call move_interface%m%print_time(file_unit)
    end subroutine

    subroutine no_rate_set(move_interface)   
        class(mc_move_interface) :: move_interface

        call move_interface%m%no_rate_set()
    end subroutine
    
    elemental function get_rate(move_interface) result(rate)
        class(mc_move_interface), intent(in) :: move_interface
        integer :: rate

        rate = move_interface%m%get_rate()
        return
    end function

    pure subroutine set_rate(move_interface, rate)
        class(mc_move_interface), intent(inout) :: move_interface
        integer, intent(in) :: rate
        
        move_interface%m%rate = rate
    end subroutine

    pure subroutine set_name(move_interface, name)
        class(mc_move_interface), intent(inout) :: move_interface
        character(50), intent(in) :: name
        
        move_interface%m%name = name
    end subroutine

    pure function get_name(move_interface) result(name)
        class(mc_move_interface), intent(in) :: move_interface
        character(50) :: name

        name = move_interface%m%name
        return
    end function

end module

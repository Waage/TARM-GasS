module swap_positions_module
    use reduced_dimensionality_energy_storage_module, only: reduced_dimensionality_energy_storage
    use ewald_module, only: ewald_type
    use mc_move_class_simple_module, only: mc_move_class
    use molecule_module, only: single_molecule
    use precision_module, only: mc_prec
    use system_module, only: system
    implicit none
    private
    public :: swap_positions_move

    type, extends(mc_move_class) :: swap_positions_move

        real(kind=mc_prec) :: dele
        integer :: index1, index2, t1, t2, cur_sys
        type(single_molecule), allocatable :: newpos1, newpos2
        class(ewald_type), allocatable :: coulombic
        type(reduced_dimensionality_energy_storage), allocatable :: new_energy_storage_1, new_energy_storage_2
        integer, dimension(:, :, :), allocatable :: attempts, successes

    contains
        procedure :: initialize_move  
        procedure :: compute_probability
        procedure :: accept
        procedure :: reject
        procedure :: update_limits
        procedure :: set_limits
        procedure :: print_success_rate
        procedure :: print_limits
    end type

    interface swap_positions_move
        module procedure new_swap_positions_move
    end interface
        

contains

    function new_swap_positions_move(systems) result(new_move)
        type(swap_positions_move) :: new_move
        type(system), dimension(:), intent(in) :: systems
        new_move%name = "swap_positions"
        allocate(new_move%attempts(size(systems), size(systems(1)%species), size(systems(1)%species)))
        allocate(new_move%successes(size(systems), size(systems(1)%species), size(systems(1)%species)))
        new_move%attempts = 0
        new_move%successes = 0
    end function

    subroutine initialize_move(move, systems)
        use rngmod
        use random_rotations_module, only: rotate

        class(swap_positions_move) :: move
        type(system), dimension(:), intent(inout) :: systems
        integer :: nsystems
        real(kind=mc_prec), dimension(3) :: dr

        nsystems = size(systems)
        !Choose a random system to do the position swap in
        move%cur_sys = int(rng()*nsystems) + 1
        associate(sys => systems(move%cur_sys))

        if(size(sys%species) < 2)return !No point in swapping the positions of identical molecules
        
        move%t1 = int(rng()*size(sys%species)) + 1
        move%t2 = int(rng()*(size(sys%species) - 1)) + 1
        
        if(move%t2 >= move%t1)  move%t2 = move%t2 + 1

        move%attempts(move%cur_sys, move%t1, move%t2) = move%attempts(move%cur_sys, move%t1, move%t2) + 1

        if((.not.sys%species(move%t1)%has_movable_molecule()).or.(.not.sys%species(move%t2)%has_movable_molecule() ))then
            return
        end if
        !Choose a molecule of the species. Make sure to account for fractional molecules
        if(sys%species(move%t1)%fluctuating)then
             move%index1 = int(rng()*(sys%species(move%t1)%nmol + 1))
        else
             move%index1 = int(rng()*sys%species(move%t1)%nmol) + 1
        end if
        if(sys%species(move%t2)%fluctuating)then
             move%index2 = int(rng()*(sys%species(move%t2)%nmol + 1))
        else
             move%index2 = int(rng()*sys%species(move%t2)%nmol) + 1
        end if
        move%newpos1 = sys%species(move%t1)%get_molecule(move%index1)
        move%newpos2 = sys%species(move%t2)%get_molecule(move%index2)
        dr(1) = move%newpos1%X(1) - move%newpos2%X(1)
        dr(2) = move%newpos1%Y(1) - move%newpos2%Y(1)
        dr(3) = move%newpos1%Z(1) - move%newpos2%Z(1)
        call move%newpos1%translate( -dr)
        call move%newpos2%translate(dr)

        if(sys%species(move%t1)%rot_type > 1)call rotate(move%newpos1, sys%box)
        if(sys%species(move%t2)%rot_type > 1)call rotate(move%newpos2, sys%box)
        
        if(sys%species(move%t1)%ncharged > 0.or.sys%species(move%t2)%ncharged > 0)then
            allocate(move%coulombic, source = sys%coulombic)
        end if
        
        move%new_energy_storage_1 = reduced_dimensionality_energy_storage(sys%species(:)%storage_space)
        move%new_energy_storage_2 = reduced_dimensionality_energy_storage(sys%species(:)%storage_space)
        end associate
    end subroutine

    subroutine compute_probability(move, systems, probability)
        use energy_module, only: swap_pos_ener
        
        class(swap_positions_move) :: move
        type(system), dimension(:), intent(inout) :: systems
        real(kind=mc_prec), intent(out) :: probability
        real(kind=mc_prec) :: delta_fourier_ewald_energy

        associate(sys => systems(move%cur_sys))
        if((.not.sys%species(move%t1)%has_movable_molecule()).or.(.not.sys%species(move%t2)%has_movable_molecule() ))then
            probability = - 1.0
            return
        end if
        call swap_pos_ener(sys, move%newpos1, move%newpos2, move%index1, move%index2, &
                            move%t1, move%t2, move%coulombic, delta_fourier_ewald_energy, &
                            move%new_energy_storage_1, move%new_energy_storage_2)
        
        move%dele = delta_fourier_ewald_energy &
                  + move%new_energy_storage_1%array_sum(sys%species(:)%nmol) &
                  - sys%energy_storage%array_sum(move%t1, move%index1, sys%species(:)%nmol) &
                  + move%new_energy_storage_2%array_sum(sys%species(:)%nmol) &
                  - sys%energy_storage%array_sum(move%t2, move%index2, sys%species(:)%nmol)

        !Correct for double - counting
        move%dele = move%dele - move%new_energy_storage_1%get_value(move%t2, move%index2) &
                              + sys%energy_storage%get_value(move%t2, move%t1, move%index2, move%index1) 

        probability = move%exp_probability( -sys%beta*move%dele)
        end associate
    end subroutine

    subroutine accept(move, systems)
        class(swap_positions_move) :: move
        type(system), dimension(:), intent(inout) :: systems

        associate(sys => systems(move%cur_sys))
        
        move%successes(move%cur_sys, move%t1, move%t2) = move%successes(move%cur_sys, move%t1, move%t2) + 1

        call sys%species(move%t1)%set_molecule(move%newpos1, move%index1)
        call sys%species(move%t2)%set_molecule(move%newpos2, move%index2)
        if(sys%species(move%t1)%ncharged > 0.or.sys%species(move%t2)%ncharged > 0)then
            call sys%update_energy_storage(move%t1, move%index1, move%new_energy_storage_1, move%coulombic)
        else
            call sys%update_energy_storage(move%t1, move%index1, move%new_energy_storage_1)
        end if
        call sys%update_energy_storage(move%t2, move%index2, move%new_energy_storage_2)
        call sys%update_running_difference(move%dele)

        deallocate(move%newpos1, move%newpos2, move%new_energy_storage_1, move%new_energy_storage_2)
        end associate
    end subroutine

    subroutine reject(move, systems)
        class(swap_positions_move) :: move
        type(system), dimension(:), intent(inout) :: systems

        associate(sys => systems(move%cur_sys))
        if((.not.sys%species(move%t1)%has_movable_molecule()).or.(.not.sys%species(move%t2)%has_movable_molecule() ))then
             return
        end if
        end associate
        deallocate(move%newpos1, move%newpos2, move%new_energy_storage_1, move%new_energy_storage_2, move%coulombic)
    end subroutine

    subroutine set_limits(move, limits)
        class(swap_positions_move), intent(inout) :: move
        real(kind=mc_prec), dimension(:), intent(in) :: limits
        !Intentionally left empty
    end subroutine

    subroutine update_limits(move)
        class(swap_positions_move), intent(inout) :: move
        !Intentionally left empty
    end subroutine

    subroutine print_success_rate(move, file_unit)
        class(swap_positions_move), intent(in) :: move
        integer, intent(in) :: file_unit
        integer :: i, j, k
        write(file_unit, '(a)') trim(adjustl(move%name))//":"
        associate(sizes => shape(move%successes))
        do i = 1, sizes(1)
            write(file_unit, '(4x, a, i3, a1)') "System ", i, ":"
            do j = 1, sizes(2)
                do k = 1, sizes(3)
                    write(file_unit, '(8x, a, i3, a4, i3, a2, i7, a3 i7, a2, f6.2, 1x, a2)') "Species ", j, " to ", k, ": ", &
                            move%successes(i, j, k), " / ", move%attempts(i, j, k), &
                            " (", move%acceptance_rate(move%successes(i, j, k), move%attempts(i, j, k)), "%)"
                end do
            end do
        end do
        end associate
    end subroutine

    subroutine print_limits(move, file_unit)
        class(swap_positions_move), intent(in) :: move
        integer, intent(in) :: file_unit
        !Intentionally left empty
    end subroutine

end module

module mc_moves_module
    use input_output_module, only: get_argument
    use mc_move_interface_module, only: mc_move_interface
    use precision_module, only: mc_prec
    use rngmod, only: rng
    use system_module, only: system
    implicit none
    private
    public ::mc_moves_container

    type :: mc_moves_container
        integer :: n_possible_moves
        type(mc_move_interface), dimension(:), allocatable :: moves
    contains
        procedure :: set_move_limits
        procedure :: update_move_limits
        procedure :: print_times
        procedure :: print_success_rate
        procedure :: mc_move
        procedure :: movelim_debug
        procedure :: moves_per_cycle
    end type

    interface mc_moves_container
        module procedure new_mc_moves_container
    end interface

contains

    function new_mc_moves_container(systems, file_unit, transrot_limits) result(container)
        type(system), dimension(:), intent(in) :: systems
        integer, intent(in) :: file_unit

        type(mc_moves_container) :: container
        integer :: i, rate
        logical :: notfound

        real(kind=mc_prec), dimension(:, :, :) :: transrot_limits
        real(kind=mc_prec), dimension(:), allocatable :: vol_disp_atp

        container%n_possible_moves = 6
        allocate(container%moves(container%n_possible_moves))

        container%moves(1) = mc_move_interface("translate_rotate", systems)
        container%moves(2) = mc_move_interface("volume_change", systems)
        container%moves(3) = mc_move_interface("swap_positions", systems)
        container%moves(4) = mc_move_interface("change_fractional_parameter", systems)
        container%moves(5) = mc_move_interface("swap_fractional_molecule", systems)
        container%moves(6) = mc_move_interface("identity_change", systems)

        do i = 1, container%n_possible_moves
            call get_argument(file_unit, container%moves(i)%get_name(), rate, notfound)
            if(notfound) then
                call container%moves(i)%no_rate_set()
            else
                call container%moves(i)%set_rate(rate)
            end if
        end do

        allocate(vol_disp_atp(size(systems)))
        call get_argument(file_unit, "vol_disp_atp", vol_disp_atp, notfound)
        if(notfound) then
            vol_disp_atp = 1E-3
        end if

        call container%set_move_limits("translate_rotate", reshape(transrot_limits, (/size(transrot_limits)/)))
        call container%set_move_limits("volume_change", vol_disp_atp)

    end function

    subroutine set_move_limits(container, name, limits)
        class(mc_moves_container) :: container
        character(*), intent(in) :: name
        real(kind=mc_prec), dimension(:) :: limits
        integer :: i

        do i = 1, container%n_possible_moves
            if(container%moves(i)%get_name() == name) then
                call container%moves(i)%set_limits(limits)
            end if
        end do

    end subroutine

    subroutine update_move_limits(container)
        class(mc_moves_container) :: container
        integer :: i

        do i = 1, container%n_possible_moves
            call container%moves(i)%update_limits()
        end do
    end subroutine

    subroutine print_times(container, file_unit)
        class(mc_moves_container) :: container
        integer :: file_unit
        integer :: i

        write(file_unit, *) 'Time spent per move'
        do i = 1, container%n_possible_moves
            if(container%moves(i)%get_rate() > 0) then
                call container%moves(i)%print_time(file_unit)
            end if
        end do
    end subroutine

    subroutine print_success_rate(container, file_unit)
        class(mc_moves_container) :: container
        integer :: file_unit
        integer :: i


        write(file_unit, *) 'Success rate of each move'
        do i = 1, container%n_possible_moves
            if(container%moves(i)%get_rate() > 0) then
                call container%moves(i)%print_success_rate(file_unit)
            end if
        end do
    end subroutine

    subroutine mc_move(container, systems)
        class(mc_moves_container) :: container
        type(system), dimension(:), intent(inout) :: systems
        integer :: ran
        integer :: which_move

            which_move = 0
            ran = int(rng()*container%moves_per_cycle()) + 1
            do while(ran > 0)
                which_move = which_move + 1
                ran = ran - container%moves(which_move)%get_rate() 
            end do

            call container%moves(which_move)%execute(systems)

    end subroutine

    pure function moves_per_cycle(container) result(mpc)
        !Return the number of moves per cycle
        class(mc_moves_container), intent(in) :: container
        integer :: mpc

        mpc = sum(container%moves(:)%get_rate())
        return
    end function

    subroutine movelim_debug(container, file_unit)
        class(mc_moves_container) :: container
        integer :: file_unit, i

        do i = 1, container%n_possible_moves
            call container%moves(i)%print_limits(file_unit)
        end do
    end subroutine

end module mc_moves_module

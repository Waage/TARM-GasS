module volume_change_module
    use precision_module, only: mc_prec
    use parameters_module
    use system_module
    use mc_move_class_simple_module
    use ewald_module
    implicit none
    private
    public :: volume_change_move

    type, extends(mc_move_class) :: volume_change_move
        integer :: curSysIndex
        real(kind=mc_prec) :: newVolume, oldVolume
        type(system) :: newSystem
        integer, dimension(:), allocatable :: attempts, successes
        real(kind=mc_prec), dimension(:), allocatable :: maximum_volume_delta

    contains

        procedure :: initialize_move
        procedure :: compute_probability
        procedure :: accept
        procedure :: reject
        procedure :: update_limits
        procedure :: set_limits
        procedure :: print_success_rate
        procedure :: print_limits
    end type

    interface volume_change_move
        module procedure new_volume_change_move
    end interface

contains

    function new_volume_change_move(systems) result(new_move)
        type(volume_change_move) :: new_move
        type(system), dimension(:), intent(in) :: systems

        new_move%name = "volume_change"

        allocate(new_move%attempts(size(systems)))
        allocate(new_move%successes(size(systems)))
        allocate(new_move%maximum_volume_delta(size(systems)))
        new_move%attempts = 0
        new_move%successes = 0
        new_move%maximum_volume_delta = 1E-3

    end function

    subroutine initialize_move(move, systems)
        use rngmod
        
        class(volume_change_move) :: move
        type(system), dimension(:), intent(inout) :: systems
        integer :: t
        real(kind=mc_prec), dimension(3, 3) :: scale

        !Choose a random system to do the volume change on
        move%curSysIndex = int(rng()*size(systems)) + 1
        associate(oldSys => systems(move%curSysIndex), newSys => move%newSystem)

        move%attempts(move%curSysIndex) = move%attempts(move%curSysIndex) + 1

        newSys = oldSys
        !Generate new volume
        move%oldVolume = newSys%V
        move%newVolume = exp( log(newSys%V) + move%maximum_volume_delta(move%curSysIndex)*(rng() - .5_mc_prec) ) 
        !Scale molecule positions to new volume
        
        scale = (move%newVolume/move%oldVolume)**(1._mc_prec/3)

        !Shift positions of the molecules, assemble them
        do t = 1, size(newSys%species)
            call newSys%species(t)%rescale(scale)
            call newSys%species(t)%assemble(newSys%box)
        end do
        !Change the boundary conditions of the box, updating tail corrections and ewald stuff
        call newSys%update_boundary(newSys%box%get_boundary()*scale) 

        end associate
    end subroutine

    subroutine compute_probability(move, systems, probability)
        use energy_module, only: etot_cfc
        class(volume_change_move) :: move
        type(system), dimension(:), intent(inout) :: systems
        real(kind=mc_prec), intent(out) :: probability

        associate(oldSys => systems(move%curSysIndex), newSys => move%newSystem)

        call etot_cfc(newSys, newSys%species, newSys%coulombic, &
            newSys%total_energy, newSys%energy_storage) !Calculate energy of new configuration
        
        probability = move%exp_probability( -newSys%beta*&
                           ( newSys%total_energy - oldSys%total_energy&
                            + newSys%pressure*(move%newVolume - move%oldVolume)&
                            - (newSys%totmol + 1)*log(move%newVolume/move%oldVolume)/newSys%beta)&
                          )
        end associate
    end subroutine

    subroutine accept(move, systems)
        class(volume_change_move) :: move
        type(system), dimension(:), intent(inout) :: systems

        move%successes(move%curSysIndex) = move%successes(move%curSysIndex) + 1
        systems(move%curSysIndex) = move%newSystem

    end subroutine

    subroutine reject(move, systems)
        class(volume_change_move) :: move
        type(system), dimension(:), intent(inout) :: systems
        !Intentionally left empty
    end subroutine

    subroutine set_limits(move, limits)
        class(volume_change_move), intent(inout) :: move
        real(kind=mc_prec), dimension(:), intent(in) :: limits

        move%maximum_volume_delta = limits

    end subroutine

    subroutine update_limits(move)
        class(volume_change_move), intent(inout) :: move
        integer :: i

        do i = 1, size(move%maximum_volume_delta)
            if(move%attempts(i) > 100)then
                if( move%successes(i) > (move%attempts(i)/2 + move%attempts(i)/10) ) then
                    move%maximum_volume_delta(i) = 1.1_mc_prec*move%maximum_volume_delta(i)
                else if( move%successes(i) < (move%attempts(i)/2 - move%attempts(i)/10) ) then
                    move%maximum_volume_delta(i) = 0.9_mc_prec*move%maximum_volume_delta(i)
                end if
                move%successes(i) = 0
                move%attempts(i) = 0
            end if
        end do

    end subroutine

    subroutine print_success_rate(move, file_unit)
        class(volume_change_move), intent(in) :: move
        integer, intent(in) :: file_unit
        integer :: i
        write(file_unit, '(a)') trim(adjustl(move%name))//":"
        associate(sizes => shape(move%successes))
        do i = 1, sizes(1)
            write(file_unit, '(4x, a, i3, a2, i7, a3 i7, a2, f6.2, 1x, a2)') "System ", i, ": ", &
                    move%successes(i), " / ", move%attempts(i), &
                    " (", move%acceptance_rate(move%successes(i), move%attempts(i)), "%)"
        end do
        end associate
    end subroutine

    subroutine print_limits(move, file_unit)
        class(volume_change_move), intent(in) :: move
        integer, intent(in) :: file_unit

        write(file_unit, *) move%name, move%maximum_volume_delta

    end subroutine

end module

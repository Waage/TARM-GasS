!Refactor this post
module change_fractional_parameter_module
    use ewald_module, only: ewald_type
    use mc_move_class_simple_module, only: mc_move_class
    use molecule_module, only: single_molecule
    use parameters_module, only: fractional_index
    use precision_module, only: mc_prec
    use random_rotations_module, only: rotate
    use reduced_dimensionality_energy_storage_module, only: reduced_dimensionality_energy_storage
    use system_module, only: system

    implicit none
    private
    public :: change_fractional_parameter_move

    !Could use an enum but fortran enums are still bad
    integer, parameter :: onlyFraction = 1, fractionAndInsert = 2, fractionAndRemove = 3 

    type, extends(mc_move_class) :: change_fractional_parameter_move
        integer ::  molecule_type,  newfr_index, cur_sys
        real(kind=mc_prec) :: fold, fnew, dele
        type(single_molecule), allocatable ::  insmol
        type(single_molecule), allocatable ::  currfrac,  newfrac
        type(reduced_dimensionality_energy_storage), allocatable ::  new_energy_storage_fractional, new_energy_storage_normal
        class(ewald_type), allocatable :: coulombic 
        integer :: moveType
        integer, dimension(:, :), allocatable :: attempts, successes
        real(kind=mc_prec) :: maximal_fractional_change = 0.5_mc_prec
    contains

        procedure :: initialize_move
        procedure :: compute_probability
        procedure :: accept
        procedure :: reject
        procedure :: update_limits
        procedure :: set_limits
        procedure :: print_success_rate
        procedure :: print_limits
!lol
        procedure ::  chfrac_initialize_insert
        procedure ::  chfrac_initialize_remove
        procedure ::  chfrac_initialize_normal       

        procedure ::  chfrac_compute_probability_insert
        procedure ::  chfrac_compute_probability_remove
        procedure ::  chfrac_compute_probability_normal

        procedure ::  chfrac_accept_insert
        procedure ::  chfrac_accept_remove
        procedure ::  chfrac_accept_normal

        procedure ::  chfrac_reject_insert
        procedure ::  chfrac_reject_remove
        procedure ::  chfrac_reject_normal    
    end type

    interface change_fractional_parameter_move
        module procedure new_change_fractional_parameter_move
    end interface
        
contains

    function new_change_fractional_parameter_move(systems) result(new_move)
        type(change_fractional_parameter_move) :: new_move
        type(system), dimension(:), intent(in) :: systems

        new_move%name = "change_fractional_parameter"

        allocate(new_move%attempts(size(systems), size(systems(1)%species)))
        allocate(new_move%successes(size(systems), size(systems(1)%species)))
        new_move%attempts = 0
        new_move%successes = 0

    end function
    
    subroutine initialize_move(move, systems)
        use rngmod

        class(change_fractional_parameter_move) :: move
        type(system), dimension(:), intent(inout) :: systems
        integer :: i

        move%cur_sys = int(rng()*size(systems)) + 1
        do while( count(systems(move%cur_sys)%species(:)%fluctuating) < 1)
            move%cur_sys = int(rng()*size(systems)) + 1
        end do
        associate(sys => systems(move%cur_sys))
            
        !Choose the species for which the fractional molecule is to be altered
        i = int(rng()*count(sys%species%fluctuating)) + 1
        
        move%molecule_type = 0
        do while(i > 0)
             move%molecule_type =  move%molecule_type + 1
            if(sys%species(move%molecule_type)%fluctuating) i = i - 1
        end do

        move%attempts(move%cur_sys, move%molecule_type) = move%attempts(move%cur_sys, move%molecule_type) + 1
        
        move%fold = sys%species(move%molecule_type)%frac
        move%fnew = move%fold + move%maximal_fractional_change*(rng() - .5_mc_prec)
        if(move%fnew > 1._mc_prec)then
            move%fnew =  move%fnew - 1._mc_prec
            move%moveType = fractionAndInsert
            call move%chfrac_initialize_insert(sys)
        else if(move%fnew < 0._mc_prec)then
            move%fnew =  move%fnew + 1._mc_prec
            move%moveType = fractionAndRemove
            call move%chfrac_initialize_remove(sys)
        else
            move%moveType = onlyFraction
            call move%chfrac_initialize_normal(sys)
        end if
        end associate
    end subroutine

!I mean obviously this should be handled differently than a million switches but whatever
    subroutine compute_probability(move, systems, probability)
        class(change_fractional_parameter_move) :: move
        type(system), dimension(:), intent(inout) :: systems
        real(kind=mc_prec), intent(out) :: probability

        select case(move%moveType)
        case(fractionAndInsert)
            call move%chfrac_compute_probability_insert(systems(move%cur_sys), probability)
        case(fractionAndRemove)
            call move%chfrac_compute_probability_remove(systems(move%cur_sys), probability)
        case(onlyFraction)
            call move%chfrac_compute_probability_normal(systems(move%cur_sys), probability)
        end select

    end subroutine

    subroutine accept(move, systems)
        class(change_fractional_parameter_move) :: move
        type(system), dimension(:), intent(inout) :: systems

        move%successes(move%cur_sys, move%molecule_type) = move%successes(move%cur_sys, move%molecule_type) + 1
        select case(move%moveType)
        case(fractionAndInsert)
            call move%chfrac_accept_insert(systems(move%cur_sys))
        case(fractionAndRemove)
            call move%chfrac_accept_remove(systems(move%cur_sys))
        case(onlyFraction)
            call move%chfrac_accept_normal(systems(move%cur_sys))
        end select

    end subroutine

    subroutine reject(move, systems)
        class(change_fractional_parameter_move) :: move
        type(system), dimension(:), intent(inout) :: systems

        select case(move%moveType)
        case(fractionAndInsert)
            call move%chfrac_reject_insert(systems(move%cur_sys))
        case(fractionAndRemove)
            call move%chfrac_reject_remove(systems(move%cur_sys))
        case(onlyFraction)
            call move%chfrac_reject_normal(systems(move%cur_sys))
        end select

    end subroutine

    subroutine chfrac_initialize_normal(move, sys)

        class(change_fractional_parameter_move) :: move
        type(system), intent(inout) :: sys

        move%dele = 0._mc_prec
        if(sys%species(move%molecule_type)%ncharged > 0)then
            allocate(move%coulombic, source = sys%coulombic)
        end if
        move%new_energy_storage_fractional = reduced_dimensionality_energy_storage(sys%species(:)%storage_space)
        move%currfrac = sys%species(move%molecule_type)%get_molecule(fractional_index)
    end subroutine

    subroutine  chfrac_compute_probability_normal(move, sys, probability)
        use energy_module, only: epart_frac_single

        class(change_fractional_parameter_move) :: move
        type(system), intent(inout) :: sys
        real(kind=mc_prec), intent(out) :: probability
        real(kind=mc_prec) :: dbias, indirect_energy

        call epart_frac_single(sys, move%currfrac, move%molecule_type, move%coulombic, &
                                  move%fold, move%fnew, indirect_energy, move%new_energy_storage_fractional)
        move%dele = indirect_energy &
                  + move%new_energy_storage_fractional%array_sum(sys%species(:)%nmol) &
                  - sys%energy_storage%array_sum(move%molecule_type, fractional_index, sys%species(:)%nmol)

        dbias = sys%species(move%molecule_type)%bias%coupling_bias(move%fnew, move%fold)
        probability = move%exp_probability(dbias - sys%beta*(move%dele))
    end subroutine

    subroutine chfrac_accept_normal(move, sys)        

        class(change_fractional_parameter_move) :: move
        type(system), intent(inout) :: sys

        call sys%set_frac(move%molecule_type, move%fnew)
        if(sys%species(move%molecule_type)%ncharged > 0)then
            call sys%update_energy_storage(move%molecule_type, &
                                           fractional_index, move%new_energy_storage_fractional, move%coulombic)
        else
            call sys%update_energy_storage(move%molecule_type, &
                                           fractional_index, move%new_energy_storage_fractional)
        end if
        call sys%update_running_difference(move%dele)
        call sys%species(move%molecule_type)%bias%increment_bin_visited(move%fnew)

        deallocate(move%new_energy_storage_fractional)
    end subroutine

    subroutine chfrac_reject_normal(move, sys)

        class(change_fractional_parameter_move) :: move
        type(system), intent(inout) :: sys

        call sys%species(move%molecule_type)%bias%increment_bin_visited(move%fold)
        deallocate(move%new_energy_storage_fractional)
        if(allocated(move%coulombic)) then
            deallocate(move%coulombic)
        end if
    end subroutine

    subroutine chfrac_initialize_insert(move, sys)
        use rngmod

        class(change_fractional_parameter_move) :: move
        type(system), intent(inout) :: sys
        real(kind=mc_prec), dimension(3) :: inspos
        real(kind=mc_prec), dimension(3, 3) :: boundary
        integer :: i

        move%fold = sys%species(move%molecule_type)%frac
        move%currfrac = sys%species(move%molecule_type)%get_molecule(fractional_index)
        
        !Create "insert" molecule at a random position 
        move%insmol = sys%species(move%molecule_type)%get_prototype()
        !Choose a position and move there
        boundary = sys%box%get_boundary()
        inspos(1:3) = 0._mc_prec
        do i = 1, 3
            inspos(1:3) = inspos(1:3) + rng()*boundary(1:3, i)
        end do
        call move%insmol%translate(inspos)
        call rotate(move%insmol, sys%box)

        !Allocate temporary energy arrays
        if(sys%species(move%molecule_type)%ncharged > 0)then
            allocate(move%coulombic, source = sys%coulombic)
        end if

        move%new_energy_storage_fractional = reduced_dimensionality_energy_storage(sys%species(:)%storage_space)
        move%new_energy_storage_normal = reduced_dimensionality_energy_storage(sys%species(:)%storage_space)

    end subroutine

    subroutine chfrac_compute_probability_insert(move, sys, probability)
        use energy_module, only: epart_frac_insert

        class(change_fractional_parameter_move) :: move
        type(system), intent(inout) :: sys
        real(kind=mc_prec), intent(out) :: probability
        real(kind=mc_prec) :: dbias, indirect_energy

        call epart_frac_insert(sys, move%currfrac, move%insmol, move%molecule_type, &
                               move%coulombic, move%fold, move%fnew, indirect_energy, &
                               move%new_energy_storage_normal, move%new_energy_storage_fractional)
        move%dele = indirect_energy &
                  + move%new_energy_storage_fractional%array_sum(sys%species(:)%nmol) &
                  + move%new_energy_storage_normal%array_sum(sys%species(:)%nmol)&
                  - sys%energy_storage%array_sum(move%molecule_type, fractional_index, sys%species(:)%nmol)
                        
        !Get the change in bias factor
        dbias = sys%species(move%molecule_type)%bias%coupling_bias(move%fnew, move%fold)
        probability = move%exp_probability(dbias + sys%beta*(sys%species(move%molecule_type)%mu - move%dele) &
                    + log(sys%V/(sys%species(move%molecule_type)%nmol + 1)) )
    end subroutine

    subroutine chfrac_accept_insert(move, sys)
        class(change_fractional_parameter_move) :: move
        type(system), intent(inout) :: sys
        !Update interaction energies relative to newly made integer molecule

        call sys%change_nmol(move%molecule_type, 1)
        call sys%species(move%molecule_type)%set_molecule(move%insmol, fractional_index)
        call sys%species(move%molecule_type)%set_molecule(move%currfrac, sys%species(move%molecule_type)%nmol)

        call sys%set_frac(move%molecule_type, move%fnew)

        if(sys%species(move%molecule_type)%ncharged > 0)then
            call sys%update_energy_storage(move%molecule_type, sys%species(move%molecule_type)%nmol, &
                                             move%new_energy_storage_normal, move%coulombic)
        else
            call sys%update_energy_storage(move%molecule_type, sys%species(move%molecule_type)%nmol, &
                                             move%new_energy_storage_normal)
        end if
        call sys%update_energy_storage(move%molecule_type, 0, move%new_energy_storage_fractional)
        call sys%species(move%molecule_type)%bias%increment_bin_visited(move%fnew)

        call sys%update_running_difference(move%dele)
        deallocate(move%currfrac, move%insmol, move%new_energy_storage_normal, move%new_energy_storage_fractional)
    end subroutine

    subroutine chfrac_reject_insert(move, sys)
        class(change_fractional_parameter_move) :: move
        type(system), intent(inout) :: sys
        call sys%species(move%molecule_type)%bias%increment_bin_visited(move%fold)
        deallocate(move%currfrac, move%insmol, move%new_energy_storage_normal, move%new_energy_storage_fractional)
        if(allocated(move%coulombic)) then
            deallocate(move%coulombic)
        end if

    end subroutine

    subroutine chfrac_initialize_remove(move, sys)
        use rngmod
                
        class(change_fractional_parameter_move) :: move
        type(system), intent(inout) :: sys

        !If there are no molecules remaining, do nothing
        if(sys%species(move%molecule_type)%nmol == 0)then
            return
        end if
        
        move%fold = sys%species(move%molecule_type)%frac
        !Choose random new molecule to be the new fractional molecule
        move%newfr_index = int(rng()*sys%species(move%molecule_type)%nmol) + 1 

        move%currfrac = sys%species(move%molecule_type)%get_molecule(fractional_index)
        move%newfrac = sys%species(move%molecule_type)%get_molecule(move%newfr_index)
        if(sys%species(move%molecule_type)%ncharged > 0)then
            allocate(move%coulombic, source = sys%coulombic)
        end if


        move%new_energy_storage_fractional = reduced_dimensionality_energy_storage(sys%species(:)%storage_space)
    end subroutine

    subroutine chfrac_compute_probability_remove(move, sys, probability)
        use energy_module, only: epart_frac_remove

        class(change_fractional_parameter_move) :: move
        type(system), intent(inout) :: sys
        real(kind=mc_prec), intent(out) :: probability
        real(kind=mc_prec) :: dbias, indirect_energy

        if(sys%species(move%molecule_type)%nmol == 0)then
            probability = - 1.0 !Autorejects
            return
        end if

        call epart_frac_remove(sys, move%currfrac, move%newfrac, move%molecule_type, move%coulombic, move%fold, move%fnew, &
                               indirect_energy, move%new_energy_storage_fractional, move%newfr_index)
        
        move%dele = indirect_energy &
                  + move%new_energy_storage_fractional%array_sum(sys%species(:)%nmol) &
                  - sys%energy_storage%array_sum(move%molecule_type, move%newfr_index, sys%species(:)%nmol) &
                  - sys%energy_storage%array_sum(move%molecule_type, fractional_index, sys%species(:)%nmol)

        !Correct for double counting
        move%dele =  move%dele + sys%energy_storage%get_value(move%molecule_type, move%molecule_type, &
                                                                move%newfr_index, fractional_index) 
        
        dbias = sys%species(move%molecule_type)%bias%coupling_bias(move%fnew, move%fold)
        probability = move%exp_probability(dbias - sys%beta*(move%dele + sys%species(move%molecule_type)%mu)&
                            + log(sys%species(move%molecule_type)%nmol / sys%V) )
    end subroutine

    subroutine chfrac_accept_remove(move, sys)
        class(change_fractional_parameter_move) :: move
        type(system), intent(inout) :: sys
        
        !Overwrite the fractional molecule by the newly made fractional molecule
        call sys%species(move%molecule_type)%set_molecule(move%newfrac, fractional_index)
        !Store the computed new energies
        if(sys%species(move%molecule_type)%ncharged > 0)then
            call sys%update_energy_storage(move%molecule_type, fractional_index, &
                                           move%new_energy_storage_fractional,  move%coulombic)
        else
            call sys%update_energy_storage(move%molecule_type, fractional_index, move%new_energy_storage_fractional)
        end if
        !Now remove all traces of the old integer molecule (i.e. shift the energy storage matrices, and position arrays)
        !NB: changes nmol and totmol for us
        call sys%remove_molecule(move%molecule_type, move%newfr_index)

        !Update energy - environment
        call sys%set_frac(move%molecule_type, move%fnew)
        call sys%update_running_difference(move%dele)
        call sys%species(move%molecule_type)%bias%increment_bin_visited(move%fnew)
        deallocate(move%currfrac, move%newfrac, move%new_energy_storage_fractional)

    end subroutine

    subroutine set_limits(move, limits)
        class(change_fractional_parameter_move), intent(inout) :: move
        real(kind=mc_prec), dimension(:), intent(in) :: limits
        !Intentionally left empty
    end subroutine

    subroutine update_limits(move)
        class(change_fractional_parameter_move), intent(inout) :: move
        !Intentionally left empty
    end subroutine

    subroutine chfrac_reject_remove(move, sys)
        class(change_fractional_parameter_move) :: move
        type(system), intent(inout) :: sys

        if(sys%species(move%molecule_type)%nmol == 0)then
            !Nothing was done so do nothing
            return
        end if

        call sys%species(move%molecule_type)%bias%increment_bin_visited(move%fold)
        deallocate(move%currfrac, move%newfrac, move%new_energy_storage_fractional)
        if(allocated(move%coulombic)) then
            deallocate(move%coulombic)
        end if

    end subroutine

    subroutine print_success_rate(move, file_unit)
        class(change_fractional_parameter_move), intent(in) :: move
        integer, intent(in) :: file_unit
        integer :: i, j
        write(file_unit, '(a)') trim(adjustl(move%name))//":"
        associate(sizes => shape(move%successes))
        do i = 1, sizes(1)
            write(file_unit, '(4x, a, i3, a1)') "System ", i, ":"
            do j = 1, sizes(2)
                write(file_unit, '(8x, a, i3, a2, i7, a3 i7, a2, f6.2, 1x, a2)') "Species ", j, ": ", &
                    move%successes(i, j), " / ", move%attempts(i, j), &
                    " (", move%acceptance_rate(move%successes(i, j), move%attempts(i, j)), "%)"
            end do
        end do
        end associate
    end subroutine

    subroutine print_limits(move, file_unit)
        class(change_fractional_parameter_move), intent(in) :: move
        integer, intent(in) :: file_unit
        !Intentionally left empty
    end subroutine

end module

module identity_change_module
    use ewald_module, only: ewald_type
    use mc_move_class_simple_module, only: mc_move_class
    use molecule_module, only: single_molecule
    use precision_module, only: mc_prec
    use reduced_dimensionality_energy_storage_module, only: reduced_dimensionality_energy_storage
    use system_module, only: system
    implicit none
    private
    public :: identity_change_move

    type, extends(mc_move_class) :: identity_change_move
        type(single_molecule), allocatable :: insmol
        type(single_molecule), allocatable :: outmol
        class(ewald_type), allocatable :: coulombic
        type(reduced_dimensionality_energy_storage), allocatable :: new_energy_storage_in 
        integer :: ot, it, om, im, cur_sys
        real(kind=mc_prec) :: dele
        integer, dimension(:, :, :), allocatable :: attempts, successes
    contains

        procedure :: initialize_move
        procedure :: compute_probability
        procedure :: accept
        procedure :: reject
        procedure :: update_limits
        procedure :: set_limits
        procedure :: print_success_rate
        procedure :: print_limits
    end type

    interface identity_change_move
        module procedure new_identity_change_move
    end interface

contains

    function new_identity_change_move(systems) result(new_move)
        type(identity_change_move) :: new_move
        type(system), dimension(:), intent(in) :: systems

        new_move%name = "identity_change"

        allocate(new_move%attempts(size(systems), size(systems(1)%species), size(systems(1)%species)))
        allocate(new_move%successes(size(systems), size(systems(1)%species), size(systems(1)%species)))
        new_move%attempts = 0
        new_move%successes = 0

    end function

    subroutine initialize_move(move, systems)
        use rngmod
        use random_rotations_module, only: rotate

        class(identity_change_move) :: move
        type(system), dimension(:), intent(inout) :: systems
        integer :: number_of_fluctuating_components
        integer :: i
        real(kind=mc_prec), dimension(3) :: dr

        move%cur_sys = int(rng()*size(systems)) + 1
        associate(sys => systems(move%cur_sys))
        number_of_fluctuating_components = count(sys%species%fluctuating)
        if( number_of_fluctuating_components < 2 ) return !Cannot swap types as only one molecule type is fluctuating
        move%dele = 0._mc_prec
        !Determine type to swap out
        i = int(rng()*number_of_fluctuating_components) + 1
        move%ot = 0
        do while(i > 0)
             move%ot = move%ot + 1
             if(sys%species(move%ot)%fluctuating) i = i - 1
        end do
        if(sys%species(move%ot)%nmol == 0)return !Cannot swap out, exit without doing anything
        !Determine type to swap in
        i = int(rng()*(number_of_fluctuating_components - 1 )) + 1
        move%it = 0
        do while(i > 0)
             move%it = move%it + 1
             if(sys%species(move%it)%fluctuating .and. move%it .ne. move%ot) i = i - 1
        end do

        move%attempts(move%cur_sys, move%it, move%ot) = move%attempts(move%cur_sys, move%it, move%ot) + 1

        if(sys%species(move%ot)%ncharged > 0.or.sys%species(move%it)%ncharged > 0)then
            allocate(move%coulombic, source = sys%coulombic)
        end if

        !Determine which molecule to swap out
        move%im = sys%species(move%it)%nmol + 1
        move%om = int(sys%species(move%ot)%nmol*rng()) + 1
        
        !Create molecule to swap in and place it at the same position as the original molecule, but with a random orientation
        move%insmol = sys%species(move%it)%get_prototype()
        move%outmol = sys%species(move%ot)%get_molecule(move%om)
        call rotate(move%insmol, sys%box)
        
        dr(1) = move%insmol%X(1) - sys%species(move%ot)%atom(1)%pos%X(move%om)
        dr(2) = move%insmol%Y(1) - sys%species(move%ot)%atom(1)%pos%Y(move%om)
        dr(3) = move%insmol%Z(1) - sys%species(move%ot)%atom(1)%pos%Z(move%om)
        call move%insmol%translate( -dr)
        error stop "Identity change is not implemented"
        move%new_energy_storage_in = reduced_dimensionality_energy_storage(sys%species(:)%storage_space)
        end associate
    end subroutine

    subroutine compute_probability(move, systems, probability)
        use energy_module, only: idchange_ener
        
        class(identity_change_move) :: move
        type(system), dimension(:), intent(inout) :: systems
        real(kind=mc_prec), intent(out) :: probability
        real(kind=mc_prec) :: indirect_energy
        
        associate(sys => systems(move%cur_sys))

        call idchange_ener(sys, move%outmol, move%insmol, move%om, move%im, move%ot, move%it, &
                           move%coulombic, indirect_energy, &
                           move%new_energy_storage_in)
        move%dele = indirect_energy &
                  + move%new_energy_storage_in%array_sum(sys%species(:)%nmol) &
                  - sys%energy_storage%array_sum(move%ot, move%om, sys%species(:)%nmol)

        probability = move%exp_probability( -sys%beta*(move%dele - sys%species(move%it)%mu + sys%species(move%ot)%mu) &
                     + log(real(sys%species(move%ot)%nmol)/(sys%species(move%it)%nmol + 1._mc_prec)) )

        end associate
    end subroutine

    subroutine accept(move, systems)
        class(identity_change_move) :: move
        type(system), dimension(:), intent(inout) :: systems

        move%successes(move%cur_sys, move%it, move%ot) = move%successes(move%cur_sys, move%it, move%ot) + 1

        associate(sys => systems(move%cur_sys))

        call sys%update_running_difference(move%dele)

        call sys%change_nmol(move%it, 1)
        call sys%species(move%it)%set_molecule(move%insmol, sys%species(move%it)%nmol)

        !Update energy storage
        if(sys%species(move%ot)%ncharged > 0.or.sys%species(move%it)%ncharged > 0)then
            call sys%update_energy_storage(move%it, move%im, move%new_energy_storage_in, move%coulombic)
        else
            call sys%update_energy_storage(move%it, move%im, move%new_energy_storage_in)
        end if
        !Delete the old molecule
        call sys%remove_molecule(move%ot, move%om)
        !TODO: Some logic needed to get rid of old energy from total energy

        deallocate(move%insmol, move%outmol, move%new_energy_storage_in) 

        end associate
    end subroutine

    subroutine reject(move, systems)
        class(identity_change_move) :: move
        type(system), dimension(:), intent(inout) :: systems
        
        deallocate(move%insmol, move%outmol, move%new_energy_storage_in, move%coulombic) 
    end subroutine

    subroutine set_limits(move, limits)
        class(identity_change_move), intent(inout) :: move
        real(kind=mc_prec), dimension(:), intent(in) :: limits
        !Intentionally left empty
    end subroutine

    subroutine update_limits(move)
        class(identity_change_move), intent(inout) :: move
        !Intentionally left empty
    end subroutine

    subroutine print_success_rate(move, file_unit)
        class(identity_change_move), intent(in) :: move
        integer, intent(in) :: file_unit
        integer :: i, j, k
        write(file_unit, '(a)') trim(adjustl(move%name))//":"
        associate(sizes => shape(move%successes))
        do i = 1, sizes(1)
            write(file_unit, '(4x, a, i3, a1)') "System ", i, ":"
            do j = 1, sizes(2)
                do k = 1, sizes(3)
                    write(file_unit, '(8x, a, i3, a4, i3, a2, i7, a3 i7, a2, f6.2, 1x, a2)') "Species ", j, " to ", k, ": ", &
                            move%successes(i, j, k), " / ", move%attempts(i, j, k), &
                            " (", move%acceptance_rate(move%successes(i, j, k), move%attempts(i, j, k)), "%)"
                end do
            end do
        end do
        end associate
    end subroutine

    subroutine print_limits(move, file_unit)
        class(identity_change_move), intent(in) :: move
        integer, intent(in) :: file_unit
        !Intentionally left empty
    end subroutine

end module

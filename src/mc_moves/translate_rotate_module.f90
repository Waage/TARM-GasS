module translate_rotate_module
    use ewald_module, only: ewald_type
    use error_handling_module, only: warn_missing
    use mc_move_class_simple_module, only: mc_move_class
    use molecule_module, only: single_molecule
    use precision_module, only: mc_prec
    use reduced_dimensionality_energy_storage_module, only: reduced_dimensionality_energy_storage
    use system_module, only: system
    implicit none
    private
    public :: translate_rotate_move

    type, extends(mc_move_class) :: translate_rotate_move
        logical :: is_translate

        real(kind=mc_prec) :: dele, delta_fourier_ewald_energy
        integer :: molType, molindex, cur_sys
        type(single_molecule), allocatable :: newpos
        class(ewald_type), allocatable :: coulombic
        type(reduced_dimensionality_energy_storage), allocatable :: new_energy_storage
        integer, dimension(:, :, :), allocatable :: attempts, successes

        real(kind=mc_prec), dimension(:, :, :), allocatable :: maximal_change

    contains

        procedure :: initialize_move
        procedure :: no_rate_set
        procedure :: compute_probability
        procedure :: accept
        procedure :: reject
        procedure :: update_limits
        procedure :: set_limits
        procedure :: print_success_rate
        procedure :: print_limits
    end type

    interface translate_rotate_move !lol fortran
        module procedure new_translate_rotate_move
    end interface

contains

    function new_translate_rotate_move(systems) result(new_move)
        type(translate_rotate_move) :: new_move
        type(system), dimension(:), intent(in) :: systems

        new_move%name = "translate_rotate"

        allocate(new_move%attempts(size(systems), size(systems(1)%species), 2))
        allocate(new_move%successes(size(systems), size(systems(1)%species), 2))
        allocate(new_move%maximal_change(size(systems), size(systems(1)%species), 2))
        new_move%attempts = 0
        new_move%successes = 0
        new_move%maximal_change = 0.1_mc_prec
    end function

    subroutine no_rate_set(move)
        class(translate_rotate_move) :: move
        call warn_missing("translate_rotate rate", "set to 1000, which may or may not be completely terrible")
    end subroutine

    subroutine initialize_move(move, systems)
        use rngmod
        use random_rotations_module, only: rotate

        class(translate_rotate_move) :: move
        type(system), dimension(:), intent(inout) :: systems
        integer :: i
        real(kind=mc_prec), dimension(3) :: dr
        integer :: total_all_systems

        !Select a random molecule from any system with equal probability
        !Determine the total number of molecules
        total_all_systems = sum(systems%totmol)
        
        !Choose one of those molecules
        move%molindex = int(rng()*total_all_systems) + 1
        move%cur_sys = 1
        !Determine which system it is in
        do while (move%molindex > systems(move%cur_sys)%totmol)
            move%molindex = move%molindex - systems(move%cur_sys)%totmol
            move%cur_sys = move%cur_sys + 1
        end do
        associate(sys => systems(move%cur_sys))
        !Choose the molecule by going thorugh each species
        !Make sure to count fractional molecules
        i = 0
        do while(move%molindex > 0)
            i = i + 1
            move%molindex = move%molindex - sys%species(i)%nmol
            if(sys%species(i)%fluctuating) move%molindex = move%molindex - 1
        end do
        move%molType = i

        !Species was chosen, now choose the specific molecule
        !Keep in mind that a potential fractional molecule is located in index 0!
        move%molindex = move%molindex + sys%species(move%molType)%nmol    
        !Copy the chosen molecule
        move%newpos = sys%species(move%molType)%get_molecule(move%molindex)

        !Allocate arrays for storing the direct interactions between molecules
        move%new_energy_storage = reduced_dimensionality_energy_storage(sys%species(:)%storage_space)

        !Prepare fourier space ewald summation
        move%delta_fourier_ewald_energy = 0._mc_prec
        if(sys%species(move%molType)%ncharged > 0)then
            allocate(move%coulombic, source = sys%coulombic)
        end if
        !Choose between translation or rotation
        if(rng() < .5_mc_prec.or.sys%species(move%molType)%rot_type == 1)then

            move%attempts(move%cur_sys, move%molType, 1) = move%attempts(move%cur_sys, move%molType, 1) + 1
            move%is_translate = .true.
            !Choose random direction, distance dr
            dr(1) = .5_mc_prec - rng()
            dr(2) = .5_mc_prec - rng()
            dr(3) = .5_mc_prec - rng()
            dr = move%maximal_change(move%cur_sys, move%molType, 1)*dr
            call move%newpos%translate(dr)
            call move%newpos%reinsert(sys%box)
        else

            move%attempts(move%cur_sys, move%molType, 2) = move%attempts(move%cur_sys, move%molType, 2) + 1
            move%is_translate = .false.
            call rotate(move%newpos, sys%box, move%maximal_change(move%cur_sys, move%molType, 2))

        end if
        end associate
    end subroutine

    subroutine compute_probability(move, systems, probability)
        use energy_module, only: epart_pos   
        class(translate_rotate_move) :: move
        type(system), dimension(:), intent(inout) :: systems
        real(kind=mc_prec), intent(out) :: probability

        associate(sys => systems(move%cur_sys))
        call epart_pos(sys, move%molindex, move%molType, move%coulombic, move%newpos, &
                        move%delta_fourier_ewald_energy, move%new_energy_storage)
        
        move%dele = move%delta_fourier_ewald_energy &
                  + move%new_energy_storage%array_sum(sys%species(:)%nmol) &
                  - sys%energy_storage%array_sum(move%molType, move%molindex, sys%species(:)%nmol)

        probability = move%exp_probability( -sys%beta*move%dele)
        end associate
    end subroutine

    subroutine accept(move, systems)
        class(translate_rotate_move) :: move
        type(system), dimension(:), intent(inout) :: systems

        associate(sys => systems(move%cur_sys))
        if(move%is_translate)then
            move%successes(move%cur_sys, move%molType, 1) = move%successes(move%cur_sys, move%molType, 1) + 1
        else
            move%successes(move%cur_sys, move%molType, 2) = move%successes(move%cur_sys, move%molType, 2) + 1
        end if
        call sys%species(move%molType)%set_molecule(move%newpos, move%molindex)
        
        if(sys%species(move%molType)%ncharged > 0)then
            call sys%update_energy_storage(move%molType, move%molindex, move%new_energy_storage, move%coulombic)
        else
            call sys%update_energy_storage(move%molType, move%molindex, move%new_energy_storage)
        end if

        call sys%update_running_difference(move%dele)

        deallocate(move%newpos, move%new_energy_storage)
        end associate
    end subroutine

    subroutine reject(move, systems)
        class(translate_rotate_move) :: move
        type(system), dimension(:), intent(inout) :: systems
        if(allocated(move%coulombic)) then
            deallocate(move%coulombic)
        end if
        deallocate(move%newpos, move%new_energy_storage)
    end subroutine

    subroutine set_limits(move, limits)
        class(translate_rotate_move), intent(inout) :: move
        real(kind=mc_prec), dimension(:), intent(in) :: limits

        move%maximal_change = reshape(limits, shape(move%maximal_change))

    end subroutine

    subroutine update_limits(move)
        class(translate_rotate_move), intent(inout) :: move
        integer i, j

        associate(sizes => shape(move%successes))
        do i = 1, sizes(1)
            do j = 1, sizes(2)
                if(move%attempts(i, j, 1) > 100)then
                    if(move%successes(i, j, 1) > (move%attempts(i, j, 1)/2 + move%attempts(i, j, 1)/10))then
                        move%maximal_change(i, j, 1) = move%maximal_change(i, j, 1)*1.1_mc_prec
                    else if(move%successes(i, j, 1) < (move%attempts(i, j, 1)/2 - move%attempts(i, j, 1)/10))then
                        move%maximal_change(i, j, 1) = move%maximal_change(i, j, 1)*.9_mc_prec
                    end if
                    move%attempts(i, j, 1) = 0
                    move%successes(i, j, 1) = 0
                end if
                if(move%attempts(i, j, 2) > 100)then
                    if(move%successes(i, j, 2) > (move%attempts(i, j, 2)/2 + move%attempts(i, j, 2)/10))then
                        move%maximal_change(i, j, 2)=move%maximal_change(i, j, 2)*1.1_mc_prec
                    else if(move%successes(i, j, 2) < (move%attempts(i, j, 2)/2 - move%attempts(i, j, 2)/10))then
                        move%maximal_change(i, j, 2)=move%maximal_change(i, j, 2)*.9_mc_prec
                    end if
                move%attempts(i, j, 2) = 0
                move%successes(i, j, 2) = 0
                end if
            end do
        end do
        end associate

    end subroutine

    subroutine print_success_rate(move, file_unit)
        class(translate_rotate_move), intent(in) :: move
        integer, intent(in) :: file_unit
        integer :: i, j
        associate(sizes => shape(move%successes))
        write(file_unit, '(a)') "Translation:"
        do i = 1, sizes(1)
            write(file_unit, '(4x, a, i3, a1)') "System ", i, ":"
            do j = 1, sizes(2)
                write(file_unit, '(8x, a, i3, a2, i7, a3 i7, a2, f6.2, 1x, a2)') "Species ", j, ": ", &
                    move%successes(i, j, 1), " / ", move%attempts(i, j, 1), &
                    " (", move%acceptance_rate(move%successes(i, j, 1), move%attempts(i, j, 1)), "%)"
            end do
        end do
        write(file_unit, '(a)') "Rotation:"
        do i = 1, sizes(1)
            write(file_unit, '(4x, a, i3, a1)') "System ", i, ":"
            do j = 1, sizes(2)
                write(file_unit, '(8x, a, i3, a2, i7, a3 i7, a2, f6.2, 1x, a2)') "Species ", j, ": ", &
                    move%successes(i, j, 2), " / ", move%attempts(i, j, 2), &
                    " (", move%acceptance_rate(move%successes(i, j, 2), move%attempts(i, j, 2)), "%)"
            end do
        end do
        end associate
    end subroutine

    subroutine print_limits(move, file_unit)
        class(translate_rotate_move), intent(in) :: move
        integer, intent(in) :: file_unit

        write(file_unit, *) move%name, move%maximal_change

    end subroutine

end module

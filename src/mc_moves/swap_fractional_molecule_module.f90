module swap_fractional_molecule_module
    use ewald_module, only: ewald_type
    use mc_move_class_simple_module, only: mc_move_class
    use molecule_module, only: single_molecule
    use parameters_module, only: fractional_index
    use precision_module, only: mc_prec
    use reduced_dimensionality_energy_storage_module, only: reduced_dimensionality_energy_storage
    use system_module, only: system
    
    implicit none
    private
    public :: swap_fractional_molecule_move

    type, extends(mc_move_class) :: swap_fractional_molecule_move
        integer :: newfr_index, frac_index
        real(kind=mc_prec) :: dele
        integer :: molecule_type, cur_sys
        class(ewald_type), allocatable :: coulombic
        type(reduced_dimensionality_energy_storage), allocatable :: new_energy_storage_frac, new_energy_storage_newfr 
        type(single_molecule), allocatable :: currfrac, newfrac    
        integer, dimension(:, :), allocatable :: attempts, successes
    contains

        procedure :: initialize_move
        procedure :: compute_probability
        procedure :: accept
        procedure :: reject
        procedure :: update_limits
        procedure :: set_limits
        procedure :: print_success_rate
        procedure :: print_limits
    end type

    interface swap_fractional_molecule_move
        module procedure new_swap_fractional_molecule_move
    end interface
        

contains

    function new_swap_fractional_molecule_move(systems) result(new_move)
        type(swap_fractional_molecule_move) :: new_move
        type(system), dimension(:), intent(in) :: systems
        
        new_move%name = "swap_fractional_molecule"
        
        allocate(new_move%attempts(size(systems), size(systems(1)%species)))
        allocate(new_move%successes(size(systems), size(systems(1)%species)))
        new_move%attempts = 0
        new_move%successes = 0
    
    end function

    subroutine initialize_move(move, systems)
    
        !Swap which molecule of a certain type is integer/fractional internally in a box
        !Fractional parameter lambda is conserved
        use rngmod
        class(swap_fractional_molecule_move) :: move
        type(system), dimension(:), intent(inout) :: systems
        integer :: number_of_fluctuating_components
        integer :: i

        move%cur_sys = int(rng()*size(systems)) + 1

        do while(count(systems(move%cur_sys)%species%fluctuating) < 1)
            move%cur_sys = int(rng()*size(systems)) + 1
        end do
        associate(sys => systems(move%cur_sys))
        number_of_fluctuating_components = count(sys%species%fluctuating)

        move%dele = 0._mc_prec
        i = int( rng()*number_of_fluctuating_components ) + 1
        !Choose which type
        move%molecule_type = 0
        do while(i > 0)
            move%molecule_type = move%molecule_type + 1
            if(sys%species(move%molecule_type)%fluctuating)i = i - 1
        end do
        
        move%attempts(move%cur_sys, move%molecule_type) = move%attempts(move%cur_sys, move%molecule_type) + 1

        if(sys%species(move%molecule_type)%nmol == 0)return !Exit if there is only the fractional molecule

        !Choose the integer molecule
        move%newfr_index = int(rng()*sys%species(move%molecule_type)%nmol) + 1 
        move%currfrac = sys%species(move%molecule_type)%get_molecule(fractional_index)
        move%newfrac =  sys%species(move%molecule_type)%get_molecule(move%newfr_index)

        if(sys%species(move%molecule_type)%ncharged > 0)then
            allocate(move%coulombic, source = sys%coulombic)
        end if

        move%new_energy_storage_frac = reduced_dimensionality_energy_storage(sys%species(:)%storage_space)
        move%new_energy_storage_newfr = reduced_dimensionality_energy_storage(sys%species(:)%storage_space)
        end associate
    end subroutine

    subroutine compute_probability(move, systems, probability)
        use energy_module, only: swap_frac_ener

        class(swap_fractional_molecule_move) :: move
        type(system), dimension(:), intent(inout) :: systems
        real(kind=mc_prec), intent(out) :: probability
        real(kind=mc_prec)  :: delta_fourier_ewald_energy

        associate(sys => systems(move%cur_sys))
        if(sys%species(move%molecule_type)%nmol == 0)then
            probability = - 1
            return 
        end if
        call swap_frac_ener(sys, move%currfrac, move%newfrac, move%molecule_type, move%newfr_index, move%coulombic, &
                             delta_fourier_ewald_energy, move%new_energy_storage_frac, move%new_energy_storage_newfr)

        move%dele = delta_fourier_ewald_energy &
                  + move%new_energy_storage_frac%array_sum(sys%species(:)%nmol) &
                  - sys%energy_storage%array_sum(move%molecule_type, fractional_index, sys%species(:)%nmol) &
                  + move%new_energy_storage_newfr%array_sum(sys%species(:)%nmol) &
                  - sys%energy_storage%array_sum(move%molecule_type, move%newfr_index, sys%species(:)%nmol)
        
        !Correct for double counting
        move%dele = move%dele &
         - move%new_energy_storage_frac%get_value(move%molecule_type, move%newfr_index)&
         + sys%energy_storage%get_value(move%molecule_type, move%molecule_type, move%newfr_index, fractional_index)

        probability = move%exp_probability( -sys%beta*(move%dele))

        end associate
    end subroutine

    subroutine accept(move, systems)

        class(swap_fractional_molecule_move) :: move
        type(system), dimension(:), intent(inout) :: systems


        associate(sys => systems(move%cur_sys))

        !nmol == 0 means we autorejected and no allocations were made
        if(sys%species(move%molecule_type)%nmol == 0)return 
        
        move%successes(move%cur_sys, move%molecule_type) = move%successes(move%cur_sys, move%molecule_type) + 1

        if(sys%species(move%molecule_type)%ncharged > 0)then
            call sys%update_energy_storage(move%molecule_type, move%newfr_index, move%new_energy_storage_newfr, move%coulombic)
        else
            call sys%update_energy_storage(move%molecule_type, move%newfr_index, move%new_energy_storage_newfr)
        end if
        call sys%update_energy_storage(move%molecule_type, fractional_index, move%new_energy_storage_frac)
        call sys%species(move%molecule_type)%set_molecule(move%currfrac, move%newfr_index)
        call sys%species(move%molecule_type)%set_molecule(move%newfrac, fractional_index)
        call sys%update_running_difference(move%dele)
    
        deallocate(move%new_energy_storage_frac, move%new_energy_storage_newfr, move%currfrac, move%newfrac)
        end associate
    end subroutine

    subroutine reject(move, systems)
        class(swap_fractional_molecule_move) :: move
        type(system), dimension(:), intent(inout) :: systems
        if(systems(move%cur_sys)%species(move%molecule_type)%nmol == 0)return !nmol == 0 means we autorejected and no allocations were made
        deallocate(move%new_energy_storage_frac, move%new_energy_storage_newfr, move%currfrac, move%newfrac)
        if(allocated(move%coulombic))then
            deallocate(move%coulombic)
        end if
    end subroutine

    subroutine set_limits(move, limits)
        class(swap_fractional_molecule_move), intent(inout) :: move
        real(kind=mc_prec), dimension(:), intent(in) :: limits
        !Intentionally left empty
    end subroutine

    subroutine update_limits(move)
        class(swap_fractional_molecule_move), intent(inout) :: move
        !Intentionally left empty
    end subroutine

    subroutine print_success_rate(move, file_unit)
        class(swap_fractional_molecule_move), intent(in) :: move
        integer, intent(in) :: file_unit
        integer :: i, j
        write(file_unit, '(a)') trim(adjustl(move%name))//":"
        associate(sizes => shape(move%successes))
        do i = 1, sizes(1)
            write(file_unit, '(4x, a, i3, a1)') "System ", i, ":"
            do j = 1, sizes(2)
                write(file_unit, '(8x, a, i3, a2, i7, a3 i7, a2, f6.2, 1x, a2)') "Species ", j, ": ", &
                    move%successes(i, j), " / ", move%attempts(i, j), &
                    " (", move%acceptance_rate(move%successes(i, j), move%attempts(i, j)), "%)"
            end do
        end do
        end associate
    end subroutine

    subroutine print_limits(move, file_unit)
        class(swap_fractional_molecule_move), intent(in) :: move
        integer, intent(in) :: file_unit
        !Intentionally left empty
    end subroutine

end module

module mc_move_class_simple_module
    use precision_module, only: mc_prec
    use system_module, only: system
    implicit none
    private
    public :: mc_move_class

    type, abstract :: mc_move_class
    
        character(50) :: name
        real :: time_spent = 0
        integer :: rate = 0
    
    contains
        
        procedure :: execute
        procedure(initialize_move), deferred :: initialize_move
        procedure(compute_function), deferred :: compute_probability
        procedure(accept_function), deferred :: accept
        procedure(reject_function), deferred :: reject
        procedure(update_limits), deferred :: update_limits
        procedure(set_limits), deferred :: set_limits
        procedure(print_success_rate), deferred :: print_success_rate
        procedure(print_limits), deferred :: print_limits
        procedure :: print_time
        procedure :: get_rate
        procedure :: no_rate_set
        procedure, nopass :: acceptance_rate
        procedure, nopass :: exp_probability

    end type    

    abstract interface  
        subroutine initialize_move(move, systems)
            use system_module, only: system
            import mc_move_class
            class(mc_move_class) :: move
            type(system), dimension(:), intent(inout) :: systems
        end subroutine
    end interface

    abstract interface  
        subroutine compute_function(move, systems, probability)
            use precision_module, only: mc_prec
            use system_module, only: system
            import mc_move_class
            class(mc_move_class) :: move
            type(system), dimension(:), intent(inout) :: systems
            real(kind=mc_prec), intent(out) :: probability
        end subroutine
    end interface

    abstract interface  
        subroutine accept_function(move, systems)
            use system_module, only: system
            import mc_move_class
            class(mc_move_class) :: move
            type(system), dimension(:), intent(inout) :: systems
        end subroutine
    end interface

    abstract interface  
        subroutine reject_function(move, systems)
            use system_module, only: system
            import mc_move_class
            class(mc_move_class) :: move
            type(system), dimension(:), intent(inout) :: systems
        end subroutine
    end interface

    abstract interface  
        subroutine update_limits(move)
            import mc_move_class
            class(mc_move_class), intent(inout) :: move
        end subroutine
    end interface

    abstract interface  
        subroutine set_limits(move, limits)
            use precision_module, only: mc_prec
            import mc_move_class
            class(mc_move_class), intent(inout) :: move
            real(kind=mc_prec), dimension(:), intent(in) :: limits
        end subroutine
    end interface

    abstract interface  
        subroutine print_success_rate(move, file_unit)
            import mc_move_class
            class(mc_move_class), intent(in) :: move
            integer, intent(in) :: file_unit
        end subroutine
    end interface
    
    abstract interface  
        subroutine print_limits(move, file_unit)
            import mc_move_class
            class(mc_move_class), intent(in) :: move
            integer, intent(in) :: file_unit
        end subroutine
    end interface

contains

    subroutine execute(move, systems)
        use rngmod
        class(mc_move_class), intent(inout) :: move
        type(system), dimension(:), intent(inout) :: systems
        real(kind=mc_prec) :: probability
        real :: t_start, t_end

        call cpu_time(t_start)
        call move%initialize_move(systems)
        call move%compute_probability(systems, probability)
        if (rng() < probability) then
            call move%accept(systems)
        else
            call move%reject(systems)
        endif

        call cpu_time(t_end)
        move%time_spent = t_end - t_start + move%time_spent
    
    end subroutine

    subroutine print_time(move, file_unit)
        class(mc_move_class), intent(inout) :: move
        integer, intent(in) :: file_unit
        write(file_unit, *) trim(adjustl(move%name))//":", move%time_spent
    
    end subroutine

    subroutine no_rate_set(move)   
        class(mc_move_class) :: move
        !Default: We don't care that no rate has been set except for special cases, 
        !so this function does nothing unless it is overridden
    end subroutine
    
    elemental function get_rate(move) result(rate)
        !returns the chance of calling this move. Overridable
        class(mc_move_class), intent(in) :: move
        integer :: rate

        rate = move%rate
    
        return
    end function

    pure real(kind=mc_prec) function acceptance_rate(successes, attempts)
        integer, intent(in) :: attempts, successes

        if(attempts == 0) then
            acceptance_rate = 100.0000
        else
            acceptance_rate = 100.0000*real(successes)/attempts
        end if

    end function

    pure real(kind=mc_prec) function exp_probability(argument)
        !function to avoid overflow/underflow issues when computing probability
        real(kind=mc_prec), intent(in) :: argument
        
        if (argument < - 100._mc_prec) then
            exp_probability = - 1._mc_prec ! exp( -100)~10^ - 40, so we would always reject this anyway
        else if (argument > 10._mc_prec) then
            exp_probability = 2._mc_prec  !Always accept positive values.
        else
            exp_probability = exp(argument)
        end if

    end function

end module mc_move_class_simple_module

program runMC
    !Main program for running MC - simulation
    use argument_parser_module, only: parse_command_line
    use precision_module, only: mc_prec
    use rngmod, only: rng, set_seed
    use system_module, only: system
    use energy_module, only:  etot_cfc, ener_print_times
    use sampling_module, only: write_energies, write_volume, write_molecule_numbers
    use simulation_top_level_module, only: simulation_top_level_container
    use initialize_module, only: simulator_initialize_from_file
    use xyz_file_module, only: write_xyz_file
    use gro_file_module, only: write_gro_file
    use dlpoly_config_module, only: write_dlpoly_configuration_to_file
    implicit none

    !Files handles
    integer :: outfile, endfile, nmolfile, fracfile, biasfile, volfile, enerfile, simulation_input_unit

    real(kind=mc_prec) :: energy
    integer :: i, j, k
    logical :: bias_done_equilibrating
    real :: start, finish
    integer :: samplefreq = 1, savefreq = 1000
    type(simulation_top_level_container) :: top
    character(100) :: number
    character(256) :: input_file_name, input_directory, output_directory

    call cpu_time(start)

    call parse_command_line(input_file_name, input_directory, output_directory)

    open(newunit=outfile, file=trim(output_directory)//'output', status='replace')
    write(outfile, *)'Getting system'

    open(newunit=simulation_input_unit, file=input_file_name, status='old')
    call simulator_initialize_from_file(simulation_input_unit, input_directory, top)
    close(simulation_input_unit)

    write(outfile, *)'seed:', top%seed
    call set_seed(top%seed)
    do i = 1, 1000 !Warming up generator (this may or may not be religious)
         energy = rng()
    end do

    energy = 0._mc_prec
    do i = 1, size(top%systems)
        call etot_cfc(top%systems(i), top%systems(i)%species, top%systems(i)%coulombic, &
                                    top%systems(i)%total_energy, top%systems(i)%energy_storage)
        write(outfile, *) 'TOTAL ENERGY START system ', i, ':', top%systems(i)%total_energy, 'kcal/mol'
        energy = energy + top%systems(i)%total_energy
    end do
    write(outfile, *) 'TOTAL TOTAL ENERGY START :', energy, 'kcal/mol'

    !Equilibration starts here!
    call save_configurations(top%systems, trim(output_directory)//"StartCON", "xyz")

    do i = 1, top%n_equil
        do j = 1, top%moves%moves_per_cycle()
            call top%moves%mc_move(top%systems)
        end do
        do j =1, size(top%systems)
            !Change movement limits
            call top%moves%update_move_limits()
        end do
        call update_biases(top%systems)
        if(mod(i, savefreq) == 0)then
            call save_restart_information(top%systems, "Equilibration")
        end if
        if(.not.all(top%systems(:)%is_valid())) error stop "Systems are in an invalid state! Aborting"
    end do

    write(outfile, *) 'Finished equilibrating'
    write(outfile, *) 'Number of equilibration steps:', i
    call cpu_time(finish)
    write(outfile, *) "Time since simulation start = ", finish - start, " seconds."

    bias_done_equilibrating = .true. 
    do j = 1, size(top%systems)
        bias_done_equilibrating = bias_done_equilibrating .and. all(top%systems(j)%species(:)%bias%has_reached_finest_scale())
    end do
    if(.not.bias_done_equilibrating) then
        write(outfile, *) "Warning: Not all biases finished equilibrating."
        write(outfile, *) "Warning: Consider rerunning with more equilibration cycles!"
    end if
    
    open(newunit=nmolfile, file=trim(output_directory)//'molecule_numbers', status = 'replace')
    open(newunit=fracfile, file=trim(output_directory)//'fractional_parameter', status = 'replace')
    open(newunit=biasfile, file=trim(output_directory)//'biases', status = 'replace')
    open(newunit=volfile, file=trim(output_directory)//'volume', status = 'replace')
    open(newunit=enerfile, file=trim(output_directory)//'energies', status = 'replace')
    do i = 1, size(top%systems)
        write(outfile, *) 'Energy:', top%systems(i)%total_energy
        call top%systems(i)%species(:)%bias%toggle_equilibrating(.false.)
    end do

    !Production run starts here!
    do i = 1, top%n_prod
        do j = 1, top%moves%moves_per_cycle()
            call top%moves%mc_move(top%systems)
        end do
        if(mod(i, samplefreq) == 0)then
            do j = 1, size(top%systems)
                call write_molecule_numbers(top%systems(j), nmolfile)
                call write_volume(top%systems(j), volfile)
                call write_energies(top%systems(j), enerfile)
            end do
        end if
        if(mod(i, savefreq) == 0)then
            call save_restart_information(top%systems, "Production")
        end if
        if(.not.all(top%systems(:)%is_valid())) error stop "Systems are in an invalid state! Aborting"
    end do
    
    !Finish up, write some final information re: energy convergence, time spent, etc
    energy = 0._mc_prec
    write(outfile, *) 'TOTAL ENERGY END:', top%systems(:)%total_energy, 'kcal/mol'
    do i = 1, size(top%systems)
        call etot_cfc(top%systems(i), top%systems(i)%species, top%systems(i)%coulombic, &
                                    top%systems(i)%total_energy, top%systems(i)%energy_storage)
        energy = energy + top%systems(i)%total_energy
    end do
    write(outfile, *) 'TOTAL ENERGY CONSISTENCY CHECK:', top%systems(:)%total_energy, 'kcal/mol'
    energy = sum(top%systems(:)%total_energy)
    write(outfile, *) 'TOTAL TOTAL ENERGY END:', energy, 'kcal/mol'
    close(nmolfile)
    close(fracfile)
    close(biasfile)
    close(volfile)
    close(enerfile)
    do i = 1, size(top%systems)
        call top%moves%movelim_debug(outfile)
    end do
    do i = 1, size(top%systems)
        write(number, '(i10)') i
        number = adjustl(number)
        do k = 1, size(top%systems(i)%species)
            call top%systems(i)%species(k)%assemble(top%systems(i)%box)
        end do  
        !Don't need to reinsert after this, since we want assembled molecules for all the end configs
    end do

    call save_configurations(top%systems, trim(output_directory)//"EndCON", "xyz")
    call save_configurations(top%systems, trim(output_directory)//"REVCON", "dlpolyconf")

    open(newunit=endfile, file=trim(output_directory)//'fracress', status='replace')
    do i = 1, size(top%systems)
        write(endfile, *)top%systems(i)%species(:)%frac
        write(endfile, *)
    end do
    close(endfile)
    call cpu_time(finish)
    write(outfile, *) 'fractions:'
    do i = 1, size(top%systems)
        write(outfile, *) "system", i
        write(outfile, *) top%systems(i)%species(:)%frac
    end do
    write(outfile, *) "Time = ", finish - start, " seconds."
    call top%moves%print_times(outfile)
    call ener_print_times(outfile)
    call top%moves%print_success_rate(outfile)
    close(outfile)
    deallocate(top%systems)

contains

    subroutine save_configurations(systems, file_name, suffix)
        type(system), dimension(:), intent(in) :: systems
        character(*), intent(in) :: file_name, suffix
        character(10) :: number
        character(100) :: revivefile

        integer :: i, unitNr

        do i = 1, size(systems)
            write(number, '(i10)') i
            number = adjustl(number)
            revivefile = file_name//"_"//trim(number)//"."//suffix
            open(newunit=unitNr, file=revivefile, status='replace')

            select case(suffix)
            case("dlpolyconf")
                call write_dlpoly_configuration_to_file(unitNr, systems(i))
            case("xyz")
                call write_xyz_file(unitNr, systems(i))
            case("gro")
                call write_gro_file(unitNr, systems(i))
            case default
                !Silently fail to mess with people
                return
            end select
            close(unitNr)
        end do

    end subroutine

    subroutine save_restart_information(systems, file_name)
        type(system), dimension(:), intent(in) :: systems
        character(*), intent(in) :: file_name
        character(10) :: number
        character(100) :: revivefile

        integer :: i, j, unitNr
        do i = 1, size(systems)
            write(number, '(i10)') i
            number = adjustl(number)
            revivefile = file_name//"_"//trim(number)//".tarmgass_ress"
            open(newunit=unitNr, file=revivefile, status='replace')
            do j = 1, size(top%systems(i)%species)
                call systems(i)%species(j)%assemble(top%systems(i)%box)
            end do    
            write(unitNr, *) systems(i)%species(:)%frac
            write(unitNr, *) systems(i)%total_energy !Check for consistency when restarting
            call write_dlpoly_configuration_to_file(unitNr, systems(i))
            close(unitNr)
            do j = 1, size(systems(i)%species)
                call systems(i)%species(j)%reinsert(top%systems(i)%box)
            end do  
         end do
    end subroutine

    subroutine update_biases(systems)

        type(system), dimension(:) :: systems
        integer :: i, j
        real(kind=mc_prec) :: normvalue
        integer :: max
        logical :: do_rescale

        do j = 1, size(systems(1)%species(:))
            if(.not.systems(1)%species(j)%fluctuating) continue
            do_rescale = .true.
            normvalue = systems(1)%species(j)%bias%current_bias(0._mc_prec)
            do i = 1, size(systems)
                call systems(i)%species(j)%bias%normalize(normvalue)
            end do
            do i = 1, size(systems)
                max = systems(i)%species(j)%bias%get_maximal_visit()
                do_rescale = do_rescale .and. systems(i)%species(j)%bias%test_flatness(max)
            end do
            if(do_rescale)then
                do i = 1, size(systems)
                    call systems(i)%species(j)%bias%update_scale()
                end do
            end if
        end do

    end subroutine

end program runMC

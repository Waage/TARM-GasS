module sampling_module
    use molecule_module, only: single_molecule
    use parameters_module, only: FRACTIONAL_INDEX
    use precision_module, only: mc_prec
    use system_module, only: system
    implicit none
    private

    public :: write_volume, write_energies, write_molecule_numbers

contains


    subroutine write_volume(sys, fil)
        class(system) :: sys
        integer, intent(in) :: fil

        write(fil, *)sys%V 

    end subroutine

    subroutine write_energies(sys, fil)
        !Should be general enough, not really tested very rigorously
        use energy_module
        use ewald_module

        class(system) :: sys
        integer, intent(in) :: fil

        type(single_molecule), allocatable :: tmpmol
        real(kind=mc_prec) :: corren, delta_fourier_ewald_energy, dele
        class(ewald_type), allocatable :: coulombic
        integer :: i, j

        allocate(coulombic, source = sys%coulombic)

        !Compute the energy loss associated with turning off the fractional molecule
        dele = 0._mc_prec
        do i = 1, size(sys%species)
            if(sys%species(i)%fluctuating) then
                delta_fourier_ewald_energy = 0._mc_prec
                tmpmol = sys%species(i)%get_molecule(FRACTIONAL_INDEX)
                call remove_ener(sys, tmpmol, i, sys%species(i)%frac, coulombic, delta_fourier_ewald_energy)
                corren = corr_frac(sys, coulombic, sys%species(i)%frac, 0._mc_prec, i)
                dele = dele +  delta_fourier_ewald_energy + corren &
                     - sys%energy_storage%array_sum(i, 0, sys%species(:)%nmol)

                !Avoid double counting
                do j = i, size(sys%species)
                    if(sys%species(i)%fluctuating)then
                        dele = dele + sys%energy_storage%get_value(j, i, 0, 0)
                    end if
                end do
            end if
        end do

        !Total energy, total energy without the fractional molecule
        write(fil, *) sys%total_energy, sys%total_energy + dele


    end subroutine

    subroutine write_molecule_numbers(sys, fil)
    
        class(system) :: sys
        integer, intent(in) :: fil
        integer :: i
        character(6), parameter :: realfmt = "F15.13"
        character(2), parameter :: intfmt= "i5"
        character(*), parameter :: fmt = "("//intfmt//", 2X, "//realfmt//", 2X)"

        do i = 1, size(sys%species)
            write(fil, fmt=fmt, advance = 'no')sys%species(i)%nmol, sys%species(i)%frac
        end do
        write(fil, *)
    end subroutine


 end module

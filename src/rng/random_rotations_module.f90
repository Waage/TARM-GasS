module random_rotations_module
    use geometry_module, only: box_type
    use molecule_module, only: single_molecule
    use precision_module, only: mc_prec, c_mc_prec
    implicit none
    private

    public :: rotate, random_unit_vector
        
contains

    subroutine rotate(mol, box, maxrot)
        type(single_molecule), intent(inout) :: mol
        class(box_type), intent(in) :: box
        real(kind=mc_prec), intent(in), optional :: maxrot

        real(kind=mc_prec), dimension(3, mol%nats) :: dr
        integer :: i


        if(mol%rot_type == 1)then
            return
        end if
        
        call mol%assemble(box)
        if(mol%rot_type == 2)then
            dr = rotate_linear(mol, box, maxrot)
        else if(mol%rot_type == 3)then
            dr = rotate_spatial(mol, box, maxrot)
        else
            STOP 'Error: unknown molecule type for rotation'
        end if

        do i = 2, mol%nats
            mol%X(i)=mol%X(1) + dr(1, i)
            mol%Y(i)=mol%Y(1) + dr(2, i)
            mol%Z(i)=mol%Z(1) + dr(3, i)
        end do
        call mol%reinsert(box)

        return
    end subroutine

    function rotate_linear(mol, box, maxrot) result(dr)
        type(single_molecule), intent(in) :: mol
        class(box_type), intent(in) :: box
        real(kind=mc_prec), intent(in), optional :: maxrot

        real(kind=mc_prec), dimension(3, mol%nats) :: dr
        real(kind=mc_prec), dimension(mol%nats) :: norm
        real(kind=mc_prec), dimension(3) :: random_direction
        integer :: i, j

        do j = 2, mol%nats
            norm(j)=0._mc_prec
            dr(1:3, j) = box%nearest_vector(mol%X(j), mol%X(1), mol%Y(j), mol%Y(1), mol%Z(j), mol%Z(1))
            do i = 1, 3
                norm(j) = dr(i, j)*dr(i, 2) + norm(j)
            end do
        end do

        if(present(maxrot))then
            random_direction = dr(1:3, 2)/sqrt(norm(2)) + maxrot*random_unit_vector()
            random_direction = random_direction/norm2(random_direction)
        else
            random_direction = random_unit_vector()
        end if

        do j=2, mol%nats
            dr(1:3, j) = random_direction*norm(j)/sqrt(norm(2))
        end do
        
        return
    end function

    function rotate_spatial(mol, box, maxrot) result(dr)
        type(single_molecule), intent(in) :: mol
        class(box_type), intent(in) :: box
        real(kind=mc_prec), intent(in), optional :: maxrot

        real(kind=mc_prec), dimension(3, mol%nats) :: dr
        real(kind=mc_prec), dimension(3) :: dummy
        real(kind=mc_prec), dimension(4) :: quaternion
        real(kind=mc_prec), dimension(3, 3) :: rotation_matrix
        integer :: i, j, k

        do j = 2, mol%nats
            dr(1:3, j) = box%nearest_vector(mol%X(j), mol%X(1), mol%Y(j), mol%Y(1), mol%Z(j), mol%Z(1))
        end do

        if(present(maxrot)) then
            quaternion(1:4) = (/1, 0, 0, 0/) + maxrot*random_unit_quaternion()
            quaternion(1:4) = quaternion(1:4)/norm2(quaternion)
            rotation_matrix = rotation_matrix_from_quaternion(quaternion)
        else
            rotation_matrix = rotation_matrix_from_quaternion(random_unit_quaternion())
        end if

        do i = 2, mol%nats
            dummy = 0
            do j = 1, 3
                do k = 1, 3
                    dummy(j) = dummy(j) + rotation_matrix(j, k)*dr(k, i)
                end do
            end do
            dr(1:3, i) = dummy(1:3)
        end do

        return
    end function

    function rotation_matrix_from_quaternion(q) result(rotation_matrix)
        real(kind=mc_prec), dimension(4), intent(in) :: q
        real(kind=mc_prec), dimension(3, 3) :: rotation_matrix

        rotation_matrix(1, 1) = q(1)*q(1) + q(2)*q(2) - q(3)*q(3) - q(4)*q(4)
        rotation_matrix(1, 2) = 2*(q(2)*q(3) - q(1)*q(4))
        rotation_matrix(1, 3) = 2*(q(2)*q(4) + q(1)*q(3))
        rotation_matrix(2, 2) = q(1)*q(1) - q(2)*q(2) + q(3)*q(3) - q(4)*q(4)
        rotation_matrix(2, 1) = 2*(q(2)*q(3) + q(1)*q(4))
        rotation_matrix(2, 3) = 2*(q(3)*q(4) - q(1)*q(2))
        rotation_matrix(3, 3) = q(1)*q(1) - q(2)*q(2) - q(3)*q(3) + q(4)*q(4)
        rotation_matrix(3, 1) = 2*(q(2)*q(4) - q(1)*q(3))
        rotation_matrix(3, 2) = 2*(q(3)*q(4) + q(1)*q(2))

        return
    end function

    function random_unit_quaternion() result(quat)
        use rngmod
        
        real(kind=mc_prec), dimension(4) :: quat
        real(kind=mc_prec) :: rsq1, rsq2, r1, r2, r3, r4, rh

        rsq1 = 2
        rsq2 = 2
        r1 = 0; r2 = 0; r3 = 0; r4 = 0; ! Suppress spurious warning about maybe uninitialized variables from gfortran/gcc - 9
        do while(rsq1 > 1._mc_prec)
            r1 = 1._mc_prec - 2._mc_prec*rng()
            r2 = 1._mc_prec - 2._mc_prec*rng()
            rsq1 = r1*r1 + r2*r2
        end do
        do while(rsq2 > 1._mc_prec)
            r3 = 1._mc_prec - 2._mc_prec*rng()
            r4 = 1._mc_prec - 2._mc_prec*rng()
            rsq2 = r3*r3 + r4*r4
        end do
        rh = sqrt((1 - rsq1)/rsq2)
        quat(1) = r1
        quat(2) = r2
        quat(3) = r3*rh
        quat(4) = r4*rh

    end function

    function random_unit_vector() result(dr)
        use rngmod
        
        real(kind=mc_prec), dimension(3) :: dr
        real(kind=mc_prec) :: rsq, r1, r2, rh

        rsq = 2
        r1 = 0; r2 = 0; ! Suppress spurious warning about maybe uninitialized variables from gfortran/gcc - 9
        do while(rsq > 1._mc_prec)
            r1 = 1._mc_prec - 2._mc_prec*rng()
            r2 = 1._mc_prec - 2._mc_prec*rng()
            rsq = r1*r1 + r2*r2
        end do
        rh = 2._mc_prec*sqrt(1._mc_prec - rsq)
        dr(1) = r1*rh
        dr(2) = r2*rh
        dr(3) = 1 - 2._mc_prec*rsq

    end function

end module random_rotations_module

module rngmod
    use precision_module, only: mc_prec
    use mt95, only: genrand_init, genrand_real, genrand_real2
    implicit none
    private
    public :: rng, set_seed

contains

    subroutine set_seed(seed)
        integer :: seed
        call genrand_init(put=seed)
    end subroutine
    
    real(kind=mc_prec) function rng()
        real(kind = genrand_real) :: converter 
        !Needed to convert between the precision defined in mt95.f90 and mc_prec
        !(since modifying mt95 seems like a horrible idea)
        call genrand_real2(converter)
        rng = converter
        return
    end function

end module rngmod

module simulation_top_level_module
    use error_handling_module, only: error
    use lennard_jones_mix_override_module, only: lennard_jones_mix_override
    use mc_moves_module, only: mc_moves_container
    use molecule_module, only: single_molecule
    use precision_module, only: mc_prec
    use system_module, only: system
    implicit none
    private

    public :: simulation_top_level_container

    enum, bind(C)
        enumerator :: normal_simulation = 1
        enumerator :: gibbs_ensemble_simulation !Not implemented, only here as an example
    end enum

    !File ending enums
    enum, bind(C)
        enumerator :: dlpoly_file = 1
        enumerator :: xyz_file
        enumerator :: gro_file
    end enum

    type simulation_top_level_container
    
        integer :: seed
        integer :: n_equil !Number of equilibration cycles
        integer :: n_prod !Number of production cycles
        integer :: simulation_type = normal_simulation !Needs to be initialized once multiple simulation styles are available
        integer :: n_possible_moves

        type(mc_moves_container) :: moves

        real(kind=mc_prec) :: cutoff_radius !Cutoff radius for lennard jones, and also real - space part of ewald sum
        real(kind=mc_prec) :: ewald_precision 
        real(kind=mc_prec) :: temperature 
        real(kind=mc_prec) :: pressure
        type(lennard_jones_mix_override), dimension(:), allocatable :: lj_sigma_override, lj_epsilon_override

        type(system), dimension(:), allocatable :: systems
        integer, dimension(:), allocatable :: file_ending
        character(100), dimension(:), allocatable :: conf_files
        

    contains
        procedure :: set_n_systems
        procedure :: set_output_conf_format
    end type

contains

    subroutine set_n_systems(top, n_systems)
    
        class(simulation_top_level_container) :: top
        integer :: n_systems

        allocate(top%systems(n_systems), &
                 top%file_ending(n_systems), &
                 top%conf_files(n_systems))
    
    end subroutine

    subroutine set_output_conf_format(top, suff, system)
    
        class(simulation_top_level_container) :: top
        integer, intent(in), optional :: system
        character(*), intent(in) :: suff

        integer :: format_int

        select case(trim(suff))
        case("dlpolyconf")
            format_int = dlpoly_file
        case("xyz")
            format_int = xyz_file
        case("gro")
            format_int = gro_file
        case default
            format_int = xyz_file
            call error(suff, "not a known configuration file type", "use .dlpolyconf, .xyz, or .gro")
        end select

        if(present(system))then
            top%file_ending(system) = format_int
        else
            top%file_ending = format_int
        end if
    
    end subroutine
    
    

end module

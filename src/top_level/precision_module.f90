module precision_module
    use, intrinsic :: iso_fortran_env
    use, intrinsic :: iso_C_binding
    private
    !Dont touch these
    integer, parameter :: sp = REAL32
    integer, parameter :: dp = REAL64
    integer, parameter :: qp = REAL128
    
    integer, parameter :: c_sp = C_FLOAT
    integer, parameter :: c_dp = C_DOUBLE


    !Change these in order to set the floating point precisions of the program
    integer, parameter :: mc_prec = dp
    integer, parameter :: c_mc_prec = C_DOUBLE
    integer, parameter :: mc_hi_prec = qp

    public :: mc_prec, c_mc_prec, mc_hi_prec
end module

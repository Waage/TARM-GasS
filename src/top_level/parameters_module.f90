module parameters_module
    use precision_module, only: mc_prec

    real(kind=mc_prec), parameter :: pi = 3.1415926535_mc_prec  !It's pi
    real(kind=mc_prec), parameter :: chargeconv = sqrt(4172.83573_mc_prec/(4._mc_prec*pi)) !Convert charge from elementary charge to internal units
    real(kind=mc_prec), parameter :: zeta = 0.5_mc_prec     !Lennard - Jones fractional molecules zeta parameter
    real(kind=mc_prec), parameter :: BIG_ENERGY = 1E16    !A big number used in order to reject overlapping molecules
    real(kind=mc_prec), parameter :: TOLERANCE = 10._mc_prec**( -8)   !Use in cases where floating point equivalence is needed
    real(kind=mc_prec), parameter :: HC_OVERLAP_SQ = 1._mc_prec !Distance before spheres "overlap"
    real(kind=mc_prec), parameter :: bar_to_kcal_per_mol = 6.022143/(4.184*100000)
    real(kind=mc_prec), parameter :: K_to_kcal_per_mol = 1.9872041_mc_prec/(1000)
    integer, parameter :: FRACTIONAL_INDEX = 0  !The fractional molecule of a species is kept in this index
    integer, parameter :: safety_pad = 16   !Needed to safely allocate enough space that aggresively vectorized routines won't segfault
    integer, parameter :: storage_increment = 128 !How many more molecules to allocate room for whenever a fluctuating component nears the current storage maximum

end module
